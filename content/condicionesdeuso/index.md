---
title:
  - Condiciones de uso
author: ¡Vaya Mierda! Fanzine
type: page
date: 2011-02-10T12:06:44+00:00
sharing_disabled:
  - 1
robotsmeta:
  - index,follow

---
A menos que se indique lo contrario, todos los contenidos originales que publicamos se distribuyen bajo la licencia **Creative Commons Atribución-No comercial 3.0 España**. Lo que implica que puedes:

  * Copiar, distribuir y comunicar públicamente la obra.

Bajo las condiciones siguientes:

  * **Atribución**. Debes reconocer la autoría de la obra en los términos especificados. 
      * Por medio de la publicación de la dirección (url) y nombre del web para medios no digitales (prensa escrita, televisión, etc.)
      * Por medio de un link (sin “nofollow”) a los artículos involucrados para medios digitales.
  * **No comercial**. No puedes utilizar esta obra para fines comerciales.

Esto es un resumen fácilmente legible del <a href="http://creativecommons.org/licenses/by-nc/2.5/es/" target="_blank">texto legal (la licencia completa)</a>. Se prohibe la copia textual de cualquiera de los artículos publicados en nuestro sitio.

Algunas imágenes, videos o marcas pertenecientes a terceros y están fuera de las atribuciones de esta licencia, siendo los respectivos dueños quienes determinan los términos de uso de dicho material.

Si encuentras algún texto, imagen, video o cualquier tipo de material que infrija sus términos de uso te pedimos por favor que te pongas en [contacto con nosotros][1] por cualquiera de los medios disponibles en este sitio.

 [1]: https://www.vayamierdafanzine.es/contactar/ "Contacto"