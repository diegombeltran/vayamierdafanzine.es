---
title: ¡Colabora con nosotros!
author: ¡Vaya Mierda! Fanzine
type: page
date: 2010-12-29T17:37:07+00:00
sharing_disabled:
  - 1
robotsmeta:
  - index,follow

---
**¡Vaya Mierda! Fanzine** lo editamos unos pocos, y nuestros medios, fuerzas y tiempo libre son bienes escasos. Por eso hemos puesto a vuestra disposición algunos medios para colaborar con nuestra pequeña joya genitocorpórea:

1-**Participa**, léenos, comenta, da tu opinión&#8230; Estamos abiertos a todo lo que la gente piense de nosotros, pues sois la gasolina para poder emplearnos a fondo mes a mes.

2-**Envía tus textos** mediante **[ESTE][1]** formulario, para que podamos revisarlos, valorarlos y publicarlos en la sección dedicada a ello. ¡No confundir con el **F.U.R.C.I.A.**! (Extensión del artículo recomendada 2.600 caracteres con espacios incluídos).

3-**¡Difunde ¡Vaya Mierda! Fanzine por tu entorno!** Lo tienes en formato PDF a tu disposición para que lo leas donde quieres&#8230; Puedes meterlo en un pendrive, enviarlo por correo electrónico, o imprimir una tirada para repartir incluso. Te puedes chulear con tus amigos de lo _cool_ que eres al leernos.

4-**Colabora** para ayudarnos con las copias en papel del fanzine. Por **10€** tendrás derecho a un espacio donde colocar el texto y fotos que quieras.

5-**Haz una donación** con la cantidad y concepto que tú elijas. Normalmente por amor al arte. Tenemos una cuenta de _Paypal_ destinada a ello. [[Haz clic aquí]][2]

 [1]: https://www.vayamierdafanzine.es/enviar
 [2]: https://www.vayamierdafanzine.es/ayudanos-ampliar-nuestra-tirada/ "Dona tu money al humilde pensamiento humano"