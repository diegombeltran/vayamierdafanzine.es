---
title:
  - ¡Vaya Mierda! Fanzine 2.0
author: ¡Vaya Mierda! Fanzine
type: page
date: 2011-02-03T13:28:36+00:00
featured_image: /contenido/uploads/2010/12/Logo.png
keywords:
  - vaya, mierda, fanzine, 2.0, nueva, web
description:
  - Bienvenidos a la nueva web del fanzine ¡Vaya Mierda!, adaptada a las últimas tendencias tecnológicas y sociales que tanto os gustan.
robotsmeta:
  - index,follow

---
[<img loading="lazy" class="alignleft size-full wp-image-15" title="¡Bienvenidos!" src="https://www.vayamierdafanzine.es/contenido/uploads/2010/12/Logo.png" alt="" width="150" height="150" />][1]**La razón de ser de ¡Vaya Mierda! Fanzine.**

Disponemos de gran afición por el pensamiento. Es algo que nos atrae fuertemente, por lo que somos aficionados a una amplia gama de divagaciones ofreciendo éstas a aficionados de la misma índole.

El humor convive con la seriedad. Lo elaborado convive con lo cutre. La convivencia entre puntos extremos es nuestra base.

Hace ya muchos años, apareció en la mente de uno de los integrantes del Fanzine la siguiente frase: «Belleza y fiasco follan»… por ahí van los tiros. Manteniendo siempre un alto grado de actividad cerebral.

**Sede física de ¡Vaya Mierda! Fanzine:**

<p style="text-align: center;">
  <a title="Nuestra sede-Local ¡Vaya Mierda! Fanzine" href="https://www.vayamierdafanzine.es/nuestra-sede-local-vaya-mierda-fanzine/" target="_blank">[Visita virtual de la sede]</a>
</p>

<p style="text-align: right;">
  Un saludo a todo el universo.
</p>

 [1]: https://www.vayamierdafanzine.es/