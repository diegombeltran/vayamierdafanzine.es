---
title:
  - Fanzines
author: ¡Vaya Mierda! Fanzine
type: page
date: 2011-01-05T04:13:54+00:00
sharing_disabled:
  - 1
keywords:
  - fanzine, fanzines, ¡vaya mierda!, números, número, sueltos, todos
description:
  - Todos los números de ¡Vaya Mierda! Fanzine desde el año 1996, en formato PDF, y a vuestra entera disposición.
robotsmeta:
  - index,follow

---
<div class="sya_container" id="sya_container">
  <a id="year2016"></a>2016
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo-2016/" class="post-1141" rel="bookmark">Marzo 2016</a>
      </div>
    </li>
  </ul>
  
  <a id="year2015"></a>2015
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembre-2015/" class="post-1133" rel="bookmark">Noviembre 2015</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembre-2015/" class="post-1097" rel="bookmark">Septiembre 2015</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/junio-2015/" class="post-1018" rel="bookmark">Junio 2015</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/fanzine-vaya-mierda-abril-2015/" class="post-968" rel="bookmark">Abril 2015</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/vaya-mierda-fanzine-enero-2015/" class="post-901" rel="bookmark">Enero 2015</a>
      </div>
    </li>
  </ul>
  
  <a id="year2014"></a>2014
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/vaya-mierda-fanzine-noviembre-2014/" class="post-866" rel="bookmark">Noviembre 2014</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembre14/" class="post-734" rel="bookmark">Septiembre 2014</a>
      </div>
    </li>
  </ul>
  
  <a id="year2013"></a>2013
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo13/" class="post-614" rel="bookmark">Marzo 2013</a>
      </div>
    </li>
  </ul>
  
  <a id="year2012"></a>2012
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembre12/" class="post-603" rel="bookmark">Noviembre 2012</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/agosto12/" class="post-598" rel="bookmark">Agosto 2012</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/junio12/" class="post-589" rel="bookmark">Junio 2012</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/abril12/" class="post-584" rel="bookmark">Abril 2012</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/febrero12/" class="post-579" rel="bookmark">Febrero 2012</a>
      </div>
    </li>
  </ul>
  
  <a id="year2011"></a>2011
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/diciembre11/" class="post-553" rel="bookmark">Diciembre 2011</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/octubre11/" class="post-313" rel="bookmark">Octubre 2011</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/agostoseptiembre11/" class="post-308" rel="bookmark">Agosto/Septiembre 2011</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/junio11/" class="post-305" rel="bookmark">Junio 2011</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/abril11/" class="post-300" rel="bookmark">Abril 2011</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/febrero11/" class="post-169" rel="bookmark">Febrero 2011</a>
      </div>
    </li>
  </ul>
  
  <a id="year2010"></a>2010
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/diciembre10/" class="post-121" rel="bookmark">Diciembre 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/octubre10/" class="post-214" rel="bookmark">Octubre 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/verano10/" class="post-215" rel="bookmark">Verano 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/julio10/" class="post-204" rel="bookmark">Julio 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/junio10/" class="post-205" rel="bookmark">Junio 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/mayo10/" class="post-210" rel="bookmark">Mayo 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/abril10/" class="post-198" rel="bookmark">Abril 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/especialmarzo10/" class="post-219" rel="bookmark">Especial Marzo 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo10/" class="post-208" rel="bookmark">Marzo 2010</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/enero10/" class="post-202" rel="bookmark">Enero 2010</a>
      </div>
    </li>
  </ul>
  
  <a id="year2009"></a>2009
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembre09/" class="post-209" rel="bookmark">Noviembre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembrediciembre09/" class="post-211" rel="bookmark">Noviembre/Diciembre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/octubre09/" class="post-213" rel="bookmark">Octubre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/especialseptiembre09/" class="post-232" rel="bookmark">Septiembre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembre09/" class="post-217" rel="bookmark">Septiembre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/abrilmayo09/" class="post-218" rel="bookmark">Abril/Mayo 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/diciembre09/" class="post-201" rel="bookmark">Diciembre 2009</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/julio09/" class="post-203" rel="bookmark">Julio 2009</a>
      </div>
    </li>
  </ul>
  
  <a id="year2008"></a>2008
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembre08/" class="post-216" rel="bookmark">Septiembre 2008</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/agosto-2008/" class="post-199" rel="bookmark">Agosto 2008</a>
      </div>
    </li>
  </ul>
  
  <a id="year2007"></a>2007
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/enero07/" class="post-243" rel="bookmark">Enero 2007</a>
      </div>
    </li>
  </ul>
  
  <a id="year2006"></a>2006
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/agosto06/" class="post-242" rel="bookmark">Agosto 2006</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/julio06/" class="post-241" rel="bookmark">Julio 2006</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/junio06/" class="post-164" rel="bookmark">Junio 2006</a>
      </div>
    </li>
  </ul>
  
  <a id="year2000"></a>2000
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/juniojulio00/" class="post-238" rel="bookmark">Junio/Julio 2000</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/mayo00/" class="post-222" rel="bookmark">Mayo 2000</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo00/" class="post-223" rel="bookmark">Marzo 2000</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/enerofebrero00/" class="post-224" rel="bookmark">Enero/Febrero 2000</a>
      </div>
    </li>
  </ul>
  
  <a id="year1999"></a>1999
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembrediciembre99/" class="post-228" rel="bookmark">Noviembre/Diciembre 1999</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/octubre99/" class="post-212" rel="bookmark">Octubre 1999</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/agostoseptiembre99/" class="post-234" rel="bookmark">Agosto/Septiembre 1999</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/mayojunio99/" class="post-233" rel="bookmark">Mayo/Junio 1999</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo99/" class="post-206" rel="bookmark">Marzo 1999</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/enerofebrero99/" class="post-236" rel="bookmark">Enero/Febrero 1999</a>
      </div>
    </li>
  </ul>
  
  <a id="year1998"></a>1998
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembrediciembre98/" class="post-229" rel="bookmark">Noviembre/Diciembre 1998</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembreoctubre98/" class="post-226" rel="bookmark">Septiembre/Octubre 1998</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/verano98/" class="post-221" rel="bookmark">Verano 1998</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/enero98/" class="post-240" rel="bookmark">Enero 1998</a>
      </div>
    </li>
  </ul>
  
  <a id="year1997"></a>1997
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/diciembre97/" class="post-237" rel="bookmark">Diciembre 1997</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/mayojunio97/" class="post-235" rel="bookmark">Mayo/Junio 1997</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/primavera97/" class="post-227" rel="bookmark">Primavera 1997</a>
      </div>
    </li>
  </ul>
  
  <a id="year1996"></a>1996
  
  <ul>
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/diciembre96/" class="post-200" rel="bookmark">Diciembre 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/noviembre96/" class="post-230" rel="bookmark">Noviembre 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/septiembre96/" class="post-239" rel="bookmark">Septiembre 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/verano96/" class="post-225" rel="bookmark">Verano 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/julio96/" class="post-231" rel="bookmark">Julio 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/abril96/" class="post-220" rel="bookmark">Abril 1996</a>
      </div>
    </li>
    
    <li class="">
      <div class="sya_postcontent">
        <span class="sya_date"> </span><a href="https://www.vayamierdafanzine.es/marzo96/" class="post-111" rel="bookmark">Marzo 1996</a>
      </div>
    </li>
  </ul>
</div>