---
title: Pide tu ¡Vaya Mierda! Fanzine
author: ¡Vaya Mierda! Fanzine
type: page
date: 2011-11-07T15:27:46+00:00
robotsmeta:
  - index,follow

---
<p style="text-align: center;">
  <strong>ADQUIERE AL</strong>
</p>

<p style="text-align: center;">
  <strong>BOCHORNOSO PRECIO DE</strong>
</p>

<p style="text-align: center;">
  <strong><span style="color: #c77577;">2,50€</span> LA VERSIÓN IMPRESA</strong>
</p>

<p style="text-align: center;">
  <strong>DE <span style="text-decoration: underline;">¡VAYA MIERDA! FANZINE</span></strong>
</p>

<p style="text-align: center;">
  <a title="¡Pídelo!" href="https://www.vayamierdafanzine.es/contactar/"><strong>¡AQUÍ!</strong></a>
</p>

<p style="text-align: center;">
  <img loading="lazy" class="size-medium wp-image-89 aligncenter" title="¡No lo pienses más!" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/06/portada_junio.jpg" alt="" width="234" height="300" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/06/portada_junio.jpg 960w, https://www.vayamierdafanzine.es/contenido/uploads/2011/06/portada_junio-234x300.jpg 234w, https://www.vayamierdafanzine.es/contenido/uploads/2011/06/portada_junio-801x1024.jpg 801w" sizes="(max-width: 234px) 100vw, 234px" />
</p>

<p style="text-align: center;">
  <strong>Páginas repletas de alta alcurnia mental entremezclada con un toque de bucocalcarismo neuronal perenne.</strong>
</p>

<p style="text-align: center;">
  <strong>Portada a todo color con una excepcional adaptación del ganador del concurso «Peor chiste regurgitado con métodos analógicos»</strong>
</p>

<p style="text-align: center;">
  <span style="color: #c77577;"><strong>ENCARGA UN FANZINE POR SOLAMENTE 2,50€ + GASTOS DE ENVÍO. TE LO MANDAMOS A TU CASA, A LA OFICINA, AL BAR, AL CHALET DE LA PLAYA O MONTAÑA, A TU EMPRESA, A LA OFICINA DEL INEM, AL OJO DEL PUENTE QUE SELECCIONES O A CUALQUIER PARADERO DONDE HABITE UN ALMA CON CEREBRO ÚTIL</strong></span>
</p>