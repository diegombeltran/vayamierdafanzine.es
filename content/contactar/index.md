---
title: Contactar
author: ¡Vaya Mierda! Fanzine
type: page
date: 2010-12-29T16:58:29+00:00
sharing_disabled:
  - 1
robotsmeta:
  - index,follow

---
¿Tienes alguna duda? ¿No te gusta algo y crees que deberíamos cambiarlo? ¿Quieres quedar con algún redactor porque te has enamorado de él/ella? Bien, te presentamos el «Formulario Universal Responsable del Contacto con Interacción Adecuada», también conocido como **F.U.R.C.I.A**. el formulario obligado típico tan de moda en las páginas de la red.

**¡CUIDADO**! Si lo que quieres son enviar textos para colaborar, entonces el tuyo es **<a href="https://www.vayamierdafanzine.es/enviar" target="_self">OTRO</a>**

<div role="form" class="wpcf7" id="wpcf7-f318-o2" dir="ltr">
  <div class="screen-reader-response" role="alert" aria-live="polite">
  </div>
</div>