---
title:
  - Redactores
author: ¡Vaya Mierda! Fanzine
type: page
date: 2011-01-05T04:16:32+00:00
sharing_disabled:
  - 1
keywords:
  - redactores
description:
  - Un vistazo a cada uno de los que hacen posible ¡Vaya Mierda! Fanzine mes a mes.
robotsmeta:
  - index,follow

---
**[<img loading="lazy" class="alignleft wp-image-672 size-thumbnail" title="Neurona" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto-mia-jilipollas-150x150.jpg" alt="" width="150" height="150" />][1]<span style="text-decoration: underline;">Neurona (David Juan)</span> ** **<a href="http://www.facebook.com/davidjuanb" target="_blank" rel="noopener"><img loading="lazy" class="alignnone" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a>** <a href="mailto:neurona@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" class="alignnone" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>  
Director, articulista y patético dibujante de mierda

> Es un humanocefálicoalmáticoeyaculativo nacido en Zaragoza (España, Europa, Mundo, Cosmos).  
> Como de pequeño le vio el culo morenito a una niña de la guardería, sus neuronas comenzaron a eyacular sin remedio y con el tiempo empezó a escribir en un fanzine. Actualmente sigue sobreviviendo.  
> «Soy un ser material y orgánico con características situadas entre un troncho de lechuga y una especie endémica de Juslibol.»

&nbsp;

**<span style="text-decoration: underline;"><a href="http://www.dmbnader.es"><img loading="lazy" class="alignleft size-thumbnail wp-image-339" title="Nader" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nader-150x150.jpg" alt="" width="150" height="150" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nader-150x150.jpg 150w, https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nader.jpg 178w" sizes="(max-width: 150px) 100vw, 150px" /></a>Nader (Diego)</span>  ** <a href="mailto:nader@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>  
Redactor, administrador web

> Bipolar tipejo acostumbrado a hablar antes de pensar, [gato][2] residente en Madrid City, capital del Imperio Gañán.  
> Se dice que cuando no rompe sus ojos picando código como un esclavo para la web, arruina tabernas mañas a base de malas artes.  
> «Peligroso y armado de prejuicios insanos»

&nbsp;

**<span style="text-decoration: underline;"><a href="http://labrujuladelerrante.blogspot.com"><img loading="lazy" class="alignleft size-thumbnail wp-image-341" title="Nolfy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nolfy-150x150.jpg" alt="" width="150" height="150" /></a>Nolfy (Alfonso)</span>  <a href="http://www.facebook.com/Nolfy" target="_blank" rel="noopener"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> ****<a href="http://twitter.com/abarcelonap" target="_blank" rel="noopener"><img loading="lazy" title="Twitter" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/twitter.png" alt="" width="16" height="16" /></a> **<a href="mailto:nolfy@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>  
Redactor, tesorero, gañán por excelencia

> Ciudadano del mundo, turista de carrera, intento de escritor _amateur_, aficionado a algo que mucha gente parece olvidar hacer: pensar. En sus ratos libres <span class="Apple-style-span">«jugón» </span>y consumidor de cerveza.  
> «El cambio es la única constante y debemos adaptarnos a él.»

&nbsp;

**<span style="text-decoration: underline;"><a href="http://sandmanvm.wordpress.com/"><img loading="lazy" class="alignleft size-thumbnail wp-image-342" title="Sandman" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/sandman-150x150.jpg" alt="" width="150" height="150" /></a>Sandman (Roberto)</span>  <a href="http://www.facebook.com/roberto.balado" target="_blank" rel="noopener"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> <a href="mailto:sandman@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactor, premio al mejor troll 2010-2011

> Persona sin escrúpulos ni pelos en la lengua, capaz de provocar ganas de suicidarte en cuestión de segundos. Mordaz dedo en la llaga.

&nbsp;

**<span style="text-decoration: underline;"><img loading="lazy" class="alignleft size-thumbnail wp-image-343" title="Teletubi" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/teletubi.jpg" alt="" width="150" height="150" />Teletubi (David)</span>  <a href="http://www.facebook.com/profile.php?id=568739580&ref=ts" target="_blank" rel="noopener" class="broken_link"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> <a href="mailto:teletubi@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactor, padre de Amebín

> Redactor de sandeces y cosas serias, dependiendo del momento vital. De pequeño ya montaba revoluciones con sus _playmobil_. Cuando se hizo mayor ratificó y consolidó su postura ideológica gracias a un mayor conocimiento científico. Analista informático al que le encanta la lectura, el cine y el teatro, además de salir por ahí rodeado de buena compañía.
> 
> «Es duro luchar ahora para cambiar las cosas, pero más duro será nuestro futuro si no lo hacemos»

&nbsp;

**<span style="text-decoration: underline;"><a href="http://ireneachon.wordpress.com/"><img loading="lazy" class="alignleft size-thumbnail wp-image-349" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nene-150x150.jpg" alt="" width="150" height="150" /></a>Nene (Irene)</span>  <a href="http://www.facebook.com/profile.php?id=100003124269016" target="_blank" rel="noopener" class="broken_link"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> <a href="mailto:nene@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactora.

> Filóloga Hispánica. Lo del fanzine lo hace de forma altruista por una promesa que le hizo a Dios un día.  
> «No creo en hombres y mujeres. Creo en personas»

&nbsp;

**<span style="text-decoration: underline;"><a href="http://nabey.deviantart.com/"><img loading="lazy" class="alignleft size-thumbnail wp-image-349" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/nabey-150x150.jpg" alt="" width="150" height="150" /></a>Nabey (Débora)</span>  <a href="http://www.facebook.com/DeboraAguelo" target="_blank" rel="noopener"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> ****<a href="http://twitter.com/Debora_Aguelo" target="_blank" rel="noopener"><img loading="lazy" title="Twitter" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/twitter.png" alt="" width="16" height="16" /></a>** <a href="mailto:nabey@vayamierdafanzine.es" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>  
Ilustradora y diseñadora gráfica

> Diseñadora industrial y dibujante. Colabora en el fanzine por amor al arte y porque le ofrecieron patatas fritas gratis. Le gusta leer, el cine, y detesta la televisión.  
> «La televisión ha hecho maravillas por mi cultura. En cuanto alguien enciende la televisión, voy a la biblioteca y me leo un buen libro»
> 
> &nbsp;

**<span style="text-decoration: underline;"><img loading="lazy" class="wp-image-641 size-thumbnail alignleft" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/sborao-141x150.jpg" alt="" width="141" height="150" />Sergio Borao Llop</span>  <a title="Sergio Borao Llop" href="https://www.facebook.com/Sergio.Borao.Llop" target="_blank" rel="noopener"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> **<a href="https://twitter.com/S_Borao_Llop" target="_blank" rel="noopener"><strong><img loading="lazy" title="Twitter" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/twitter.png" alt="" width="16" height="16" /></strong></a> <a href="mailto:sbllop@gmail.com" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>  
Poeta y escritor

> Es miembro de Poetas del Mundo, de la Red de Escritores en Español (REMES) y de Los puños de la paloma. Colabora habitualmente en los boletines electrónicos Inventiva Social e Inventrén.
> 
> Sus trabajos aparecen en diferentes páginas web como la Biblioteca Virtual Miguel de Cervantes, Poesi.as, Arte poética (web de André Cruchaga), Poetas del mundo, Proyecto Patrimonio y en revistas como EOM, Almiar (Margen Cero) o Letralia entre otras muchas.  
> Ha publicado el libro de relatos El alba sin espejos <a href="https://literaturame.net/libro/el-alba-sin-espejos/" target="_blank" rel="noopener">https://literaturame.net/libro/el-alba-sin-espejos/</a><span style="color: #008080; font-size: medium;"><b><span style="color: #800000; font-size: large;"><span style="color: #008080; font-size: medium;"><b><br /> </b></span></span></b></span>
> 
> &nbsp;

<p style="text-align: left;">
   <strong><span style="text-decoration: underline;"><a href="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Nombre1.jpg"><img loading="lazy" class="alignleft wp-image-661 size-thumbnail" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Nombre1-150x150.jpg" alt="Santo Espanto" width="150" height="150" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Nombre1-150x150.jpg 150w, https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Nombre1.jpg 163w" sizes="(max-width: 150px) 100vw, 150px" /></a></span></strong><strong><span style="text-decoration: underline;">Santo Espanto (Iñaki Goñi)</span>  <a title="Iñaki Goñi - Santo Espanto" href="https://www.facebook.com/santoespanto" target="_blank" rel="noopener" class="broken_link"><img loading="lazy" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> </strong> <a href="mailto:igsilustrador@gmail.com" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a><br /> <em><span class="_5yl5" data-reactid=".4r.$mid=11403864033212=29792d5ddcd51e7e666.2:0.0.0.0.0"><span class="null">Ilustrador y diseñador gráfico profesional.</span></span></em>
</p>

> > <span class="_5yl5" data-reactid=".4r.$mid=11403864033212=29792d5ddcd51e7e666.2:0.0.0.0.0"><span class="null">Ser bidimensional de alto resplandor gris. Inconsciente ilustrador polimórfico. Grabador de insectos en piel humana. Pensador inflamable y fluorescente. Buscador diogenético de tesoros urbanos. Su habitat se reduce a lugares fríos y oscuros. Manténgase fuera del alcance de los niños. </span></span>
> 
> > <span class="_5yl5" data-reactid=".4r.$mid=11403864033212=29792d5ddcd51e7e666.2:0.0.0.0.0"><span class="null">Cita preferida: “Un intelectual cuenta cosas simples de forma complicada. Un artista cuenta cosas complejas de forma simple” Charles Bukowski</span></span>
> > 
> > &nbsp;

**<span style="text-decoration: underline;"><a href="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/xmasylogo.png"><img loading="lazy" class="alignleft wp-image-687 size-thumbnail" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/xmasylogo-150x150.png" alt="" width="150" height="150" /></a>X+Y</span>   <a href="https://twitter.com/xmasynet" target="_blank" rel="noopener"><strong><img loading="lazy" title="Twitter" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/twitter.png" alt="" width="16" height="16" /></strong></a> <a href="mailto:xmasystudios@gmail.com" target="_blank" rel="noopener"><img loading="lazy" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactor

**X + Y** está  solo ante las dudas ecuacionales y esconde sus experimentos todo lo que puede. Puede ser que lo que más aprecies sean los títulos, y que aprendas a valorar un poco más el silencio a partir de hoy. Su rincón digital en: <a href="http://xmasy.net" target="_blank" rel="noopener" class="broken_link">http://xmasy.net</a>

&nbsp;

<p style="text-align: left;">
  <strong><span style="text-decoration: underline;"><strong><span style="text-decoration: underline;"><a href="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto-perfil.jpg"><img loading="lazy" class="alignleft wp-image-699 size-thumbnail" title="Nene" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto-perfil-150x150.jpg" alt="" width="150" height="150" /></a></span></strong>Javier Burguinho</span>    </strong><a href="http://www.facebook.com/nenmelo" class="broken_link"><img loading="lazy" class="alignnone" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a><strong> </strong><a href="mailto:javibman@hotmail.com" target="_blank" rel="noopener"><img loading="lazy" class="alignnone" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a><br /> Redactor y pensador variopinto
</p>

_«Yo era el nuevo Maradona»._

Sus espacios digitales en:

> <p style="text-align: right;">
>   <a title="Burguinhadas" href="http://burguinhadas.wordpress.com" target="_blank" rel="noopener">http://burguinhadas.wordpress.com</a>
> </p>
> 
> <p style="text-align: right;">
>   <a title="Japiplallers" href="http://happyplayers.wordpress.com" target="_blank" rel="noopener">http://happyplayers.wordpress.com</a>
> </p>
> 
> <p style="text-align: right;">
>   <a title="Portfolio Javi Burgueño" href="http://www.behance.net/lalocuraosharalibres" target="_blank" rel="noopener">http://www.behance.net/<wbr></wbr>lalocuraosharalibres</a>
> </p>

&nbsp;

**<span style="text-decoration: underline;"><a href="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Patricia-Torres-Foto-pequeña.jpg"><img loading="lazy" class="alignleft size-thumbnail wp-image-730" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Patricia-Torres-Foto-pequeña-150x150.jpg" alt="Patricia Torres" width="150" height="150" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Patricia-Torres-Foto-pequeña-150x150.jpg 150w, https://www.vayamierdafanzine.es/contenido/uploads/2011/01/Patricia-Torres-Foto-pequeña.jpg 300w" sizes="(max-width: 150px) 100vw, 150px" /></a>Lady Jamfri (Patri Torres)</span>  <a href="https://www.facebook.com/patry.torresvicente" target="_blank" rel="noopener" class="broken_link"><img loading="lazy" class="alignnone" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> <a href="mailto:patricia.tor@hotmail.com" target="_blank" rel="noopener"><img loading="lazy" class="alignnone" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactora sección cine.

> Demente enamorada del cine. Se alimenta de todo lo escatológico y bizarro. No dar de comer a partir de las 24 h.
> 
> &nbsp;
> 
> &nbsp;
> 
> &nbsp;

**<span style="text-decoration: underline;"><a href="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto.jpg"><img loading="lazy" class="alignleft wp-image-878 size-thumbnail" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto-150x150.jpg" alt="" width="150" height="150" /></a>Susana portero</span>  <a href="https://www.facebook.com/susanasantidad" target="_blank" rel="noopener" class="broken_link"><img loading="lazy" class="alignnone" title="Facebook" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/facebook.png" alt="" width="16" height="16" /></a> <a href="mailto:susanaporterozgz@gmail.com" target="_blank" rel="noopener"><img loading="lazy" class="alignnone" title="Email" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/01/email.png" alt="" width="16" height="16" /></a>**  
Redactora.

> Documentalista. Cinéfila extreme. Traductora oficial de idiomas alienígenas en sus ratos libres.
> 
> <a href="http://www.etereoestereo.tumblr.com" target="_blank" rel="noopener" class="broken_link">http://www.etereoestereo.tumblr.com</a>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2011/01/foto-mia-jilipollas.jpg
 [2]: http://es.wikipedia.org/wiki/Madrid#Gentilicio