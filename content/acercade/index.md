---
title: Acerca de...
author: ¡Vaya Mierda! Fanzine
type: page
date: 2010-12-29T09:33:21+00:00
sharing_disabled:
  - 1
robotsmeta:
  - index,follow

---
###### **«El fanzine de España, más famoso a excepción de los que tienen más fama que él»** &#8211; _Señora de barrio anónima de las que salen en el Telediario de Antena 3._

Estimados entes pensiles del cosmos orgánico:

**¡Vaya Mierda! Fanzine** tuvo como inicio y nacimiento la asociación _Os Mesaches_ de Zaragoza hacia el año **1996** (_MCMXCVI_,  año bisiesto comenzando en lunes según el calendario gregoriano)

En esa época, la madurez de los seres que lo gestionaban era algo inferior o llamemosla «diferente» a la actual&#8230; Por lo que en sus páginas convivían en exceso pornografía y mentes deformes. Además, los medios de difusión eran radicalmente distintos a lo que tenemos hoy en día, lo que no nos permitía llegar a vosotros como hubiésemos querido.

Actualmente somos unos cuantos micos emancipados de nuestras raíces, alguno pasado la treintena, otro recién salido del cascarón&#8230; y de distintas partes del mapa, por lo que vemos la vida con cristales muy diferentes a hace 15 años.

Nos gusta fardar de nuestro carácter **gratuito**, y animamos a a que la gente lo difunda entre sus amiguetes (pues no hay nada más gratificante como el ser leído), o que **envíen** sus artículos, opiniones, colaboraciones, etc.

Esperamos que disfrutéis de nuestras eyaculaciones neuronales (_**Ajos Revolt**_, como solemos decir nosotros) que pueblan cada una de las inquietantes páginas de nuestra cutre humilde publicación.
