---
title: Un buen día
author: Neurona
type: post
date: 2015-04-06T11:12:29+00:00
url: /un-buen-dia/
categories:
  - Blog

---
A las ocho en punto Javier abre los ojos y pone fin así a sus plácidos sueños. Con la mirada del que acaba de soñar cosas que no debería, mira por la ventana y ve a su vecina pasar, que lo saluda sonriente, sin razón aparente. Javier se extraña porque se mudó hace seis meses y nunca han coincidido. Se extraña, pero también obtiene energía extra para este pegajoso día de agosto.  
Sale a la calle y se da cuenta de que se ha olvidado las llaves, sabiendo que va a estar todo el verano solo en casa. De repente oye un ruido brusco detrás de él, como si algo cayera al suelo. Se gira y encuentra sus llaves. Mira hacia arriba, para ver de dónde han caído, y en una ventana no consigue vislumbrar más que la sombra de una melena al viento.  
Después de salir del trabajo, con la ciudad tan tranquila como cada mes de verano, se queda paralizado cruzando un paso de cebra: un coche desbocado y sin control se acerca a toda velocidad hacia él. A la vez, una ciclista, también muy deprisa, lo arroya, con tanta suerte que ambos consiguen caer a suficiente distancia del imprudente conductor del automóvil. La ciclista se monta en la bicicleta y se va. Javier, igualmente paralizado que antes, contempla cómo se alejan ambos conductores. De los nervios le da por volver a comprarse tabaco, ocho meses después del último cigarrillo.  
Volviendo a casa, al abrir el ascensor, un poco confundido, comprueba que la puerta está abierta. Demasiado para un solo día, ¿es el Señor que hoy ha centrado todos sus experimentos en él? Entra en casa y mira en cada habitación, pero no falta nada. Al final va a la cocina y comprueba que se había dejado el gas abierto.  
A las ocho y cinco el timbre de casa lo despierta. Abre los ojos, pero no ve absolutamente nada. Descubre que su cabeza se encuentra apoyada en el bol de los cereales. Da gracias por, inexplicablemente, haberse mantenido en equilibrio y no haberse derramado nada de leche: sabe que tiene que estar presentable para el otro lado de la puerta.  
Se levanta de la silla, va, y abre:  
– Javier, ¡no va a creerse lo que le ha pasado! ¡Vaya día ha tenido!

X+Y