---
title: Sesión dobla-grapil Junio 2015
author: Neurona
type: post
date: 2015-06-28T13:36:15+00:00
url: /sesion-dobla-grapil-junio-2015/
categories:
  - Blog

---
¡Vaya Mierda! Fanzine en papel del mes de Junio 2015 ha sido parida.

Contacten con nosotros si desean un ejemplar o varios para repartir.

Os dejamos los mejores momentos del evento.

[<img loading="lazy" class="alignleft size-full wp-image-1024" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015b.jpg" alt="dg-junio2015b" width="1000" height="750" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015b.jpg 1000w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015b-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015b-400x300.jpg 400w" sizes="(max-width: 1000px) 100vw, 1000px" />][1] [<img loading="lazy" class="alignleft size-large wp-image-1025" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015a.jpg" alt="dg-junio2015a" width="584" height="438" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015a.jpg 1000w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015a-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015a-400x300.jpg 400w" sizes="(max-width: 584px) 100vw, 584px" />][2]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015b.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2015/06/dg-junio2015a.jpg