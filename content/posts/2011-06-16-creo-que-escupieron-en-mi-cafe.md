---
title: Creo que escupieron en mi café
author: Neurona
type: post
date: 2011-06-16T07:57:36+00:00
url: /creo-que-escupieron-en-mi-cafe/
jabber_published:
  - 1308214658
categories:
  - Blog
tags:
  - café
  - gapo

---
De vez en cuando me vienen a la mente cosas raras. Elementos absurdos de extraña procedencia que provocan en mi innato comportamiento la necesidad de plasmar en un papelito el meollo de la, a veces compleja, trama de un microrrelato que no pretendo dejar pasar y no plasmarlo a golpe de tecla.  
La reflexión que toca hoy, porque así surgió mientras tomaba café, tiene su origen cuando observaba mi matutino cortado en vaso de corcho blanco que mi hermano me había traído del bar. Generalmente suelo oler algunas cosas, no todas. Las de índole fecal y escatológica suelen producirme un tremendo respeto. Tomé el vaso de café con mi mano derecha y percibí el añorado calor de una fría mañana de invierno. Acto seguido acerqué mi nariz a dicho recipiente y olisqueé su contenido. Es extraño, pero el olor primordial que emanaba del interior no era precisamente la leche caliente mezclada con café exprés, sino un cierto toque a saliva.  
¿Saliva? ¿Y cómo sé a qué sabe la saliva? Reconozco que es una reflexión de lo más extraña y asquerosa, pero no lo puedo remediar, así vino a mi Yo desde algún mutante gen.  
¡Alguien ha escupido en mi café! Fue lo primero que pensé.