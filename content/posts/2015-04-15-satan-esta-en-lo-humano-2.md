---
title: Satán está en lo humano
author: Neurona
type: post
date: 2015-04-15T18:31:19+00:00
url: /satan-esta-en-lo-humano-2/
categories:
  - Blog

---
Desconozco, he de reconocerlo, por vagancia, si los humanos fuimos inventados por los reptilianos o por obra de la evolución (vibraciones – átomos – moléculas – tejidos – seres vivos – mono – ser humano) o por cualquier otra teoría.

Sin embargo, en este breve texto, voy a plasmar mi impresión sobre lo que creo que es la esencia satánica que en mayor o menor medida se encuentra integrada en nuestro Yo más interno.

«Homo homini lupu» (el hombre es el lobo del hombre) Se cita con frecuencia cuando se hace referencia a los horrores de los que es capaz la humanidad para consigo misma.

«Ande yo caliente y ríase la gente» «Tengo la vida resuelta, no puedo empatizar por que me pongo enfermo» y un largo etcétera, creo que pueden sintetizar de forma altamente concentrada el por qué el mal nace en lo humano al igual que la pobreza.

Tengo una edad en la cual ni soy viejo ni joven. No sé hacer nada en concreto y a la vez, puedo hacer bastantes cosas, sin embargo me será difícil hacerme valedor en este mundo donde cada pieza laboral implica ser un premio nobel en el arte de poner un tornillo aunque no sepas pelar una patata.

Miro a mi alrededor y sigo viendo el mundo dirigido por cuatro y al resto conviviendo entre el disfraz de la felicidad y el estallido social.

Todo está controlado para comprobar cuál es el límite en que la sociedad puede rebasarlo y asociarse reventando hasta crear una bomba humana del mayor calibre que jamás hayamos visto en la breve historia de nuestra especie.

En ese estrecho segmento se mueven sin escrúpulos los propietarios de las cuerdas de nuestras vidas.

A día de hoy no creo en las coincidencias, sino en la fina planificación del sistema que decide sobre cada insignificante vida.

¿Por qué no revienta?

Muy simple. En mayor o en menor medida, todos somos Satán. En función de las circunstancias éste aflora o permanece cautivo. Pero no vive en el aire, sino en cada uno de los rincones de nuestro corazón.

Sinceramente, todavía no se si la especie humana tiene esperanza y más aún, si la merece.

_El nombre Satanás -o Satán- deriva del latín Satāna, y éste a su vez del arameo_ _הַשָּׂטָן, ha-shatán, «adversario, enemigo, acusador». Aunque luego se le menciona como un espía errante de Dios sobre la Tierra, el sentido primario, de la raíz_ _שטן (štn, «impedir, hostigar, oponerse»), sería simplemente el de «enemigo»._

P.D.: Quizá todos nos hemos convertido en espías.

<p style="text-align: right;">
  Neurona
</p>