---
title: La realidad Por Nolfy
author: Neurona
type: post
date: 2015-09-09T11:38:44+00:00
url: /la-realidad-por-nolfy/
categories:
  - Blog

---
Real o irreal. Par o impar. Reducir lo real o irreal a un binomio es lo mismo que tratar de reducir las matemáticas a par o impar, es limitado y de perspectiva mediocre.

Los seres humanos basamos nuestra realidad en percepciones e instintos, tanto en nuestra relación con el mundo como con nuestras relaciones con el resto de personas. Estas percepciones evolucionan, amplían o reducen su espectro de onda, mezcla de los intereses, voluntades y moldeado por las experiencias del individuo. Las personas pues nos comunicamos con otras con las que compartimos esos espectros de onda, ciertos niveles perceptivos o sensoriales, y es más, nos vemos atraídos hacia ellos. Y con aquellos que no, pasan por nuestra vida como si fueran trozos de carne sentientes parcialmente ajenos a nuestra realidad. En parte escogemos lo que queremos ver y a nivel inconsciente escogemos con quién queremos hablar.

Incluso con esas personas con las que compartimos esos niveles sensoriales (pueden ser muchos, pocos o todos) existe una subcomunicación en una pléyade de longitudes que vamos aprendiendo (o no) con el tiempo y que hay que reconocerlo, las mujeres dominan con mayor profusión y facilidad y de forma más precoz también. A ellas les toca parir, pero también ellas tienen la capacidad de escoger a quién paren. Aunque sea a un nivel primario e inconsciente, las mujeres saben cuando un tío se siente atraído por ellas mientras que al contrario ocurre mucho más raramente. Ventaja táctica creo yo, clave en el proceso de apareamiento humano. Para muchos hombres, esa comunicación no existe, es irreal. Otras veces claro está, distorsiones de estas capacidades pueden llevar a una mujer a escoger erróneamente y perpetuar los genes de un hijo de la gran puta. Mala suerte. Por otro lado también existen las liantas mosquitas muertas.

Ciertamente mi enfoque de lo real o irreal ha sido hasta ahora a través de un objetivo macro. Cambiando la pieza de la cámara y poniendo un teleobjetivo podríamos volver a formular la eterna pregunta de los filósofos de si lo que percibimos en nuestra vida diaria es real o no. Volvemos una y otra vez a lo mismo, desde luego, en cierta longitud de onda o de sensación es real. Si me tiro por la ventana desde un sexto piso y aterrizo encima de un coche en la acera seguramente no seguiré escribiendo artículos como éste, pero al igual que en las relaciones humanas las percepciones del lenguaje y la comunicación no verbal se desarrollan y se vuelven más complejas, es posible que nuestra evolución como especie nos lleve a un desarrollo sensorial superior que nos permita ampliar nuestras percepciones y comunicarnos, qué se yo, con los pollos. Y desde luego quien dice pollos podría decir otras especies vivientes alienígenas que quizá, a partir de determinado grado de evolución podríamos ver, al igual que si cogiésemos a un troglodita y lo llevásemos a la edad media un caballero medieval en armadura le parecería una divinidad aterradora, o a ese mismo caballero le parecería un mundo demoníaco si le llevásemos al siglo XXI. Pero a nosotros nos parece normal.

Si hacemos una panorámica&#8230; ¿habrá un final alguna vez a la evolución? ¿Estamos condenados a una chiflada carrera de eterno desarrollo sensorial o llegaremos alguna vez a algo definitivo? ¿Se terminará el consumo mundial de pollos por una comprensión y empatía con sus emociones? ¿Hacia dónde vamos como especie?

Nolfy.