---
title: Tácticas y estrategias en el escenario político
author: Neurona
type: post
date: 2015-06-28T20:25:57+00:00
url: /tacticas-y-estrategias-en-el-escenario-politico/
categories:
  - Blog

---
<p align="CENTER">
  <span style="color: #000000;"><span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;"><i>Para que estalle la revolución no suele bastar con que «los de abajo no quieran» sino que hace además que «los de arriba no puedan» seguir viviendo como hasta entonces.</i></span></span></span>
</p>

<p align="CENTER">
  <span style="color: #000000;"><span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Vladimir Ilich Ulianov (Lenin)</span></span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;"><b>Antecedentes</b></span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">El llamado «milagro económico» español de hace casi veinte años resultó ser una estafa. Lógicamente, una política basada en privatizar las empresas públicas con beneficios, liberalizar el suelo para fomentar la burbuja inmobiliaria, recortar en derechos sociales, rebajar impuestos a las grandes fortunas y permitirles «emprender» libremente trajo una liquidez inmediata que permitió a España entrar en el euro. No obstante, esta liquidez no salía de la nada, sino que se estaba robando del futuro: Con un estado débil y un mercado desequilibrado y sin control el desastre estaba servido.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Durante el auge de esta gran burbuja la brecha entre pobres y ricos iba aumentando, y con el comienzo de la crisis esta diferencia se incrementó exponencialmente. Finalmente, la juventud se dio cuenta de que se le había prometido vivir mejor que sus padres pero que acabará viviendo como sus abuelos. Ante esta situación la protesta cristalizó con un grito de rabia e indignación, dando forma a fenómenos como el 15-M, Stop Deshaucios, Mareas de distintos colores, entre otros.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Las calles estaban comenzando a entrar en ebullición reclamando justicia y democracia de verdad. No obstante, desde la mal llamada transición democrática (sería más correcto definirla como una restauración bornónica), el poder económico, legislativo y ejecutivo seguía estando al servicio de la oligarquía económica, siendo defendida por dos partidos lacayos: PP y PSOE. Las calles eran nuestras. El poder era de ellos.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Sin embargo, este poder se podía perder. Y las primeras campanadas se dieron tras varios sondeos en verano de 2013: Junto a una inexorable caída de PP y PSOE el, por entonces, único partido nacional de izquierda real (Izquierda Unida) comenzaba a superar en intención directa de voto (IDV) al PSOE y rondaba ya el 15% de voto estimado. Existía el miedo de que ocurriese en España lo mismo que en Grecia, donde Syriza (el equivalente a IU tanto en ideología como en estrategia y en estructura organizativa) ha pasado en 6 años de tener menos del 5% de los votos a gobernar su país. Por ello, la élite gobernante se puso a trabajar para evitar que ocurriese lo que ya iba a ser inevitable en Grecia.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Diez meses después, un nuevo partido político, Podemos, dio la sorpresa en las elecciones europeas pese a tener unos pocos meses de vida, consiguiendo el 8% de los votos. Izquierda Unida, en coalición con otras fuerzas alcanzó el 10%. Había, por tanto, dos grandes bloques de izquierda real y entre ambos abarcaban el 18% de los votos.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;"><b>Tácticas y estrategias</b></span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Antes de seguir avanzando, es necesario identificar a los tres actores que permitieron crear el fenómeno de Podemos:</span></span>
</p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Un grupo de profesores universitarios, con amplio conocimiento en estrategia política.</span></span>
    </p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Izquierda Anticapitalista, un partido político de ideología trostkista escindido de Izquierda Unida, que pudo aportar organización y experiencia militante.</span></span>
    </p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Los medios de comunicación, al servicio de la oligarquía económica, que aportaron la promoción y difusión.</span></span>
    </p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Los dos primeros actores aspiraban a un cambio en la sociedad. El objetivo era el mismo que Izquierda Unida, pero su estrategia era muy diferente, utilizando líneas populistas (léase no en sentido peyorativo, sino en el definido por Laclau). En contraposición, el tercer actor buscaba frenar el crecimiento de Izquierda Unida, fomentando un partido similar, y promover el enfrentamiento entre ambos para dinamitar cualquier posibilidad de cambio verdadero.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">El resultado de las europeas fue agridulce para la élite gobernante: Efectivamente, se consiguió frenar y dividir el crecimiento de la izquierda real, pero en el intento de «suplantar» a Izquierda Unida con algo que se pareciese a ella se había creado un posible socio. La principal dificultad de IU en las últimas décadas ha residido en su soledad en la lucha frente al liberalismo propugnado por PP y PSOE. Ahora surgía una nueva oportunidad que se podía aprovechar.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Sin embargo, esta oportunidad también contenía veneno. Dentro de Izquierda Unida convivía una mayoría de militantes de intachable compromiso y honradez con una minoría que había adquirido vicios propios de otros partidos y que perjudicaba la imagen al exterior. Hay que tener en cuenta que Izquierda Unida se fundó en 1986 como un movimiento político y social y que, por tanto, la confluencia está dentro de su naturaleza. No obstante, una parte de la organización (por ejemplo, la cúpula de la federación de Madrid o Izquierda Abierta, de Llamazares) trató de evitar cualquier posibilidad de entendimiento, echando las culpas a quienes veían en la unión una oportunidad para transformar la sociedad.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Pero no solamente allí había lucha interna: En Podemos, dos de los grupos fundadores entraron en batalla para tomar en soledad el timón de la organización. En Vista Alegre uno de los dos se erigió campeón. Como consecuencia, el bando perdedor (Izquierda Anticapitalista) se disolvió como partido en enero de 2015, pasando a ser un movimiento organizado dentro de Podemos. En Vista Alegre también se estableció también una hoja de ruta fija y estricta que asegurase al bando victorioso el control del timón.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Mientras la izquierda se dinamitaba ella sola, los medios de comunicación, fieles a sus amos, reestructuraron sus tácticas de la siguiente forma:</span></span>
</p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Continuar con el acoso y vacío a Izquierda Unida (fue presentada como perdedora, pese a haber pasado del 3,8% de los votos al 10%)</span></span>
    </p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Sobre-exponer a Podemos, de forma que generase fanáticos en uno y en otro sentido. Se trataba de desgastar la imagen de este nuevo partido para que pasase de ser «algo nuevo» a «algo viejo-nuevo», y como efecto adicional pasar voto útil de Izquierda Unida a Podemos, lo cual paradójicamente perjudica ambos por la ley electoral.</span></span>
    </p>

  * <p align="JUSTIFY">
      <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Buscar un nuevo candidato para reemplazar o debilitar a Podemos. En este caso se empleó Ciudadanos, un partido político fundado en Cataluña en 2006, con una ideología de extrema derecha económica, pero algo más moderado en cuanto a lo social y dispuesto a dar el salto a la fama.</span></span>
    </p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Para Podemos, las elecciones autonómicas y municipales de 2015 suponían un gran peligro para su objetivo de tomar La Moncloa. Sufrirían situaciones que ya ha vivido IU en las que, incluso absteniéndose, se da el gobierno a un PSOE vendido o a un PP retrógrado, siendo mala cualquier posible decisión. Además existe el riesgo de oportunistas incorporados a sus filas para aprovechar el tirón electoral (cosa que ha ocurrido muy claramente en Ciudadanos).</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Por todo ello, decidió «abandonar» las municipales y centrarse en las autonómicas, participando en el primer caso dentro de Candidaturas de Unidad Popular, algo en lo que también estaba trabajando Izquierda Unida. En lo municipal, allí donde Izquierda Unida y Podemos han aceptado confluir junto a más participantes, el resultado ha sido un éxito rotundo (hay que destacar también que, pese a que una facción de IU-CM presentó candidatura, Ahora Madrid fue apoyada tanto por IU como por el PCE). Donde no se consiguió confluir, habiendo dos o hasta tres candidaturas de «unidad» popular en la misma ciudad, el éxito se diluyó parcial o completamente.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Las elecciones autonómicas supusieron un fracaso. Incluso en los feudos donde Podemos tenía más fuerza, no pasó de ser la tercera fuerza en votos. En Aragón, la iniciativa Ganar Aragón no llegó a conformarse entre otras cosas por el rechazo de Podemos a la misma, y la dispersión de votos ha evitado que alcanzase el segundo puesto. En todas las autonomías el bipartidismo ha resistido el golpe relativamente bien, y todavía se mantiene en pie.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;"><b>Conclusiones</b></span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: DejaVu Sans,sans-serif;"><span style="font-size: small;">Podemos debe ser humilde y aprender de los resultados si realmente quiere transformar la sociedad. Paradójicamente, la actitud de soberbia que muestra Podemos fue criticada por ellos no hace mucho sobre Izquierda Unida. Si Podemos continúa insistiendo en que se sumen todos bajo su «paraguas» acabará fracasando, ya que quienes no se acojan por su izquierda tienen un suelo electoral que se no se dejará vender. Jugar con el voto útil será más perjudicial que beneficioso con la ley electoral actual. La confluencia por la izquierda de Podemos está en marcha y se generará para las elecciones generales con o sin ellos. Aunque el resultado de ambos escenarios será muy diferente.</span></span>
</p>