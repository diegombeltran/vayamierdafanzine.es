---
title: 'Proximamente: La masturbación'
author: Neurona
type: post
date: 2014-09-14T10:50:45+00:00
url: /proximamente-la-masturbacion/
categories:
  - Blog
tags:
  - masturbación

---
A día y hora de hoy, está decidido disponer de una sección en cada número de este fanzine, llamada «Sección temática».

En el próximo número, el tema seleccionado es «La masturbación». Tres redactores fijos de esta humilde publicación serán los encargados de escribir un artículo por cerebro al respecto.

Os dejamos un mini montaje para ir abriendo boca.

[<img loading="lazy" class="alignleft size-full wp-image-851" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/MONTAJE-LA-MASTURBACIÓN.jpg" alt="La mastubación ¡Vaya Mierda! Fanzine" width="635" height="477" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/MONTAJE-LA-MASTURBACIÓN.jpg 635w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/MONTAJE-LA-MASTURBACIÓN-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/MONTAJE-LA-MASTURBACIÓN-399x300.jpg 399w" sizes="(max-width: 635px) 100vw, 635px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/MONTAJE-LA-MASTURBACIÓN.jpg