---
title: Lista de Fanzines ¡Vaya Mierda! a la venta en el Salón cómic Zaragoza 2014
author: Neurona
type: post
date: 2014-12-10T07:44:32+00:00
url: /lista-de-fanzines-vaya-mierda-la-venta-en-el-salon-comic-zaragoza-2014/
categories:
  - Blog

---
Hola a tod@s,

Os mostramos un listado de los Fanzines que pondremos a la venta en el Salón del cómic de Zaragoza que comienza el viernes 12-12-2014.

La tirada es de unos 29 ejemplares de cada uno y el precio de 0,50€/unidad o 1,20€ si se adquieren 3.

**Lista de Fanzines ¡Vaya Mierda! a la venta Salón cómic Zaragoza 2014: (Orden desídemado)  
** 

<table width="465">
  <tr>
    <td width="465">
      fanzine abril 2012
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine abril 2011
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine agosto 2012
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine agosto-septiembre 2011
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine febrero 2012
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine febrero 2013
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine junio 2011
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine abril 2011
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine febrero 2011
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine septiembre 2014
    </td>
  </tr>
  
  <tr>
    <td>
      fanzine noviembre 2014
    </td>
  </tr>
</table>

[<img loading="lazy" class="alignleft size-large wp-image-818" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-767x1024.jpg" alt="fanzine gratuito en nuestra sede" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede.jpg