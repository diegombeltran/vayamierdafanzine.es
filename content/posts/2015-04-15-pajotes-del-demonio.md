---
title: Pajotes del demonio
author: Neurona
type: post
date: 2015-04-15T18:35:51+00:00
url: /pajotes-del-demonio/
categories:
  - Blog

---
El director de esta revista debe estar algo chalado con estos temas que se le ocurren. Ahora hay que hablar de pajotes. Pero bueno, el tema no es malo y se presta a debate desde múltiples perspectivas. Ignoro los que escogerán mis compañeros de sección, pero yo me voy a decantar por los aspectos sociales relacionados con la masturbación. ¡¡Amo la humanidad!! o más bien, en realidad amo la diversidad sana que genera la humanidad, porque la humanidad como tal sujeta a un análisis concienzudo se hace merecedora de un genocidio selectivo que podría ser materia de análisis en otro número de este nuestro reducto literario marginal. Así que vamos al tema.

La dimensión racional del homo sapiens y su propia consciencia de su sexualidad a diferencia del resto de seres vivos nos ofrece dos líneas de desarrollo diferentes. Por un lado, somos capaces de subyugar a nuestra razón la urgencia biológica de la reproducción. No son los instintos y la efervescencia hormonal lo que únicamente nos domina, sino que somos capaces de planificar el cómo y el cuándo de la generación de nuevos seres a partir de nuestro material genético. Como ocurre con todo en este maremágnum social, unos individuos más que otros, con más o menos lógica. Pero la capacidad existe.

Por otro lado esa misma autoconsciencia nos lleva a subordinar a voluntad la funcionalidad reproductora de la sexualidad para buscar el placer en sí de la experiencia sexual. El medio se convierte en fin, algo que sólo el ser humano y algunas especies de monos realizan.

De momento es todo bastante sencillo y básico, bastante obvio si lo pensamos bien. Pero el ser humano además de razón también tiene una dimensión moral. Ese propio uso egoísta de la sexualidad le lleva a plantearse si es moralmente aceptable o si es reprobable. Igual ocurre con las relaciones sexuales con una pareja. La búsqueda del medio como fin, ignorando la funcionalidad original plantea dilemas.

Analizando desde un punto de vista meramente biológico no parece haber grandes problemas. El desperdicio de fluido seminal en el caso del hombre o algunos fluidos en el caso de la mujer es fácilmente asimilable por el medio natural. Unos bichos aman lo que otros cagan. Los protagonistas pueden generar más de esos fluidos con sus organismos. Y la práctica y conocimiento del cuerpo de uno mismo no puede sino redundar en mayores probabilidades de éxito en el momento en el que se deseen tener relaciones sexuales para reproducirse. La biología no entiende de moralidad ya que se auto-regula. Es por tanto el propio ser humano el que se monta la película, para bien o para mal puesto que es el único ser vivo, irónicamente, incapaz de auto-regularse con el resto del medio natural.

“Si te haces pajas te saldrán granos”. Probablemente sea el ejemplo más inocente de como funciona el sistema de chantaje moral que existen en muchas sociedades humanas. La sexualidad de esta forma puede convertirse en algo desde poco recomendable a algo demoníaco y perverso.

Voy a intentar entrar lo menos posible en expresar un juicio moral sobre el tema. Este fanzine es para que cada uno saque sus propias conclusiones. Pero lo que sí está claro es que una sexualidad reprimida lo será con considerables dificultades y grandes dosis de fuerza de voluntad del individuo, y le traerá una considerable cantidad de insatisfacción o incluso contribuir a diversas patologías y a actos delictivos sin duda profundamente condenables. Por contra una sexualidad vivida con plenitud tiene probados beneficios para la salud, genera autoconfianza y reduce el estrés. Ahora depende de cada persona construirse unos valores morales con respecto a la sexualidad.

<p style="text-align: right;">
  Nolfy
</p>