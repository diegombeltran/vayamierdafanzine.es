---
title: A girl walks home alone at night
author: Neurona
type: post
date: 2015-09-09T11:33:27+00:00
url: /a-girl-walks-home-alone-at-night/
categories:
  - Blog

---
En este número, la humilde parcela de 3200 caracteres que poseo está de celebración. Cuando estaba a punto de retirarme definitivamente como portavoz y defensora de mamarrachadas audiovisuales de serie B y películas independientes con una audiencia en números rojos; entonces, en ese momento, llegó ella.  
Ana Lily Amirpour, de ascendencia iraní y reptiliana es, sin duda, el mayor descubrimiento cinematográfico que he hecho desde que me quitaron la ortodoncia.  
Hasta la fecha, he visto, por fortuna y desgracia, muchas cosas. Pero me atrevería a decir que ninguna otra se asemeja a lo que os presento hoy. Esta mujer, escondida bajo una camiseta de David Lynch tres tallas más grandes de lo que se ajusta a su fisonomía, es la mente brillante y perturbada que tanto tiempo estábamos esperando. Es por esta valiosa razón que os pido con todo mi ser que dejéis de leerme en diagonal y prestéis atención.  
A girl walks home alone at night es el híbrido con el que Amirpour ha debutado en el mundo del largometraje. Con apenas un añito de edad, este “espagueti western vampírico iraní” ya ha asegurado a su progenitora un hueco en la historia del cine.  
La película combina de forma extraordinaria lo nunca visto y lo que todos hemos visto miles de veces. Me explico. Todo el amasijo compuesto por trama, contexto y el desarrollo de los personajes goza de una incuestionable originalidad. Sin embargo, todos aquellos espectadores, que hayan visto más de 50 películas en su vida, adivinarán la cantidad de influencias que erosionan este proyecto. Tarantino, David Lynch, Jorodowsky, Bruce Lee, Abbas Kiarostami, Ninja de Die Antwoord (lo sé), el estilo de James Dean en Giant, la devastación de Gummo y el vestuario de Madonna en el videoclip Papa don’t preach son algunas de las millones de referencias que encontramos en la película de Amirpour.  
Me estoy enrollando y ni siquiera sabéis de qué va. Lo siento, he estado alargando el redoble mucho pero era necesario.  
A girl walks home alone at night muestra al espectador cómo las miserables y solitarias vidas de los habitantes de “Ciudad Mala” mejoran increíblemente gracias a la intervención moralmente discutible de un vampiro con burka.  
Lo sé, es un vampiro con burka, qué pasada. Sin embargo, no son los colmillos de esta mujer lo verdaderamente importante del film.  
A girl walks home alone at night es un homenaje a todas aquellas personas que la sociedad margina al considerarlas un fracaso e incluso una vergüenza. La película olvida la “compasión marginal” e inyecta a sus protagonistas la fuerza que necesitan para levantarse y reírse de sus hostigadores.  
Esa es la esencia que da título a la película. Advertir de la fuerza que esconden aquellos seres que parecen más vulnerables. Es decir, avisar a los que se consideran los dioses del universo de que las chicas que van a casa solas por la noche pueden arrancarles la yugular.  
La crudeza del film es suavizada gracias al romance entre la vampira y el James Dean iraní, cuya conexión es el punto álgido de la trama. Este romance es extraordinario, no por la absurdez que entraña, sino por la autenticidad de las emociones reflejadas.  
Si todavía tengo alguna posibilidad de que veáis A girl walks home alone at night, sólo me queda desearos que os maravilléis tanto como una servidora. Y ya sabéis, estas películas son de ver dos veces, así que ánimo valientes.

Patry Torres