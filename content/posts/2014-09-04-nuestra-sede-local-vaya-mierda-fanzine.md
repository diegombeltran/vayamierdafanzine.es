---
title: Nuestra sede-Local ¡Vaya Mierda! Fanzine
author: Neurona
type: post
date: 2014-09-04T07:16:17+00:00
url: /nuestra-sede-local-vaya-mierda-fanzine/
categories:
  - Blog

---
Poco a poco añadimos detalles a nuestra sede oficial. Un humilde rincón físico de divagaciones y espiritualidades difusas donde se reunen supuestos seres humanos con bondades y rarezas.

**El local de ¡Vaya Mierda! Fanzine &#8211; Zaragoza (España)**

[<img loading="lazy" class="alignleft size-large wp-image-818" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-767x1024.jpg" alt="fanzine gratuito en nuestra sede" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][1]Puedes recoger nuestro fanzine gratuito en papel en nuestra sede. Contacta con nosotros antes. Si hay cervezas, te invitaremos a una. El auténtico Fanzine en blanco y negro con una grapa en medio.

[<img loading="lazy" class="alignleft size-full wp-image-809" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-comun2-sofa-mesita-vermú.jpg" alt="Zona vermú 2 vaya mierda fanzine" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-comun2-sofa-mesita-vermú.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-comun2-sofa-mesita-vermú-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-comun2-sofa-mesita-vermú-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" />][2]

La nueva adquisición vayamierdil. Zona común 2, compuesta por sofá-cama por si algún ser humano se encuentra indispuesto y tiene que hacer noche en la sede, acompañado por una mesita de cristal y espejo para verse reflejada la cara mientras degusta algún compuesto líquido o sólido. También se le une al pack un ordenador vintage para escuchar spotify (versión gratuita).

[<img loading="lazy" class="alignleft size-large wp-image-837" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-cerrada-556x1024.jpg" alt="nevera-grande-cerrada sede vaya mierda fanzine" width="556" height="1024" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-cerrada-556x1024.jpg 556w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-cerrada.jpg 800w" sizes="(max-width: 556px) 100vw, 556px" />][3]

La edad del hielo llegó.Por fin vamos a poder conservar esta preciosa agua sólida en la sede. Nos será muy útil para diferentes fines.

[<img loading="lazy" class="alignleft size-large wp-image-839" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-abierta-725x1024.jpg" alt="nevera-grande-abierta sede vaya mierda fanzine" width="584" height="824" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-abierta-725x1024.jpg 725w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-abierta-212x300.jpg 212w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-abierta.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][4]

Nuestro aparato de producción de frío con acotaciones (low-cost), dispone de una parte superior donde albergar las bolsas de hielo para mezclar con espirituosidades variopintas y una parte inferior que será llenada con una mixturanza de manjares provocadores de excitaciones agudas del aparato pensil.

[<img loading="lazy" class="alignleft size-large wp-image-782" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-con-rincón-del-café-767x1024.jpg" alt="Sede fanzine vaya mierda" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-con-rincón-del-café-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-con-rincón-del-café-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-con-rincón-del-café.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][5]

Zona común 1: Mesa para vermutes, sofá ergonómico, rincón del café y té y adornos con las mejores portadas del fanzine pegados en la pared con blu-tack.

[<img loading="lazy" class="alignleft size-large wp-image-784" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-767x1024.jpg" alt="sede local vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][6]

Otra vista de la zona común 1: En ella se aprecia la mesa de los vermutes y la exposición de las best portadas del fanzine pegoteadas por la pared.

[<img loading="lazy" class="alignleft size-large wp-image-785" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-b-767x1024.jpg" alt="Local vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-b-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-b-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-b.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][7]

Más vistas de la zona común 1: Se aprecia la mini nevera y el microgüeiber. También hace acto de presencia el sofá ergonómico. A la izquierda, sobre la estantería, con fondo azul, se encuentra un collage de gran valor, realizado por Neurona en 1995. Más adelante podrá disfrutar de su esplendor en una imagen en primer plano.

[<img loading="lazy" class="alignleft size-large wp-image-786" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-2-767x1024.jpg" alt="Local vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-2-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-2-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-2.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][8]

Zona común 2: Sillas y mesa con ordenador para consultar la web de la revista Pronto.

[<img loading="lazy" class="alignleft size-large wp-image-787" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/sección-café-y-té-767x1024.jpg" alt="sección café y té" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/sección-café-y-té-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/sección-café-y-té-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/sección-café-y-té.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][9]

Rincón del café y el té. No hay nada más que decir.

[<img loading="lazy" class="alignleft size-large wp-image-788" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/puerta-baño-767x1024.jpg" alt="Cartel de entrada al baño en la sede de fanzine vaya mierda" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/puerta-baño-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/puerta-baño-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/puerta-baño.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][10]

Cartel de entrada al baño. Se supone que está en Fabla aragonesa, pero seguro que hay alguna errata.

[<img loading="lazy" class="alignleft size-large wp-image-790" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-entrada-2-767x1024.jpg" alt="Local vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-entrada-2-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-entrada-2-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-entrada-2.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][11]

Pasillo de entrada: Se aprecian diversas portadas del Fanzine ¡Vaya Mierda! pegoteadas por la pared junto a obras de arte auténticas.

[<img loading="lazy" class="alignleft size-large wp-image-791" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-de-entrada-767x1024.jpg" alt="local vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-de-entrada-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-de-entrada-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-de-entrada.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][12]

Otra vista del pasillo de entrada: Fotos en la pared de antiguas presencias del fanzine ¡Vaya Mierda! en el salón del cómic de Zaragoza junto a fotos del Pirineo aragonés.

[<img loading="lazy" class="alignleft size-large wp-image-792" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/higiene-y-espejo-767x1024.jpg" alt="El baño de vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/higiene-y-espejo-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/higiene-y-espejo-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/higiene-y-espejo.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][13]Zona de limpieza corpórea junto a pieza colgada de la pared para verte reflejado.

[<img loading="lazy" class="alignleft size-large wp-image-793" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/evacuador-de-sustancias-767x1024.jpg" alt="Taza del váter de vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/evacuador-de-sustancias-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/evacuador-de-sustancias-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/evacuador-de-sustancias.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][14]

Zona de evacuación.

[<img loading="lazy" class="alignleft size-large wp-image-794" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/copas-gin-tonic-767x1024.jpg" alt="copas gin tonic en sede del fanzine vaya mierda" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/copas-gin-tonic-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/copas-gin-tonic-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/copas-gin-tonic.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][15]Set de Gin Tonic cuidadosamente guardadicos.

[<img loading="lazy" class="alignleft size-large wp-image-795" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/jarras-bírricas-767x1024.jpg" alt="Jarras de cerveza en sede de vaya mierda fanzine" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/jarras-bírricas-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/jarras-bírricas-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/jarras-bírricas.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" />][16]El líquido base humano tiene cabida en estas jarras.

[<img loading="lazy" class="alignleft size-full wp-image-796" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/collage-vintage.jpg" alt="collage realizado por neurona en 1995" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/collage-vintage.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/collage-vintage-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/collage-vintage-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" />][17]Collage datado en 1995 realizado por Neurona. En él se oculta a modo de «dónde está wally» una uña del dedo gordo del pie derecho real. Si la encuentras, te invitamos a una jarra.

[<img loading="lazy" class="alignleft size-large wp-image-806" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada-1024x561.jpg" alt="cartel-fachada-sede-fanzine-vaya-mierda" width="584" height="319" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada-1024x561.jpg 1024w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada-300x164.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada-500x274.jpg 500w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada.jpg 1452w" sizes="(max-width: 584px) 100vw, 584px" />][18]

El cartel pegado con silicona en la fachada que indica que en el interior del local se concentran variedades insólitas de discernires variopintos.

[<img loading="lazy" class="alignleft size-full wp-image-807" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fachada-sede.jpg" alt="fachada-sede-vaya-mierda-fanzine" width="1000" height="709" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fachada-sede.jpg 1000w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fachada-sede-300x212.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fachada-sede-423x300.jpg 423w" sizes="(max-width: 1000px) 100vw, 1000px" />][19]

Fachada de la sede. El diseño y pintado del graffiti de la fachada fue encargado a un artista manierista-abstracto radicado en <span id="article_image_description">NSK (Neue Slowenische Kunst o Nuevo Arte Esloveno)</span>. <a title="NSK" href="http://www.elcultural.es/noticias/ARTE/3052/El_NSK_Nuevo_Arte_Esloveno_a_revision_en_la_Tate" target="_blank">[Ver más sobre NSK]</a>

[<img loading="lazy" class="alignleft size-full wp-image-824" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/calefactor.jpg" alt="calefactor sede vaya mierda fanzine" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/calefactor.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/calefactor-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/calefactor-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" />][20]Las nuevas adquisiciones para pasar el duro invierno de mañolandia. Un par de calefactores de aire gemelos low-cost. Con suerte durarán más de 3 usos.

[<img loading="lazy" class="alignleft size-large wp-image-835" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/refrigeración-low-cost-532x1024.jpg" alt="Regrigerador vayamierdil" width="532" height="1024" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/refrigeración-low-cost-532x1024.jpg 532w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/refrigeración-low-cost-156x300.jpg 156w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/refrigeración-low-cost.jpg 800w" sizes="(max-width: 532px) 100vw, 532px" />][21]

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

Disponemos de 2 artilugios refrigeradores de bajo coste instalados en la sede. Las axilas quizá evacúen menos gracias a estas adquisiciones.

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fanzines-sede.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-comun2-sofa-mesita-vermú.jpg
 [3]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-cerrada.jpg
 [4]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/nevera-grande-abierta.jpg
 [5]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-con-rincón-del-café.jpg
 [6]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1.jpg
 [7]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-1-b.jpg
 [8]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/zona-común-2.jpg
 [9]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/sección-café-y-té.jpg
 [10]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/puerta-baño.jpg
 [11]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-entrada-2.jpg
 [12]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/pasillo-de-entrada.jpg
 [13]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/higiene-y-espejo.jpg
 [14]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/evacuador-de-sustancias.jpg
 [15]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/copas-gin-tonic.jpg
 [16]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/jarras-bírricas.jpg
 [17]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/collage-vintage.jpg
 [18]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/cartel-fachada.jpg
 [19]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/fachada-sede.jpg
 [20]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/calefactor.jpg
 [21]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/refrigeración-low-cost.jpg