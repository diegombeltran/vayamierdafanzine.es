---
title: Que vuelva el Rey León
author: Neurona
type: post
date: 2015-04-24T18:29:26+00:00
url: /que-vuelva-el-rey-leon/
categories:
  - Blog

---
Que vuelva el Rey León que mola más.

Harán un #ReferendumQueVuelvaElReyLeón, un twitter, y un change.org solicitando la petición de que vuelva el Rey de la Selva.

Porque oiga, reyes hay muchos, de todos los colores, ¡qué divertido! Rompa el código de la sangre azul y pongan al Rey León como Jefe del Estado Mayor.

Vendrá con un séquito más minoritario, tan sólo Timón, Pumba y su chica. El Rey León aportará su carisma y gran hacer. En su currículum, chorrocientos años al servicio de la Selva, con buena imagen, y rigor.

Teníamos al Rey del Pop, al Rey de Picas, el Rey del Pollo, incluso el Príncipe de Bekeler. Prince, Reyes de la pista, y del bacalo. Reyes de los judíos sin olvidarnos del gran Elvis, rey del bailoteo. También hay princesas de la boca de fresa, princesa del guisante.

¡Y ahora tenemos al Rey León! ¡Precio especial!

Porque mola más. Porque vendrá con líder espiritual incluido, Rafiki, porque comerá larvas y algún manjarcito, pero nunca cenas opíparas, porque dirá no a los viajes con empresarios por aburridos, y sí a la lucha por su pueblo con valentía y garra.

Que vuelva el Rey León, por favor. Pero Simba no, que tiene menos carácter ni dicen que cuenta los chistes y juegos de palabras de su padre. Y además es del Aleti.

**_Javi Burguinho_**