---
title: La Princesa de Kiev
author: Nolfy
type: post
date: 2011-06-16T07:59:35+00:00
url: /la-princesa-de-kiev/
jabber_published:
  - 1308214776
categories:
  - Blog

---
Tuve una compañera que se llamaba Olga. Era ucraniana. A pesar de tener unos cuarenta años y haber tenido un hijo, lucía un físico envidiable: delgada, alta, rubia, pequeños y chispeantes ojos azules y un rostro cálido y risueño. Sin ser bella resultaba atractiva. Según sé, sigue casada con un español que la hace feliz y continúa viviendo a cinco minutos de mi casa dos calles más arriba. Su hijo Boris es de un matrimonio anterior.

Como alguna compañera más de las que han pasado por mi historia laboral, Olga era extranjera pero escapa a los cánones. Olga es ucraniana, pero no es cualquier ucraniana. Porque Olga, a pesar de que ganaba la vida honradamente limpiando habitaciones en el hotel, llegaba siempre a trabajar a las cinco y media de la mañana envuelta en sus botas de forro y en su abrigo largo, tocada con un elegante gorro gris que junto a su porte la hacían parecer, y estoy seguro de que seguirá siendo así, una dama distinguida salida de un perdido palacio nevado de Kiev, su ciudad natal. Por si fuera poco, regalaba sonrisas y trato cariñoso cuando recogía las llaves antes de ir a enfundarse el gris uniforme&#8230;en suma, muy lejos de la típica dama del este fría, brusca y seca que se ve en las películas. 

Una vez me contó que allá en Ucrania, su exmarido había vendido el piso en el que vivían juntos: había cogido el dinero y había desaparecido. Se había enterado por casualidad, por unos vecinos de su madre. Una pareja joven se había mudado hacía pocas semanas. El problema del asunto es que ese piso seguía siendo suyo también. Estaba preocupada, porque no podía gestionar un viaje a Ucrania tan rápido aun pidiendo vacaciones y tenía que buscar contra reloj un abogado y dar poderes legales a su madre para poder solucionar todo el embrollo. Para más dificultad, en Ucrania hacía un frío de 35 grados bajo cero aquellas semanas. Tan sólo unas botas de pieles adecuadas al clima le valían allí el equivalente a unos 80€. Un abrigo de pieles, unos 100€. Me confesó que su economía no le permitía semejante gasto de repente, junto a costes de abogados y el precio del vuelo aun cogido de bajo coste.  
Dos semanas después volví a hablar con ella. Estaba muy contenta. El asunto se había solucionado casi solo: al ir su madre al registro los funcionarios habían constatado que el piso era también de su propiedad y de oficio habían llamado a su marido y a los inquilinos, declarando el contrato de venta nulo. La cara de los inquilinos al saber que habían pagado un dineral y que les habían tomado el pelo debió ser también interesante.

Pero a pesar de que le habían pillado, el exmarido de Olga no quería soltar la parte de pasta que a ella por derecho le correspondía. No a ella.

Al final, por mediación de la madre de Olga habían abierto dos cuentas. La mitad del dinero sería para él, la otra mitad para el hijo en común, Boris, que poco quería saber desde hace años de su padre biológico. El sinvergüenza jetudo cedió y Olga autorizó la venta del piso a través de los poderes legales conferidos a su madre. Tan sólo quedaba ya poder traer el dinero a España, y pagar las posibles tasas por importación de divisas.

Cuando acababa la historia, Olga me cogió la mano en un arranque de cercanía que no era raro en ella. Estaba contenta, se había solucionado todo, el dinero le venía bien y ya no le quedaban lazos con su exmarido, pero lo que de verdad le dolía era que ese cerdo se había aprovechado del pasado matrimonio y había vendido el piso que antaño había sido de su abuelo y después de su madre. El piso donde había sido niña y donde había crecido. Las raíces de su familia. Sus recuerdos.