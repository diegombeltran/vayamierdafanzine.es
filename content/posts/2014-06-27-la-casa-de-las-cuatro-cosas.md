---
title: La casa de las cuatro cosas
author: Neurona
type: post
date: 2014-06-27T08:43:00+00:00
url: /la-casa-de-las-cuatro-cosas/
categories:
  - Blog
tags:
  - Casa 4 cosas Zaragoza

---
[<img loading="lazy" class="alignleft size-medium wp-image-658" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/Garitos-bar_casa_4_cosas_1-elegir-una-sola-foto-dos-serán-mucho-233x300.jpg" alt="Casa de las 4 cosas Zaragoza" width="233" height="300" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/Garitos-bar_casa_4_cosas_1-elegir-una-sola-foto-dos-serán-mucho-233x300.jpg 233w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/Garitos-bar_casa_4_cosas_1-elegir-una-sola-foto-dos-serán-mucho-797x1024.jpg 797w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/Garitos-bar_casa_4_cosas_1-elegir-una-sola-foto-dos-serán-mucho.jpg 1359w" sizes="(max-width: 233px) 100vw, 233px" />][1]Hay que intentar hacer aquellas cosas que nos hacen sentir bien. Aunque claro, el que menos necesita para encontrar el placer, es el más feliz.

Por esa razón me desplazo dos días a la semana siempre que mi agenda me lo permite a practicar la vida social con mis amistades en compañía de un espirituoso líquido.

Un día, camino de dicho acto social, me paré en un semáforo y giré la cabeza hacia la izquierda. Me percaté de la existencia de un cartel que indicaba el nombre de un bar: La casa de las cuatro cosas. Me llamó la atención, por lo que continué investigando con la mirada la fachada del local. Descubrí otro cartel realizado con un rotulador en el que se podía leer: «Ya estamos en Facebook». Inmediatamente decidí que este lugar iba a aparecer en ¡Vaya Mierda! Fanzine.

Esa misma semana convoqué una actividad «vayamierdil» para acudir a degustar los manjares del establecimiento en cuestión.

El bar dispone de poquitas mesas y un reservado VIP. La barra se encuentra a la derecha y efectivamente disponen de una escueta carta con exquisitos placeres culinarios que cocinan a la plancha o fritos.

El comité «fanzinero» degustó: pimientos de Padrón, Sepia, Sardinas plancha y demás placeres orgánicos para masajear el paladar, perfectamente elaborados y, por supuesto, la bebida de la gente alegre: la cerveza.

El propietario de este humilde pero auténtico paraje es un profesional de su oficio. Simpático, atento y cordial.

Precio muy interesante, acogedor, campechanismo profundo sin llegar a cutre y productos exquisitos.

Investigando a través de San Google me leí que buscan franquiciar dicho modelo de negocio. Materia prima de primerísimo orden cocinada a la plancha. Cuatro cosas, pero muy buenas.

**_La casa de las cuatro cosas_**

**_Paseo Mª Agustín 86. Junto Plaza Europa._**

**_Zaragoza (España)_**

<p style="text-align: right;">
  <em>Neurona</em>
</p>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/Garitos-bar_casa_4_cosas_1-elegir-una-sola-foto-dos-serán-mucho.jpg