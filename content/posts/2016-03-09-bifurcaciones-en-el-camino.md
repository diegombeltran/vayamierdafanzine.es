---
title: Bifurcaciones en el camino
author: Neurona
type: post
date: 2016-03-09T10:23:19+00:00
url: /bifurcaciones-en-el-camino/
categories:
  - Blog

---
<p align="CENTER">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: medium;">por Teletubi</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">No creo en el destino. Mediante mis decisiones soy dueño de mi futuro, al igual que lo soy del presente en el que vivo y de mi pasado.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">La vida es, al fin y al cabo, un largo recorrido en el que se presentan innumerables bifurcaciones y un único camino. Pero ese camino no es algo predestinado, sino una consecuencia materializada de las elecciones tomadas. A diario, desde que nos levantamos hasta que nos acostamos, tomamos miles de decisiones que, en su práctica totalidad, no advertimos en el momento. Cualquier acción tomada, cualquier acción no tomada, son el resultado del análisis que hemos realizado consciente o inconscientemente y de la opción que hemos considerado preferible con esa información.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">Por ello, no me arrepiento de nada de lo que he hecho a lo largo de mi todavía no larga vida. He hecho daño y también he ofrecido consuelo. He cometido aciertos y errores. Pero no siento arrepentimiento por ninguna acción por pura responsabilidad, porque todas ellas fueron fruto de lo que consideré oportuno en cada momento. Por supuesto, me avergüenzo de muchos de mis actos, y también reconozco y lamento las múltiples equivocaciones que he cometido además de las que seguro que cometeré. Si pudiera volver al pasado muy probablemente actuaría de una forma distinta. Pero ahí es donde están la trampa y la paradoja: Conociendo información futura todo parece más fácil, pero también es diferente. «Yo» no sería «yo».</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">Vienen a mi mente algunas bifurcaciones en mi camino que marcaron claramente el rumbo de mi vida. Algunas fueron claras y evidentes, como cuando tuve que decidir en qué ciudad estudiar la primera carrera. Otras, por el contrario, fueron construyendo mi futuro de una forma silenciosa y encubierta hasta que una se constituyó en desencadenante. Por ejemplo, recuerdo un concierto a partir del cual cambió mi forma de activismo en la vida (concretamente, pasar de la inacción a la acción). Allí no hice nada más que disfrutar de la música y el ambiente, pero ocurrió algo que hizo que diera un paso una semana después, y un segundo y definitivo días más adelante. La base y la naturaleza de la inquietud ya las tenía anteriormente, pero creo que ir a ese concierto fue lo que marcó la diferencia entre comenzar en ese momento o, por ejemplo, hacerlo dos años más tarde.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">Es por eso mismo por lo que me encanta mirar hacia atrás y contemplar desde una perspectiva ajena lo que es mi vida y cómo la he ido diseñando. También aprovecho para fantasear como sólo la imaginación permite sobre qué habría sucedido si hubiese actuado de otra forma allí donde parece que había alternativas mejores. Luego la cabeza se me va del todo y pienso que, si la teoría de universos múltiples es cierta, otro «yo» puede haberlas escogido y estar imaginando qué habría sido de su vida si «él» hubiese hecho lo mismo que yo.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">El resultado es que hagamos lo que hagamos, siempre es tentador pensar que habría sido mejor haber actuado de otra forma. Eso en sí es algo bueno, ya que permite la autocrítica constructiva y consolidar el crecimiento como persona. No obstante, también conlleva el riesgo de que nos impida vivir el presente arrepintiéndonos constantemente del pasado.</span></span>
</p>

<p align="JUSTIFY">
  <span style="font-family: 'DejaVu Sans', sans-serif;"><span style="font-size: small;">Casi nunca elegimos la mano de cartas que nos toca, pero nosotros somos quienes las jugamos. No tengo miedo a cambiar mi futuro, así como abrazo el pasado que inexorablemente forma parte de mí.</span></span>
</p>