---
title: Chistes absurdos genialmente dibujados
author: Neurona
type: post
date: 2015-05-14T12:10:48+00:00
url: /chistes-absurdos-genialmente-dibujados/
categories:
  - Blog

---
Desde la redacción de ¡Vaya Mierda! Fanzine sabemos que no podéis aguantar más sin captar nuestras eyaculaciones neuronales. Por ello os dejamos aquí un anticipo típico vayamierdil. Una forma gratis de ayudar a este humilde Fanzine, es compartir cosas moco ésta. Aunque también buscamos patrocinadores que paguen 20€ por número, unos 80€-100€ al año. ¡Somos indigentes camuflados!

Sería de gran ayuda para nuestro humilde proyecto, contar con empresas y negocios que deseen anunciarse con nosotros. Por solamente 20€ por número te colocaríamos un banner en cada número y un enlace a tu página web.

[<img loading="lazy" class="alignleft size-full wp-image-1009" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web.jpg" alt="chiste absurdo genial" width="700" height="974" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web.jpg 700w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web-216x300.jpg 216w" sizes="(max-width: 700px) 100vw, 700px" />][1] [<img loading="lazy" class="alignleft size-full wp-image-1010" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web2.jpg" alt="chiste absurdo genial" width="700" height="963" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web2.jpg 700w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web2-218x300.jpg 218w" sizes="(max-width: 700px) 100vw, 700px" />][2]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2015/05/bolibic-para-web2.jpg