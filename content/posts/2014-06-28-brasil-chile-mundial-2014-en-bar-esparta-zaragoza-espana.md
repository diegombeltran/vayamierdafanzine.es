---
title: Brasil-Chile Mundial 2014 en Bar Esparta (Zaragoza-España)
author: Neurona
type: post
date: 2014-06-28T21:44:44+00:00
url: /brasil-chile-mundial-2014-en-bar-esparta-zaragoza-espana/
categories:
  - Blog
tags:
  - cervezas
  - fútbol

---
El intrépido grupo de reporteros de ¡Vaya Mierda! Fanzine ha acudido al bar Esparta de Zaragoza (España) para hacer vida social con uno de los colectivos brasileños de la ciudad del cierzo con el fin de disfrutar de un gran partido internacional que se resolvió en los penaltis y de muy buenas curvas.  
Nosotros íbamos con Chile, pero bueno, al menos lo pasamos muy bien en una maño-brasileña tarde.