---
title: Bar Bonanza
author: Neurona
type: post
date: 2015-06-28T20:11:53+00:00
url: /bar-bonanza/
categories:
  - Blog

---
Era increíble, pero cierto. La realidad supera con creces la ficción. Llevo viviendo en Zaragoza desde el año 1982 y no conocí el «Bar Bonanza» hasta el año 2012 de mano de Nabey, la artista que llena de magnificencia las portadas de ¡Vaya Mierda! Fanzine.

Como buen amante de los bares y tabernas sin un orden definido en su decoración y exentos de artificiales e insulsas modas, he de decir que me gustó, haciendo que mi Yo se sintiese plácido en dicho paraje.

Bar Bonanza dispone de un variopinto repertorio de arte colocada donde hay hueco. Sin reglas aparentes. Tal y como bien indica y resume Javier Barreiro en su blog <http://javierbarreiro.wordpress.com> , este párrafo concentra la esencia de Bar Bonanza:

«Bar económico, sala de exposiciones, reducto de heterodoxos, tótem de la modernidad, almacén de objetos estrambóticos, refugio de artistas, cauce de distintas sensibilidades y estéticas, amparo de insolventes y borrachos, escenario de improvisados happenings diarios, escaparate de su tan profuso, humano, visceral y entrañable propietario, dinamizador del entorno, cajón de sastre…, el Bonanza ha sido sobre todo un espacio casi mítico para el imaginario de muchos de sus frecuentadores.»

Dicho genial establecimiento fue abierto por Manuel García Maya (Manolo) el 4 de noviembre de 1973. Manolo pagó 350.000 pesetas por el traspaso.

Al inicio, tenía que complementar el trabajo en el bar con servicios externos en banquetes. Sin embargo, su forma de ser, repleta de humor, original y siempre con respeto, atrajo a su establecimiento un público variopinto muy centrado en las bellas artes.

El gusto por la rareza que siempre ha sido el denominador común de este local, vibra en mi misma frecuencia. Por ello, si usted, estimado lector de este reducto marginal de índole bucocalcárica también se identifica con la esencia de ¡Vaya Mierda! Fanzine, le recomiendo echar una copa y un bocadillo en este oasis de humanidad e intelecto.

Manuel García Maya falleció el día 11 de octubre de 2013. (Morata de Jalón 1942- Zaragoza 2013).

Pueden documentarse al respecto de lo aquí hablado en:

<https://javierbarreiro.wordpress.com/2012/11/03/bar-bonanza-no-hay-dios-para-quien-habita-el-templo/>

[http://www.heraldo.es/noticias/ocio\_cultura/cultura/2013/10/11/ha\_muerto\_manolo\_tabernero\_galactico\_del\_bar\_bonanza\_zaragoza\_252608_308.html][1]

<http://www.elperiodicodearagon.com/noticias/escenarios/fallece-pintor-agitador-cultural-manuel-garcia-maya-71-anos_890230.html>

 [1]: http://www.heraldo.es/noticias/ocio_cultura/cultura/2013/10/11/ha_muerto_manolo_tabernero_galactico_del_bar_bonanza_zaragoza_252608_308.html