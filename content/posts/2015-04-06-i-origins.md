---
title: I origins
author: Neurona
type: post
date: 2015-04-06T11:16:44+00:00
url: /i-origins/
categories:
  - Blog

---
Antes de sacar conclusiones precipitadas sobre por qué estoy escribiendo sobre I origins , por favor, termine de leer la reseña. Si lo hace y aún así tiene ganas de darme una paliza, contacte con el staff de ¡Vaya mierda! Fanzine, los domingos de 9:00 a 14:00 me encargo de atender a gente enfurecida.

Buscando entre miles de post-its manchados de café, garabateos de bic azul en el brazo y películas apuntadas ebriamente en el cuaderno de notas de mi móvil, recordé que no había hablado todavía sobre la ganadora del premio a Mejor película en el festival de Sitges.  
I origins fue la considerada como mejor película de cine fantástico, y sí, viene de Sitges, así que debería considerarse como la más bizarra en su categoría. Vamos a ver, no sé si la crisis nos ha hecho más blandos, que todo puede ser, pero quiero recordaros que hace dos años la ganadora fue Holy Motors, una verdadera joya del cine desequilibrado. Al lado de la veterana ganadora, I origins es un drama que repasa de forma superficial las grandes cuestiones que el ser humano lleva planteándose desde que tiene consciencia.

I origins nos presenta la historia del Dr. Ian Gray, un biólogo molecular especializado en la evolución del ojo humano. Partiendo del patrón que da vida, parece ser, a la mayoría de científicos en las películas, él es un férreo ateo, dispuesto a rebatir la existencia de Dios a través de sus investigaciones. Los creyentes defienden que Dios dota a cada ser de unicidad y que por eso es imposible que haya dos ojos iguales. ¿Qué pasaría si pudiésemos crear nosotros dos ojos iguales?  
Esta sería la primera parte de la historia.  
En la segunda se suma una nueva pregunta, ¿en el caso de que exista en el mundo personas con los mismos ojos, significa que están interconectadas de alguna forma? 

Sé que dan ganas de verla, y es que realmente es una buena historia. El problema es que falta desarrollo y coherencia (o por lo menos argumentación) en ciertos aspectos y, además, hay un componente lacrimógeno excesivo, el cual impide tomarte en serio a los personajes.  
Mike Cahill, director y guionista de la película, debería haberse centrado más en toda la trama filosófico-religiosa del film y no convertir los amores del científico en la parte cuasi principal. Esto hace que el espectador esté más preocupado por el estado anímico de esta o aquella mientras dejamos que cuestiones de grave importancia para toda la historia de la humanidad pasen por alto.  
Esto último que he dicho se aplicaría sólo en el caso de que Mike Cahill hubiese querido escribir un largometraje profundo y trascendental, si sólo jugaba a hacer dramas poniendo un poco más de chicha entonces está perfecto.

Si quieres ver I origins en el caso de que nunca hayas visto películas raras o poco comunes, puede ser un buen comienzo. Si por el contrario eres un apasionado del género fantástico y de todo lo que tenga que ver con el más allá, está película te dejará un poco frío. Repito, cualquier cosa, los domingos de 9:00 a 14:00.

Patry Torres