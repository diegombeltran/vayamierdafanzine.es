---
title: El fenómeno de las descargas
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-07-02T09:44:51+00:00
url: /el-fenomeno-de-las-descargas/
jabber_published:
  - 1309599892
categories:
  - Blog

---
“Desde que todo el mundo tiene Internet, el negocio ha caído en picado” ha comentado el cancerbero de la SGUACE (Sociedad de Gestión Útil de Automóviles Caducos de España), Don Filomeno Marroncín, en entrevista exclusiva para la agencia ¡Vaya Mierda! News. Se refería -¡cómo no!- a las descargas ilegales de tapacubos, culatas, faros, llantas, válvulas, transmisiones, catalizadores, alternadores, motores de arranque, bombas de dirección y demás repuestos, que están obligando a cerrar la mayoría de desguaces por falta de compradores. “La gente sólo quiere autos de gama alta a coste cero. A este paso” razona “pronto no se verán por las calles más que descapotables clónicos. Debemos informar a la población del peligro que entraña la piratería. Porque esas piezas procedentes de descargas ilegales carecen de garantía, te pueden dejar tirado en cualquier curva. Nada que ver con las que comercializamos nosotros, que son originales”.  
[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/sguace.jpg?w=300" alt="" title="sguace" width="300" height="221" class="alignnone size-medium wp-image-122" />][1]{.broken_link}

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/sguace.jpg