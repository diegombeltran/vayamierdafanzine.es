---
title: Síntesis fotográfica Salón Cómic Zaragoza 2014
author: Neurona
type: post
date: 2014-12-16T12:49:34+00:00
url: /sintesis-fotografica-salon-comic-zaragoza-2014/
categories:
  - Blog

---
<div id="attachment_882" style="width: 810px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand2-web.jpg"><img aria-describedby="caption-attachment-882" loading="lazy" class="wp-image-882 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand2-web.jpg" alt="stand2-web" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand2-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand2-web-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand2-web-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
  
  <p id="caption-attachment-882" class="wp-caption-text">
    Stand compartido con los excelentes «Cómics y cigarrillos».
  </p>
</div>

<div id="attachment_881" style="width: 594px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56.jpg"><img aria-describedby="caption-attachment-881" loading="lazy" class="wp-image-881 size-large" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56-1024x768.jpg" alt="2014-12-13 19.55.56" width="584" height="438" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56-1024x768.jpg 1024w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56-400x300.jpg 400w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/2014-12-13-19.55.56.jpg 1280w" sizes="(max-width: 584px) 100vw, 584px" /></a>
  
  <p id="caption-attachment-881" class="wp-caption-text">
    Dos mentes de ¡Vaya Mierda! Fanzine con ropa interior incluída.
  </p>
</div>

&nbsp;

> <div id="attachment_883" style="width: 810px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand-web.jpg"><img aria-describedby="caption-attachment-883" loading="lazy" class="wp-image-883 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand-web.jpg" alt="stand-web" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand-web-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/stand-web-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
>   
>   <p id="caption-attachment-883" class="wp-caption-text">
>     Sapiencia en palets.
>   </p>
> </div>
> 
> <div id="attachment_884" style="width: 594px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft2-web.jpg"><img aria-describedby="caption-attachment-884" loading="lazy" class="wp-image-884 size-large" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft2-web-767x1024.jpg" alt="kraft2-web" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft2-web-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft2-web-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft2-web.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" /></a>
>   
>   <p id="caption-attachment-884" class="wp-caption-text">
>     Papel Kraft puesto a disposición de nuestros lectores para responder a la pregunta: «¿Qué te tatuarías en el culo?»
>   </p>
> </div>
> 
> <div id="attachment_885" style="width: 810px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/fans-web.jpg"><img aria-describedby="caption-attachment-885" loading="lazy" class="wp-image-885 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/fans-web.jpg" alt="fans-web" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/fans-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/fans-web-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/fans-web-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
>   
>   <p id="caption-attachment-885" class="wp-caption-text">
>     3 grandes fans de esta humilde publicación y compañeros de la infancia del director vayamierdero.
>   </p>
> </div>
> 
> <div id="attachment_886" style="width: 810px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web2.jpg"><img aria-describedby="caption-attachment-886" loading="lazy" class="wp-image-886 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web2.jpg" alt="redactoras-web2" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web2.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web2-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web2-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
>   
>   <p id="caption-attachment-886" class="wp-caption-text">
>     2 mentes pensantes integrantes del staff vayamierdil del sector fémino-neuronal.
>   </p>
> </div>
> 
> <div id="attachment_887" style="width: 810px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web.jpg"><img aria-describedby="caption-attachment-887" loading="lazy" class="wp-image-887 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web.jpg" alt="redactoras-web" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactoras-web-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
>   
>   <p id="caption-attachment-887" class="wp-caption-text">
>     Otra pose de nuestras 2 redactoras.
>   </p>
> </div>
> 
> <div id="attachment_888" style="width: 594px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft-web.jpg"><img aria-describedby="caption-attachment-888" loading="lazy" class="wp-image-888 size-large" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft-web-767x1024.jpg" alt="kraft-web" width="584" height="779" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft-web-767x1024.jpg 767w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft-web-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/kraft-web.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" /></a>
>   
>   <p id="caption-attachment-888" class="wp-caption-text">
>     Otra panorámica del producto neuronal de nuestros lectores.
>   </p>
> </div>
> 
> <div id="attachment_889" style="width: 810px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactores-web.jpg"><img aria-describedby="caption-attachment-889" loading="lazy" class="wp-image-889 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactores-web.jpg" alt="redactores-web" width="800" height="600" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactores-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactores-web-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/redactores-web-400x300.jpg 400w" sizes="(max-width: 800px) 100vw, 800px" /></a>
>   
>   <p id="caption-attachment-889" class="wp-caption-text">
>     3 redactores vayamiérdicos disfrutando de sapiencia a raudales.
>   </p>
> </div>
> 
> <div id="attachment_890" style="width: 594px" class="wp-caption alignleft">
>   <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/camiseta-web.jpg"><img aria-describedby="caption-attachment-890" loading="lazy" class="wp-image-890 size-large" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/camiseta-web-768x1024.jpg" alt="camiseta-web" width="584" height="778" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/12/camiseta-web-768x1024.jpg 768w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/camiseta-web-225x300.jpg 225w, https://www.vayamierdafanzine.es/contenido/uploads/2014/12/camiseta-web.jpg 800w" sizes="(max-width: 584px) 100vw, 584px" /></a>
>   
>   <p id="caption-attachment-890" class="wp-caption-text">
>     La camiseta oficial del fanzine ¡Vaya Mierda! que debes comprar por solo 4 euros.
>   </p>
> </div>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;