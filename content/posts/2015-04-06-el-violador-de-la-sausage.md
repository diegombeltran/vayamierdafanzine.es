---
title: El violador de la sausage
author: Neurona
type: post
date: 2015-04-06T11:15:57+00:00
url: /el-violador-de-la-sausage/
categories:
  - Blog

---
por Teletubi

Siéntate y escucha atentamente. O léelo, como prefieras. Pero has de saber que si estás leyendo esto, entonces tu vida puede cambiar a partir de ahora. Y no para bien.

Abotargado. Así me encuentro. Me han dado demasiadas pastillas&#8230; Algunas de colores. Otras no. Pero aun así soy consciente de ello, de su existencia, de su amenaza. Sigue aquí, mirándome y bien atento mientras escribo. Garabatos, letras, lo que sea.

No, no, no, no&#8230; No quiero pensar en ello, y sin embargo no puedo evitar hacerlo. Mil razones para olvidar y una sola para recordar, que persiste una y otra vez, rondando en mi cabeza. ¿Te has despertado alguna vez a las cuatro de la mañana? Estás a oscuras, en silencio, y sientes cómo algo acaricia tu cabello, lentamente, sin prisas. No tiene una forma concreta, y no porque no la tenga, si no porque no te atreves a mirar. Su tacto es etéreo y deseas que todo sea fruto de tu imaginación. Cierras los párpados fuertemente y la caricia se detiene.

Algunos niños no se lo toman en serio. Se ríen de mí y me tiran piedras. Piedras duras y afiladas como su corazón, como su ausencia de comprensión y compasión. Y sin embargo yo sé, no tengo la menor duda. El conocimiento fortalece y ayuda a seguir adelante, pero a costa de una felicidad que solamente otorga la ignorancia. Jamás me quebraré, o quizá sí. Quizá esté cerca de sucumbir mi frágil mente a los antojos del violador.

El violador de la sausage. Si tiras un pepinillo por la ventana nada pasará. Si tiras dos, le oirás susurrar. Si tiras tres, vendrá y te atrapará.

Todo comenzó con un inocente juego. Pero no, no puedo recordar. No debo recordar. Jamás debí entrar en aquel sótano. Ahora mi vida está ligada a este hospital. Soy un invitado, un invitado que jamás saldrá. Él y yo estaremos siempre aquí. Pero a él no lo ven. ¿O están fingiendo? Sí, debe ser eso: es mejor disimular que aceptar. Porque aceptar te puede llevar a sucumbir.

No, no creo que sea justo lo que está ocurriendo. Los golpes de electricidad no ayudan. Las pastillas no ayudan. Me siento mal. ¿La muerte? Él me lo propone, así que debo desconfiar. No recomendaría tan alegremente algo que fuese bueno para mí. Y sin embargo puede significar un fin. El fin. ¿Y si sigo atado aquí tras fallecer? ¿Es tal el vínculo? Espero que no. Voy a serenarme. Respiro hondo. Me tranquilizo. Ya. Ya estoy mejor. Ahora lo veo claro. Todo es una ilusión y como tal puede ser vencida.

Siéntate y escucha atentamente. O léelo, como prefieras. Pero has de saber que si estás leyendo esto, entonces yo he muerto.