---
title: Tres chicas de recuerdo
author: Neurona
type: post
date: 2015-06-28T20:09:49+00:00
url: /dos-chicas-de-recuerdo/
categories:
  - Blog

---
I

Más de diez años después he olvidado toda la noche, excepto esos cinco segundos. Se me acercó sonriendo y me sugirió que fuéramos al baño. La discoteca pinchaba una canción de dos jovencitas lesbianas; las cuatro -las lesbianas, la canción y Sonia- se llevaban por aquel entonces todos los vítores. Me atreví a rechazar su invitación, porque para mí lo máximo en aquella época era salir de fiesta con pantalones de rapero.

II

He pasado casi todo el día viendo cine y sin dejar de pensar que ya no sé disfrutarlo como de pequeño. Ahora capto cualquier sensación de los protagonistas y las intenciones del director con cada escena, los análisis y los juicios rebotan por mi cabeza sin parar. Preferiría ver las imágenes que llegan a mis ojos sin interpretaciones fusilándome. Después de cada película he pasado unos veinte minutos tumbado intentando asimilarla.

De camino al bar donde habíamos quedado, al observarme caminando en soledad, me han venido a la mente aquellos momentos de _guerra fría_ colegial en los que unos y otros intentaban imponerse sobre otros y unos acelerando el paso o entrometiendo la mano y la voz con la excusa de ofrecer el relato de la mejor de las historias vividas por un ser humano. Estos amigos, o compañeros, iban orientando sus vidas hacia el enfrentamiento con los demás, y ya empezaban a poner en práctica métodos para hacerse con el primer puesto social, mediante la exageración de la visibilidad de uno mismo y la invisibilidad del de al lado.

¿Es eso que cae una hoja o un pájaro? Si pienso en una hoja, parece un pájaro. Y si pienso en un pájaro, veo una hoja. Esta mañana no hay nadie por la calle, como todos los domingos. Solo aparecen individuos corriendo o parejas paseando, y la mitad son visiones provocadas por lo que tomé anoche. Encima, esos dos se han puesto a discutir. No he podido evitar, cual padre empedernido en sus labores salvadoras, lanzar unos alaridos con un mensaje, a mi parecer iluminador, sobre la inutilidad de las discusiones y sus causas profundas. Sin éxito: aprenderán discutiendo que no hay nada que discutir. Y qué hay peor que una pareja discutiendo. Ya casi nunca me apetece discutir, aunque la chica de antes era muy dura. Dudo que me llame. Me ha costado mucho convencerla para intercambiar teléfonos. Para los besos menos. Qué labios tan tiernos. ¿La volveré a ver? Claudia, creo…

III

Cené con ellas en un sitio cutre y barato; con ella y su madre. Invité. O, más bien… pagué. Las que me invitaron fueron sus miradas. En el momento en que la cajera anunció el total de euros, sus ojos las convirtieron en cámaras de seguridad: que están allí, viéndote, pero a la vez no. Me pareció como si yo me hubiera pedido tres menúes. «Míralas, parecen dos gatitas hambrientas que no saben ni qué es el dinero, que solo quieren que su ‘papito’ les dé de comer…», le dijo mi mente a mi yo. Procedí con un billete de veinte y algunas monedas, lo ganado en los últimos tres días.

Por la extrañeza de la situación y mis sensaciones, al día siguiente me curé en salud escribiendo hasta la frase anterior. Si algo salía mal siempre podría transformarlo en un texto más y, sin mucha maldad, hacerlo público. No creo que se entere, estoy casi seguro de que Paola leía lo que yo le daba, pero no creo que nunca vaya ella a buscarme.