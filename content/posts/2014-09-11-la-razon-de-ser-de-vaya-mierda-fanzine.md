---
title: La razón de ser de ¡Vaya Mierda! Fanzine
author: Neurona
type: post
date: 2014-09-11T10:38:34+00:00
url: /la-razon-de-ser-de-vaya-mierda-fanzine/
categories:
  - Blog

---
[<img loading="lazy" class="size-full wp-image-15 aligncenter" src="https://www.vayamierdafanzine.es/contenido/uploads/2010/12/Logo.png" alt="Logo vaya mierda fanzine" width="150" height="150" />][1]**FANZINE**

Según San Wikipedia:

_«Un fanzine (abreviatura en inglés de fan&#8217;s magazine, revista para fanáticos) es una publicación temática realizada por y para aficionados, uno de los tipos de Zine. El desarrollo de esta actividad no suele ir acompañado de remuneración económica, siendo los fanzines tradicionalmente gratuitos o con un coste mínimo para pagar los gastos de producción. El término se originó en octubre de 1940 con el fanzine de ciencia ficción «Detours» de Russ Chauvenet.»_

** La razón de ser de ¡Vaya Mierda! Fanzine.**

En el caso de ¡Vaya Mierda! Fanzine, disponemos de gran afición por el pensamiento. Es algo que nos atrae fuertemente, por lo que somos aficionados a una amplia gama de divagaciones ofreciendo éstas a aficionados de la misma índole.

El humor convive con la seriedad. Lo elaborado convive con lo cutre. La convivencia entre puntos extremos es nuestra base.

Hace ya muchos años, apareció en la mente de uno de los integrantes del Fanzine la siguiente frase: «Belleza y fiasco follan»… por ahí van los tiros. Manteniendo siempre un alto grado de actividad cerebral.

<a title="Fanzine según wikipedia" href="http://es.wikipedia.org/wiki/Fanzine" target="_blank">[Entrada completa de Wikipedia]</a>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2010/12/Logo.png