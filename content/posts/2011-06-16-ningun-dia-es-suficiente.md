---
title: Ningún día es suficiente
author: Neurona
type: post
date: 2011-06-16T08:01:16+00:00
url: /ningun-dia-es-suficiente/
jabber_published:
  - 1308214877
categories:
  - Blog

---
Las vibraciones de la tierra y el desencadenamiento de sucesos dramáticos en el mundo está siendo un problema. Por otro lado, a mi parece no importarme mucho.

A veces pienso que el ser humano merece todo lo que tiene. Aquí nadie respeta la tierra ni la vida. Tanto es así que hemos creado las guerras. Como le dije a alguien en Facebook en el grupo de Ateorizar: la razón humana no sirve. Esa fue una conversación nada constructiva y que comencé por la simple curiosidad que tenía de saber cómo reaccionarían si les preguntara si Dios existía o no. Terminamos hablando de Sócrates y su «sólo sé que no sé nada». Me di cuenta que, como me dijo la otra persona de esa conversación, Sócrates sabía que sí sabía mucho y utilizaba esa frase como un sarcasmo, más o menos. Digo que la utilizaba como sarcasmo porque él la decía para hacerse el que no sabía ante otras personas y así podía ver mejor cuán ignorantes eran los demás. Algo así era que funcionaba su lógica.

En fin, eso me dio una percepción diferente de Sócrates. No sé ni qué pensar.

También, hablando con una señora me di cuenta de que no soy el único que piensa que en esta sociedad hay miles de personas viviendo de apariencias. Las apariencias nos llegan a las narices y no nos damos cuenta. La gente vive sucumbida en deudas sólo para aparentar que viven bien pero en realidad no están viviendo bien. Compran casas que ni pueden pagar sólo para aparentar. Y así sucesivamente hacen muchas cosas por pura apariencia. Eso ya lo sabía y algunos de ustedes también, pero me pareció interesante que esas palabras salieran de una persona que estaba bien vestida y con una actitud de «lo sé todo», pero luego me di cuenta de que su actitud era a causa de su trabajo. O sea, actuaba de esa manera, aunque no era así, porque tenía que utilizar esa manera de ser para poder alcanzar sus metas. Tenía que vestirse bien en una oficina que ni lo ameritaba por el simple hecho de que aquí, en Puerto Rico, la gente tiene un ego tan y tan cabrón y quieren cagar más arriba del culo, que si no es con esa actitud con la que llega una persona no la contratan. O sea, si no tienes el ego trepao&#8217;, te jodiste cabrón, no tienes trabajo.

Así es en ésta mierda de isla. Una hermosa isla, con playas que están entre las mejores del mundo, un clima muy bueno y muchos lugares a dónde ir pero el ego del Boricua lo caga tó&#8217;. Entonces, gente como yo, piensa y analiza la situación en el país que cada vez está peor y no va a mejorar, y al final nos damos cuenta de una cosa: no vale la pena. No vale la pena seguir jodiéndome por un chorru&#8217;e anormales que no quieren hacer nada por su vida y quieren que todo les llegue por arte de magia. No vale la pena esforzarse para vivir dentro de la sociedad si al final la sociedad no te va a dar nada a ti. No vale la pena integrarse al sistema cuando el sistema es una mierda.

No tengo tiempo, ni dinero; no tengo propiedades valiosas fuera de mi casa; puedo quedarme sin comida y sin ropa pero nunca me quedaré sin ser quien soy. No voy a tratar de seguir integrándome a esta mierda de sociedad. No tengo un día para sentarme a hablarte de todo lo que pasa por mi mente porque no sería suficiente. Además, no tengo ganas.