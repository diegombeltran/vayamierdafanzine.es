---
title: 'Teleguía: Dónde masturbarse'
author: Neurona
type: post
date: 2015-04-15T18:37:21+00:00
url: /teleguia-donde-masturbarse/
categories:
  - Blog

---
Está claro que el arte del onanismo (es decir, amarse a uno mismo) es una actividad frecuente en este mundo cruel y despiadado. Una alternativa de relajación cuando no hay nada más a mano que la propia mano. No obstante, a veces podemos aburrirnos incluso de ese tipo de práctica y es por ello necesario añadir algo de originalidad y buscar sitios alternativos al habitual sofá del salón de los vecinos. Es aquí donde me atrevo a proponer ubicaciones ligeramente incorrectas que podrán añadir un «plus» al «plas» de este jugueteo sexual.

Podemos comenzar a probar a masturbarnos en sitios que vemos habitualmente en la tele. Así hacemos turismo, vemos cosas nuevas y seguimos adquiriendo práctica en esta popular perversión. Un lugar que me viene a la mente es el Parlamento. ¿Por qué no?, se trata de un lugar infrautilizado por la escasa asistencia de la mayoría de los políticos.

Aunque pensándolo mejor, y ya puestos, quizás sería mejor acercarse a Ferraz o a Génova, donde podríamos llegar a conocer gente interesante que aumentará sin duda nuestra satisfacción personal. Si tus preferencias sexuales tienden hacia las mujeres, por su notable apellido Leire Pajín sería una gran opción para acompañarte, pudiendo finalizar en bukkake tal y como relataron una vez Los Chikos del Maíz. Si te atrae el cuerpo masculino, Arias Cañete es tu gran opción: Con su gran superioridad intelectual te asesorará en tus técnicas. Su especialidad es el oral, al ser un experto en «comerlo todo».

Otra bella localización puede ser tu comisaría más cercana. De hecho, si sigues las indicaciones de este artículo tu presencia aquí será segura tarde o tempranto. Aquí puedes experimentar con la opción del sadomasoquismo, pero por desgracia ahora no se permite grabar mientras los agentes de la ley te apalean. Una lástima, ya que no podrás compartir con tus amistades las imágenes de esa increíble noche en la que te tocabas mientras recibías una amable paliza al grito de «Viva España, viva el Rey, viva el orden y la ley».

Una vez arrepentidos podemos proceder en un lugar más pío. Si eres mujer y optas por una mezquita deberás tener en cuenta que la masturbación perderá emoción, ya que con tanta ropa nadie se enterará de lo que estás haciendo, perdiendo así parte de la gracia. En las iglesias católicas hay una posibilidad de encontrarte con uno de esos sacerdotes que piensan que lo mejor de la biblia cristiana es la frase de «Dejad que los niños se acerquen a mí». En ese caso, el cura puede apuntarse a tu actividad con alegría y entusiasmo y, quién sabe, igual hasta inventáis una coreografía o termináis en «trenecito».

¿Y qué me decís de un plató de Telecinco? Es cierto, tenéis razón, eso está ya muy visto.

Espero que estas recomendaciones os hayan abierto la mente y os permitan investigar en muchos más sitios. Eso sí, una advertencia: Niños, niñas, no hagáis esto en casa.

<p style="text-align: right;">
  Teletubi
</p>