---
title: Mi amigo Adolfo
author: Neurona
type: post
date: 2014-06-27T08:35:44+00:00
url: /mi-amigo-adolfo/
categories:
  - Blog
tags:
  - Adolfo

---
Una vez me sentí mal. Estaba hablando con Adolf Hitler en una taberna de Cuenca y este fue el diálogo:

<img loading="lazy" class="aligncenter" src="http://www.imagenescaricaturas.com/wp-content/uploads/2012/10/caricatura-hitler.jpg" alt="" width="319" height="231" /> &#8211;       Bartolo, no te metas el dedo en la nariz, es de mala educación.

&#8211;       También es de mala educación que nazca gente como tú. Por lo tanto deberías dar ejemplo.

&#8211;       Perdona, pero creo que estás equivocado. Mis pensamientos son altamente acertados y justos. Lo que ocurre es que el mundo no ha sabido nunca la verdad.

&#8211;       ¿Me la puedes explicar? Amigo, Adolfito mío. (la cerveza hacía que terminase las frases de esta forma tan jocosa).

&#8211;       Por supuesto, te mereces todo y más. Eres la reencarnación del verdadero y futuro ser humano.

&#8211;       Anonadado me dejas con tus halagos.

&#8211;       Te explicaré lo que ocurrió.

El incomprendido Rudolf Hess tenía razón. Los judíos nos drogaron a todos para que los aniquilásemos sin piedad y de forma salvaje. ¿Qué conseguirían sacrificando a millones de los suyos?

Muy sencillo. Compensaciones tras la Segunda Guerra Mundial.

Algunos dirán que soy un paranoico, pero el pueblo judío sacrificó gran parte de su pueblo con el fin de hacerse más fuerte.

&#8211;       «Juer», Adolfo. Me he quedado petrificado. Necesito otro _gin-tonic_. Por cierto, ¿no te habías suicidado en la década de los cuarenta?

&#8211;       No, eso se lo inventaron. Los medios engañan.

&#8211;       ¿Y por qué te conservas tan joven? Estamos en el año 2012, ¿no pasa el tiempo por ti?

&#8211;       Uno es joven si tiene proyectos en la vida. Actualmente estamos trabajando muy duro en Alemania con un plan magistral. Si quisiera envejecer me iría a España a ver Telecinco.

&#8211;       Me vuelves a dejar anonadado.

<p style="text-align: right;">
  <em>Neurona</em>
</p>

&nbsp;

&nbsp;