---
title: La bodega d´en Rubén (Barcelona)
author: Neurona
type: post
date: 2015-09-09T11:37:53+00:00
url: /la-bodega-den-ruben-barcelona/
categories:
  - Blog

---
«Hay gente que le llama la atención, por decirlo de alguna manera, las prostitutas maduras.» Ser humano.  
Hace poco tiempo, varios amigos que comparten, o mejor dicho, soportan mi adicción por lo raro, me acompañaron al Raval Barcelonés. Habíamos estado varios días en un camping cercano al aeropuerto de El Prat asando y friendo cosas hasta que mi primo nos invitó a su hogar. Un ser humano, que coincidió ser yo, le informé sobre mi interés en visitar la calle donde vivió Carmen de Mairena y mi primo, amablemente, nos llevó a dicho paraíso. Es en ese barrio donde nos topamos con La bodega d´en Rubén.  
El bar, de aspecto del bueno, es decir, cutre, contaba con dos o tres prostitutas maduras que intentaban captar clientes generalmente también maduros. No estuvimos mucho tiempo, lo justo para tomarnos una caña y pedir un pincho de tortilla que comenzó a comerse un desconocido cuando no mirábamos. Al salir a la calle, dicho desconocido, satisfecho con nuestro pincho de tortilla nos pidió un cigarro.  
Me gustó la mirada que me echó una madura negra con pechos del tamaño de Júpiter.  
Si deseas un ambiente diferente y es usted una persona que no se escandaliza de la humanidad, no lo dudes, acude a La bodega d´en Rubén.

[<img loading="lazy" class="alignleft size-full wp-image-1118" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/09/bodega-ruben-barcelona.jpg" alt="bodega-ruben-barcelona" width="995" height="662" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/09/bodega-ruben-barcelona.jpg 995w, https://www.vayamierdafanzine.es/contenido/uploads/2015/09/bodega-ruben-barcelona-300x200.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/09/bodega-ruben-barcelona-451x300.jpg 451w" sizes="(max-width: 995px) 100vw, 995px" />][1]

Por Neurona

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/09/bodega-ruben-barcelona.jpg