---
title: El cine históricamente fiel es para menopáusicas.
author: Neurona
type: post
date: 2015-04-24T18:35:54+00:00
url: /el-cine-historicamente-fiel-es-para-menopausicas/
categories:
  - Blog

---
Las primeras muestras cinematográficas vienen de la mano de los hermanos Lumière, quienes se limitaban a reproducir la realidad plantando la cámara en sitios random y filmando a tiempo real cómo la gente respiraba y hacía cosas que no interesan a nadie.

Tras varios años de filmaciones sin sentido nos dimos cuenta de la infinidad de posibilidades que encierra la cámara oscura. Sólo combinando planos y escenas, jugando con el tiempo y lugar, podíamos captar sensaciones sin apenas sufrir distorsión, emociones en bruto, sonido e imagen integrados desafiando a un público confudido entre lo que veía y lo que realmente era.

¿Y este rollo tan gratuito a qué viene? Mi declaración de guerra a los puristas del cine histórico comienza por la idea del cine como medio íntegramente artístico.

No me voy a poner idealista porque es evidente que todo arte tiene sus influencias históricas, políticas, sociales, culturales y sexuales; pero considero que para poder vomitar arte en toda su pureza hay que ser lo suficientemente valiente como para transgredir ciertos límites.

Abogo por la infidelidad en el cine. La infidelidad histórica es maravillosa, sin ella qué habría sido de Gladiator, Braveheart, Amadeus, Apocalypto, El último samurái, 300 y otras ochocientas millones de películas que harían retorcerse a cualquier historiador pero que nos han hecho disfrutar durante 90 minutos de nuestra vida.

El arraigo histórico en el cine es para menopáusicas. El excesivo cuidado que se necesita para la representación totalmente fiel de una época y acontecimiento histórico impide al creador eyacular libremente, mutilando la creatividad y comprimiendo la intensidad de lo que se transmite.

Recuerda que el cine solo nutre tus sentidos, a la razón ya la alimentamos suficiente con otra mierda.

Patry Torres