---
title: 'Nota de prensa: Posible retorno de ¡Vaya Mierda! Fanzine'
author: Neurona
type: post
date: 2014-06-25T08:38:08+00:00
url: /nota-de-prensa-posible-retorno-de-vaya-mierda-fanzine/
categories:
  - Blog
tags:
  - retorno fanzine ¡Vaya Mierda! 2014

---
El otro día, mientras tomaba un zumo de naranja con néctar de cucumber split, sentí envidia del grupo ACDC. No por que tengan más dinero que yo. A mi me gusta ser un vividor, chupóctero del sistema y adaptado a mi situación de indigente camuflado. Si no por que ellos vuelven de gira y a sacar un nuevo LP.

Por ello, si mis ataques de ansiedad me lo permiten y nuestro grupo de colaboradores va respondiendo medianamente, es posible que nuestra temporada sabática termine para poder ir publicando un fanzine cada 2 meses por ejemplo.

Creo que este hobby vayamierdil puede nutrir en gran medida mi desarrollo como ameba pseudoevolucionada.

No olvides, que si te gusta escribir, puedes colaborar de forma altruista con esta iniciativa intelectual sin lucro económico pero si almático. Contacta con nosotros a través del formulario de contacto de esta web.

Otra forma de colaborar es ser patrocinador nuestro, colocando un banner publicitario en nuestro fanzine.

Reciban una cordial abrazadera,

Neurona.