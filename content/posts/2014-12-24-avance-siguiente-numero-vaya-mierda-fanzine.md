---
title: Avance siguiente número ¡Vaya Mierda! Fanzine.
author: Neurona
type: post
date: 2014-12-24T08:09:59+00:00
url: /avance-siguiente-numero-vaya-mierda-fanzine/
categories:
  - Blog

---
[<img loading="lazy" class="alignleft size-full wp-image-15" src="https://www.vayamierdafanzine.es/contenido/uploads/2010/12/Logo.png" alt="Logo vaya mierda fanzine" width="150" height="150" />][1]

&nbsp;

&nbsp;

&nbsp;

<span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">El nuevo número de ¡Vaya Mierda! Fanzine será parido en brevas.<br /> A falta de la ansiada portada, todas las demás eyaculaciones neuronales se dan cita en otro riconcito más del bucocalcarismo marginal humano.<br /> En este ejemplar, podrán ustedas disfrutar de:<br /> &#8211; Portada humorística a cargo de la genial <a href="https://www.facebook.com/DeboraAguelo" data-hovercard="/ajax/hovercard/page.php?id=151495178227009">Débora Aguelo / nabey</a><br /> &#8211; Editorial: 3 pensamientos brevísimos por Neurona.<br /> &#8211; Paritorio: Un chiste del bol<span class="text_exposed_show">i bic por Neurona.<br /> &#8211; Ajos´ Revolt: Artículos varios. «Un buen día» por X+Y, «El 90% de mis amigos son gilipollas» por Javi Burguiño y «El violador de la sausage» por Teletubi.<br /> &#8211; El vitriolo de Méliès: Nuestra sección de cine, con artículos de Nolfy y Patry Torres.<br /> &#8211; Especial Salón del cómic Zaragoza 2014.<br /> &#8211; Sección temática: Hablaremos del sentido de la vida.Artículos de Nolfy, Teletubi y Neurona.<br /> &#8211; El Rincón de Susana: Susana Portero nos hablará sobre su «Manual de autoayuda contra el absurdo cotidiano».<br /> &#8211; Contraportada: Chiste ilustrado por nuestro artista underground Santo Espanto.</p> 

<p>
  Esperamos que lo disfruten.</span></span>
</p>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2010/12/Logo.png