---
title: I’m a cyborg but that’s okay
author: Neurona
type: post
date: 2015-06-28T20:18:15+00:00
url: /im-a-cyborg-but-thats-okay/
categories:
  - Blog

---
Mientras intentaba conservar la integridad física y mental a 40º con un café postcomunista en un bar chino de Zagreb, un hombre sabio me dictaba todas las películas que debía ver para perder lo que me queda de cordura. Antes de desfallecer por lipotimia, me dio tiempo de apuntar algunas grandes marcianadas del cine para compartir con vosotros.

Una de las joyas que mi amigo tuvo la generosidad de compartir conmigo es la peculiar _I&#8217;m a cyborg but that&#8217;s okay_, una ecléctica apuesta engendrada por el maestro surcoreano Park Chan Wook.

Hace 9 años, hastiado de los violentos y oscuros guiones que le habían dado el éxito con la _Trilogía de la Venganza_, Chan Wook decidió darse un respiro y producir la divertida, rara y curiosamente adorable comedia romántica que comentaremos a continuación.

_I&#8217;m a cyborg but that&#8217;s okay_ nos introduce la historia de Young-goon, una joven que acaba de ser internada en el psiquiátrico debido a creer fervientemente ser un cyborg.

Mientras vemos como se desenvuelve una chica robot en un instituto mental (dialogando con las máquinas de café, chupando pilas para alimentarse&#8230;), Chan Wook empieza a narrarnos paralelamente el triste pasado de la joven, lleno de aflicciones, frustración y una atmósfera familiar insana tanto emocional como psicológica.

Nuestra cyborg empieza a deteriorarse rápidamente ante la despreocupada mirada de los psiquiatras. Se niega a comer por miedo a estropearse y las únicas energías que le quedan las agota pensando mil y una formas para poder ir a ver a su abuela al geriátrico, llevarle su dentadura y que le revele de una vez por todas el propósito de su existencia.

Cuando la batería de Young-goon está a punto de extinguirse, interviene nuestro príncipe azul, un chico antisocial y cleptómano que ideará todas las formas posibles para conseguir que el mecanismo del frágil cyborg siga funcionando.

Ya sé que son 105 minutos de película y muchos de vosotros iréis a hacer un pis varias veces sin darle a pausa, pero es un tiempo que no os arrepentiréis de invertir.

Disfrutad con esta rareza que el director surcoreano se ha atrevido a hacer cabreando a todos sus fans ansiosos de sangre y violencia. Dejaos llevar por las escenas estéticamente increíbles y llenas de significado, las panorámicas de 360º, los planos largos que encadenan mil historias y la atmósfera bizarra que resulta de la fusión entre _Amèlie_ y _Alguien voló sobre el nido del cuco._ Atended todas las cuestiones sobre la humanidad que el director esparce a lo largo de la película y no olvidéis la simbiótica relación que Chan Wook hace entre la estética visual de la escena y su significado.

Que la disfrutéis.