---
title: Robots biológicos
author: Neurona
type: post
date: 2015-04-15T18:40:24+00:00
url: /robots-biologicos-2/
categories:
  - Blog

---
¿Una tablet se basa en el Silicio y un humano en el Carbono?

Cada vez me da más la impresión que civilizaciones extraterrestres llevan contactando con nosotros desde el comienzo de la vida en nuestro humilde planeta.

Si nuestro origen tiene algo que ver con dichas civilizaciones es algo en lo que debo profundizar, aunque no hay que descartar nada.

Lo que si tengo claro es que si somos simples robots biológicos cuyo devenir se basa únicamente en segregaciones químicas ordenadas por el cerebro para producir placer, tenemos el mismo valor que una piedra o una hez.

Me obligo a no creer que solamente estemos programados para nacer, crecer, reproducirnos y morir al único compás de la química. Si no hay nada más que la insignificante existencia en el segmento de la vida, el sentido es un sinsentido y viceversa.

Hay otra idea que también voy teniendo cada vez más clara. Las civilizaciones extraterrestres no han contactado de forma oficial con nosotros porque todavía nos encontramos en una fase medieval. No estamos para nada preparados para entender la supuesta realidad, el universo o multiversos. Nos queda por descubrir mucho sobre la ambigua existencia, en el caso de existir realmente dicha palabra. Quizá colapsemos con tanta reflexión y únicamente nos interese permanecer entretenidos con Tele 5 y los Gin Tonics.

Creo que las religiones y la espiritualidad tienen algo que ver con lo que estoy escribiendo, aunque en una forma algo burda y poco pulida.

¿El sentido de la vida? ¿Existencia? ¿Sentido? ¿Nuestros orígenes? ¿Nuestro destino? ¿Lo que nos falta por evolucionar y aprender? Prometo centrar gran parte de mi vida a este tema, creo que por ahí debemos caminar.

<p style="text-align: right;">
  Neurona
</p>