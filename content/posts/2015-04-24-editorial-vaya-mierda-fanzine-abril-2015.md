---
title: Editorial ¡Vaya Mierda Fanzine! Abril 2015
author: Neurona
type: post
date: 2015-04-24T18:11:08+00:00
url: /editorial-vaya-mierda-fanzine-abril-2015/
categories:
  - Blog
tags:
  - reflexiones sobre la vida

---
Somos en gran parte química.  
Un principio activo llega a través del torrente sanguíneo hasta mi cerebro y cambia mi estado anímico.  
Ansiedad, bajón, alegría, acción. La bipolaridad es mi sino.  
Carezco del poder de segregar de forma natural mis compuestos del placer.  
La Física, base de la Química y todo lo demás, hace que tengamos una breve presencia en una desconocida todavía dimensión.  
Cuerpo, albergador de alma, sustrato, cáscara, mecanismo del mecánico llamado médico.  
Maltratado por ese vicio necesario en la especie humana. Y en todas. El desconocimiento del vicio es la salvaguarda, la salud, castidad.  
Ojos que no ven, cachondeo que desconozco.  
Felicidad, extraño término. La denominaría como un estado placentero continuado en el tiempo. Balance positivo del devenir en nuestros días.  
Cada uno es un cosmos, cada uno es parte de Dios. Cada uno es diferente en una supuesta realidad común.  
Para uno la caca es hez, para otro es lujo. Todo depende de nuestra experiencia.  
No quiero ser millonario, solamente pretendo el lujo de que me traten bien, un hogar humilde pero digno, alimento, ropa, vida social llevadera y un hobby, ¡Vaya Mierda! Fanzine.  
Si hay algo malo es ir a peor, porque desmotiva y hunde. Si se vive en escasez pero se atisba la mejora, te comes el mundo, lo contrario agarrota las fuerzas de levantarse, de moverse, de trabajar, pensar y seguir en esta dimensión.  
Aprender cada día. Cuando una persona deja de aprender debería armarse de valor y volar, viajar hacia la inexistente muerte y conocer hasta el infinito.

_Neurona_