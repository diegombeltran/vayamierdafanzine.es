---
title: Pignatelli experience
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-06-20T08:07:24+00:00
url: /pignatelli-experience/
jabber_published:
  - 1308557244
categories:
  - Blog

---
El sábado íbamos a celebrar una de nuestras cutrepartys vayamierdiles. Habíamos quedado en un lugar que nos habían recomendado, un antro viejo como el mundo llamado El Pajarcico. Al llegar allí nos encontramos con una sorpresa: ya no existía. Los bares cutres están desapareciendo como los dinosaurios. Pero no desesperemos: Una vieja ley no escrita dice que cuando uno se cierra, otro toma su lugar. No todo fue malo: Unas trabajadoras sociales, de las que abundan en la zona, nos alegraron la tarde a base de guiños y palabras amables cuyo objetivo no era otro que encaminar nuestros pasos en la dirección adecuada. Pero -pobreza obliga- simplemente agradecimos su buena voluntad y continuamos con nuestra cutreparty por otras calles.  
[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/putis.jpg?w=200" alt="" title="putis" width="200" height="300" class="alignnone size-medium wp-image-110" />][1]{.broken_link}

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/putis.jpg