---
title: Concierto Tuco Requena+Los Gandules Anfiteatro Expo.
author: Neurona
type: post
date: 2014-07-27T15:23:50+00:00
url: /concierto-tuco-requenalos-gandules-anfiteatro-expo/
categories:
  - Blog
tags:
  - los gandules
  - tuco requena

---
Os regalamos para deleite de vuestro Yo interno la última producción audiovisual de la productora del Fanzine ¡Vaya Mierda! bautizada como «Chapuzas audiovisuales».

Se trata del concierto que ofrecieron Tuco Requena y Los Gandules el pasado viernes 25 de Julio de 2014 en el anfiteatro de la expo en la ciudad española de Zaragoza.