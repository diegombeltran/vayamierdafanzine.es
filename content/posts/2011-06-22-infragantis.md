---
title: Infragantis
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-06-22T07:19:26+00:00
url: /infragantis/
jabber_published:
  - 1308727167
categories:
  - Blog

---
[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/billar.jpg?w=300" alt="" title="BILLAR" width="300" height="203" class="alignnone size-medium wp-image-116" />][1]{.broken_link}  
Militantes de los dos partidos mayoritarios, sorprendidos in fraganti mientras disputaban una partida de billar americano en paños menores y en un local de Sitges. Ambos partidos han emitido sendos comunicados en los que:

1) Afirman que todo ha sido un montaje de Izquierda Menuda para desacreditarles. También se insinúa que los fotógrafos eran, en realidad, agentes del servicio secreto venezolano y de la KGB.

2) Afirman que en el momento de la interrupción, sus militantes eran quienes estaban ganando la partida.

3) Niegan la militancia de esas personas en sus respectivos partidos.

4) Niegan la existencia del billar americano.

5) Niegan rotundamente la existencia de un lugar llamado Sitges.

(Fuente: Agencia VayaMierda News)

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/billar.jpg