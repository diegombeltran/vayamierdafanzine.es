---
title: La cocacola en lata
author: Nolfy
type: post
date: 2011-06-16T08:11:00+00:00
url: /la-cocacola-en-lata/
jabber_published:
  - 1308215462
categories:
  - Blog
tags:
  - cocacola en lata

---
Mi querido y apreciado editor, quizá viendo que ando escaso de genio y creatividad, me ha propuesto para este mes el tema “¿Qué es la vida? ¿Cómo afrontar este diminuto segmento de existencia? “. Pero puesto que ya he dicho bastantes sandeces sobre la vida en números anteriores, (quizá por eso precisamente lo ha hecho) y como ahora mismo no me apetece hablar de algo tan profundo, además de que cualquier cosa que diga va a ser algo manido y repetitivo, porque tampoco he filosofado últimamente sobre nada nuevo al respecto, pues básicamente voy a pasarme el tema por el forro de los cojones y voy a hablar sobre la cocacola en lata. Por cierto, que si veis a mi compañero columnista, el señor Teletubi Rojo hablar de Cataluña y el País Vasco&#8230; ha hecho caso al editor. Podéis reíros de él.

Sí, mis queridas personas humanas. Porque aunque mi querido profesor de literatura de bachillerato decía que la expresión “persona humana” es una reiteración, yo digo que nadie ha pensado en las formas de vida alienígenas inteligentes. Que aunque no existan realmente, existen literariamente, por lo que desde ese punto de vista, “persona humana” ya no sería reiterativo semánticamente. ¿son personas las formas de vida alienígenas? ¿es correcto hablar de “persona alienígena”? Todo depende de la forma de vida, puesto que una especie basada en una mente colectiva, al estilo de las hormigas, quizá no pueda considerarse como tal, aunque otra basada en individuos con forma de marcianillos verdes seguramente sí. ¡¡Anda!! Y los zombis. Tampoco existen, pero sí literariamente, por lo que quizá quepa también. La clave es&#8230; ¿es un zombi un individuo?

En fin, planteados estos debates, (&#8230;para que luego digan que últimamente no soy profundo) pasemos a la cocacola en lata, que es de lo que realmente va este artículo. Un artículo que hace ya tiempo que tengo ganas de escribir.

No, no voy en coña. Voy a hablar de la cocacola en lata. Me gusta la cocacola. Es decir, sé que no es lo más sano del mundo, sé que lleva gas, cafeína, es de color negro (cosa que da yuyu) se usa para desatascar las cañerías y además da enormes beneficios a una empresa que explota su mano de obra en múltiples países como cualquier multinacional. Y además, sabe diferente según el país donde se beba, por lo que en todo momento tengo que decir que estoy hablando acerca de la cocacola en España, que no es la misma que en China. Sé que al llevar cafeína me excita el sistema nervioso, a mí, que soy nervioso por naturaleza, que contribuye a subir mi yang y por lo tanto a que seque las raíces de mi pelo, por lo que puede que me quede sin pelo en la cabeza antes de lo previsto.

Joder, pero es que me encanta. Y como persona humana que soy, gilipollas en ciertos aspectos como muchas personas humanas, reivindico mi derecho a autoperjudicarme, aunque sea menos que los que fuman, beben como cosacos o se drogan. Mira, ahí si que no hago daño a nadie. De todas formas, creo que somos todos yonkis de algún modo, vivimos esclavos de nuestras emociones e impulsos, pero bueno, eso es otro tema.

Como cualquier persona que vaya al supermercado de vez en cuando, sabréis que la cocacola podéis encontrarla en botellas de plástico de 2 litros, de plástico de medio litro, botellines de vidrio de 20 cl y latas de aluminio de 33cl. Los “entendidos” de la cocacola dicen que la mejor forma de beberla es en botella de vidrio, porque conserva sus propiedades y su sabor. Majaderos&#8230;

Creo firmemente en la cultura de lo cotidiano. Y así como hay una cultura de la cerveza, una cultura del vino, como me gusta la cocacola, y como soy caro cuando hay vicio (como dice Pereza) me gusta la cocacola en condiciones. Y la verdad, creo que un estudio de la cocacola puede ser tan válido como el estudio del whisky de malta de 12 años.

&#8230;Si analizamos los diferentes envases, vemos que por una parte, las botellas de dos litros de cocacola, destinadas a las familias, son las que mejor precio ofrecen. Pero vamos a ver, teniendo en cuenta que una botella de dos litros conserva la cantidad de gas suficiente en la cocacola durante unos tres días&#8230; ¿cuántas familias se chuflan 0,66 litros de cocacola al día? Porque en cuanto la botella pierde gas, la cocacola pasa a ser un guano líquido como un campano de gordo. Sí, parece una cantidad perfectamente razonable, pero si tenemos en cuenta que la cantidad es equivalente a dos latas, y que el envase es de mayor dificultad de enfriamiento, y que si se queda abierta mucho rato pierde todo el gas&#8230; pues francamente, pierde mucha utilidad. Vamos, cocacola de garrafón.

&#8230;parecido pasa con la botella de medio litro. Por un poco más de precio que una lata, tenemos casi un 50% más de producto. Parece ideal para el cine. Pero sigo viéndole pegas, aunque sea usuario ocasional de este envase. El proceso de abrir y cerrar la botella conlleva la consecuente pérdida de gas, además, la bebida se calienta mucho más rápido, con lo que hacia el final de la película podemos volver a tener un guano líquido diarreil como un campano de gordo. Al final, la única ventaja es la portabilidad.

Si vamos al “ideal” envase de vidrio de 20cl&#8230; ¿dónde están las ventajas? Porque eso de que conserva mejor las propiedades y el sabor me parece una sarta de memeces. Al ser el envase ideal de cafés y bares, estamos pagando por una cantidad de cocacola inferior a una lata, un precio mayor, en igualdad de condiciones, porque es así, el vidrio es mucho más caro. A eso hay que añadir que la sirven con una rodaja de limón&#8230; y hielo. ¡¡¡NOOOOOO JODEEEER!!! coño, mi cocacola va a seguir fría, pero a costa de ir viéndose degradada con la infame adición de agua derretida. ¡¡Vaya mierda!!. Y si nos la bebemos sin la mediación del vaso, la forma de la boca dificulta el beberla porque no hay por donde salga el aire, y la saboreamos menos. Innumerables noches de turno en hoteles con escarceos a la cámara de refrescos de cafetería lo corroboran.

Y llegamos al quid de la cuestión. A la moraleja. A la R de Randy. La repera, vamos&#8230; la cocacola en lata. Tenemos ante nosotros un envase con una cantidad de líquido respetable, pero no demasiada, un tercio de litro, en un envase perfectamente apilable, reciclable y que se enfría rápida y fácilmente en la nevera. Podemos abrirlo de forma simple (problema que había en las latas presentes en mi infancia, aquellas que dejaban aquella chapa cortante) beberla directamente del envase, con un flujo de líquido considerable debido a la entrada de aire por la forma de la boca, por lo que la saboreamos bien. Debido a las características del material, no necesita hielo y se mantiene fría mucho tiempo. Al estar al aire libre, el gas dura menos, pero eso no es inconveniente puesto que la cantidad de líquido presente y la carencia de portabilidad del envase al no poder cerrarlo fuerzan en cierto modo a beberla en un corto espacio de tiempo. Por no decir del hermoso sonido que producen cuando las abres&#8230;

En suma, es el envase ideal para disfrutar plenamente. Por tanto, señores entendidos, si siguen diciendo semejantes chorradas como parthenones, no han podido beber tantas cocacolas como yo.

Nolfy