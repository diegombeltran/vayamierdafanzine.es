---
title: Bar Online
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-06-30T08:24:44+00:00
url: /bar-online/
jabber_published:
  - 1309422285
categories:
  - Blog

---
Tras 17 años de arduas investigaciones, la policía ha conseguido localizar y clausurar un bar online, acusado, entre otros graves delitos, de adulterar los cubatas, hacer pasar cariñena de garrafón por Rioja gran reserva y tener varias salas virtuales en las que, al parecer, se practicaba de forma habitual el tute subastao e incluso el mus, además de haber organizado un campeonato clandestino de ajedrez a tres bandas. En la operación se han intervenido diversos artefactos, entre los que cabe destacar una placa base 4DDR3 16GB, 8 memorias DIMM de 1 Giga, 3 tarjetas gráficas GTX460 y una bolsa de patatas fritas a medio consumir. También se ha intervenido dinero, y aunque la cantidad se mantiene en secreto por parte de las autoridades, fuentes cercanas a la redacción de ¡Vaya Mierda! han asegurado que estaría alrededor de tres euros y sesenta céntimos. Se desconoce el paradero del tasquero virtual. (VM press)  
[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/baronline.jpg" alt="" title="baronline" width="300" height="184" class="alignnone size-full wp-image-119" />][1]{.broken_link}

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/baronline.jpg