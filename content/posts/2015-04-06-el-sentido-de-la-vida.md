---
title: El sentido de la vida
author: Neurona
type: post
date: 2015-04-06T11:17:26+00:00
url: /el-sentido-de-la-vida/
categories:
  - Blog

---
Es en la fase vital de la adolescencia donde creo que todas las personas pasamos por una fase de autocuestionamiento. El tránsito a la vida adulta conlleva una serie de preguntas que uno se hace a sí mismo y que antes no habían surgido. Se estabilizan muchos de los valores de la persona, las creencias espirituales y muchas actitudes ante la vida. El motivo de nuestro devenir mundano es una de esas preguntas, quizá la más importante. El sentido de la vida, porqué estamos aquí.

El mero planteamiento de la pregunta presupone ya un estatus de diferenciación con respecto al resto de seres vivos del planeta, algo que algunas corrientes filosóficas niegan. Si nuestra existencia es más importante que la de un cardo, ¿qué propósito tiene? Porque si un cardo del campo tiene la misma importancia que nosotros, es decir, a nivel de propósito, no tiene sentido como digo plantearse la pregunta. Supongamos entonces que es así y somos más importantes que un cardo o que un dromedario ya que nosotros podemos plantearnos esta pregunta y ellos no, tenemos conciencia de nuestra propia individualidad.

Un propósito, ¿en base a qué? ¿qué razón debería sostener que el individuo deba tener un propósito? ¿la razón? Si queremos sostener esto surgen varias preguntas. En primer lugar, ¿estamos usando bien nuestro raciocinio? Un análisis de los ecosistemas y de cómo interactúa el hombre con ellos indica que más que una especie adaptada al medio natural nos estamos comportando como un virus, agotando los recursos naturales. Como sociedades, nos matamos y nos esclavizamos unos a otros, bien de forma directa o de forma más sutil mediante la dependencia económica.

Y aun suponiendo este argumento como válido, ¿Qué propósito es ese? ¿un propósito a nivel cósmico? ¿estamos hechos para dominar el cosmos? ¿para explorarlo? ¿para contactar con otras civilizaciones extraterrestres? Suponiendo que existan. ¿un propósito a nivel espiritual? ¿hemos nacido en este mundo para alcanzar algún tipo de trascendencia hacia otro plano de existencia superior? Eso implica cierta predestinación en nuestro nacimiento, por lo que estaríamos presuponiendo la existencia de un ser superior.

Son muchas preguntas. Yo no tengo las respuestas. Tengo mis propias creencias personales y en esto cada uno debe hacerse las suyas.