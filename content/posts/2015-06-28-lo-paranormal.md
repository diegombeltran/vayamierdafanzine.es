---
title: Lo paranormal
author: Neurona
type: post
date: 2015-06-28T20:16:39+00:00
url: /lo-paranormal/
categories:
  - Blog

---
Aunque la línea de mi vida es en general muy anormal, hubo un breve período de tiempo en el que estuve casado. No me sentía cómodo siendo un anormal 100%, así que decidí intentar sumarme a las reglas de la sociedad.

Al poco de conocer a mi ex mujer, comenzó a prepararme lentamente sobre el Don que poseía. Con algo de vergüenza me confesó que podía ver a gente que ya había fallecido. Este Don lo tenía desde pequeña. Como ser humano abierto de mente que soy, no le di mucha importancia, es más, me gustó enamorarme de una mujer tan especial.

Entre una multitud en la calle, podía diferenciar a un muerto. Éste disponía de otra tonalidad, otro brillo, otra luz, creo recordar que me comentó.

En el primer viaje que hicimos juntos como novios, a las Rías Bajas, ella sentía presencias en la habitación del hotel. Menos mal que me lo comentó de regreso a Zaragoza, ya que aunque estoy abierto a este tipo de circunstancias, soy a la vez un cagueta.

Una vez casados y afincados en nuestro piso, todo se tornó mucho más interesante. Resulta que en nuestra casa había un fantasma llamado Sebastián que había fallecido fruto de un suicidio. Sebastián se llevaba muy bien con mi mujer y siempre vestía en chándal.

Cosas curiosas de Sebastián, las enumero a continuación:

  * No tragaba a un amigo que por aquél entonces vivía en Zaragoza. A veces me daba miedo invitarlo a casa, por si había alguna represalia, aunque nuestro fantasma se contenía. Me fastidiaba que se llevase mal con él, porque nunca me han gustado los malos rollos, ni en el más allá.
  * Cuando estaba con mi ex mujer en el salón viendo la televisión, ella me avisaba que Sebastián estaba haciendo el gracioso y había encendido la luz del baño que estaba al otro lado de la casa al final del pasillo. Yo me asomaba por la puerta del salón, y efectivamente, nuestro ente la había encendido.
  * La cocina estaba junto al salón. El grifo de la fregadera era de palanca. De vez en cuando, estando los dos en el sofá, de repente se abría dicho grifo. Sebastián era muy jocoso, aunque afortunadamente solamente podía comunicarse con mi ex, ya que era la que disponía de dicha sensibilidad.

Cuando yo estaba solo en casa, Sebastián no hacía nada. Quizá era consciente de que mi diarrea mancharía ampliamente el suelo. Era jocoso, pero muy respetuoso.

Aquí culmina mi relación con el mundo de lo paranormal. Agradezco enormemente a mi ex mujer que me brindase haber podido compartir con ella esta experiencia.

&nbsp;