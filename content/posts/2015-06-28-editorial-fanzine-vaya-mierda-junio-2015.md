---
title: Editorial Fanzine ¡Vaya Mierda! Junio 2015
author: Neurona
type: post
date: 2015-06-28T20:15:25+00:00
url: /editorial-fanzine-vaya-mierda-junio-2015/
categories:
  - Blog

---
Caminaba, bajando del tranvía hacia uno de mis bares cutres favoritos de la ciudad en la que resido desde hace más de 30 años.

Acudí con un gran amigo, a hora temprana para poder disfrutar del placer de estar sentados en una vieja y desgastada mesa de madera antes de que decenas de adolescentes asesinaran el mínimo espacio libre, deseosos de una espirituosidad líquida, anestesia de gran parte de nosotros.

Tras varias horas en estado adecuado de toxicidad etílica, pudimos eyacular lo mejor de nuestras mentes en esos momentos. Todo fruto de los datos introducidos en nosotros a lo largo de la vida y de las circunstancias personales.

Deleitamos nuestra nostalgia visualizando un partido de fútbol entre el Celta de Vigo y el F.C. Barcelona del año 1994. ¡Qué fraude! El Dream Team de Cruiff disponía de un nivel de segunda división actual, y eso que ganó una Copa de Europa y varias ligas. Todo evoluciona, hasta algo tan burdo a veces como el fútbol. El Real Madrid de Di Stéfano por ejemplo, no lo veo superior a la segunda división B del año 2015.

Todo va a mejor, digan lo que digan los pesimistas como yo. Lo que ocurre es que la línea no es perfecta, tiene baches, como el actual, fruto de un sistema global satánico.

Entre ataque del Barcelona y contra ataque del Celta, comenté a mi amigo cuál es mi ideal de gestión humana. Una humanidad en la que todos dispongamos de los elementos mínimos adecuados para encontrar la felicidad, sin que existan diferencias en cuanto a nivel adquisitivo disparatadas. El que menos tenga, el que realice la ocupación más sencilla en nuestra sociedad, debería ganar un mínimo adecuado. Por ejemplo se me ocurre, 1.000 euros al mes de la España del año 2015. Con respeto y estabilidad. Las ocupaciones con mayor desgaste, especialización y responsabilidad, deberían obtener ingresos superiores, pero lógicos, por ejemplo, 5.000 euros al mes. Parte de esta idea ya lo recoge un cúmulo de ideas bonitas que quedaron en papel mojado en 1.978.

Algo se desmorona cuando una persona, crea un imperio y no gana 5.000 euros al mes si no, por ejemplo un millón de euros a la semana. Realmente, no llego a comprenderlo. Si se necesita ser millonario para ser feliz es que algo muy malo nos está ocurriendo como especie.

La Rueda de la vida son varios factores que hay que intentar satisfacer. No solo somos comer, trabajar y dormir. (Busquen Rueda de la vida en su buscador de Internet favorito).

Un sistema de gestión humana en mi humilde opinión adecuado debería garantizar el apoyo suficiente a nuestra especie para disponer de una Rueda de la vida saludable.

Ahora bien, Felicidad, ¿a cambio de qué? ¿Y si nos convierten en presos clónicos ignorantes pero felices? Intelecto limitado que no se plantee nada con la simpleza mental por bandera disfrutando de trivialidades 24 horas al día. ¿Lo verían ustedes como algo correcto?

No todo es tan fácil. En mi caso particular, me gusta pensar, aunque ¿si fuera un ignorante crónico pero muriese el más feliz del cementerio? ¿Y si nunca fuera capaz de ser consciente de mi cerebro vacío?