---
title: El lujo de la reproducción humana
author: Neurona
type: post
date: 2015-04-24T18:16:26+00:00
url: /el-lujo-de-la-reproduccion-humana/
categories:
  - Blog

---
Reproducirse es muy grave.  
Ya sebéis que me gusta ser más pesimista que el Antiguo Testamento.  
¿Para qué reproducirse? ¿Para contribuir a la perpetuación de la especie humana? ¿Para no parecer raro, fracasado? ¿Para dar una vida mejor que la tuya a tus hijos?  
Al fin y al cabo todo se ciñe a golpe de biología. Hay que reproducirse para no extinguirse, ahora bien ¿a qué precio?  
A parte queda la capacidad ética de los padres como ejemplo a seguir de los hijos.  
Hoy en día, bajo mi humilde opinión, si formas parte de una clase media y acomodada, es posible que puedas plantearte, o no reproducirte, pero si tu vida en términos económicos está por debajo de dicha clase, tienes que estar muy motivado para nutrir de una fuerza infinita el duro camino de la reproducción humana en nuestro presente.  
Más que nada se debe al grado de complejidad que hemos alcanzado en nuestra sociedad. En vez de ser todo más fácil y placentero, hemos llegado a un momento de nuestra historia en que todo está saturado, inestable y difícil. Y esto afecta terriblemente a la reproducción humana, a los hijos y a los padres.  
Sé que todo irá a mejor en el camino de la humanidad, pero en el presente en el que eyaculo estas palabras, es lo que siento.

_Neurona_