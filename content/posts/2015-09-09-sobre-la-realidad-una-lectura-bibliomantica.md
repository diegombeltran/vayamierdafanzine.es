---
title: Sobre la realidad, una lectura bibliomántica
author: Neurona
type: post
date: 2015-09-09T11:39:43+00:00
url: /sobre-la-realidad-una-lectura-bibliomantica/
categories:
  - Blog

---
Sobre la realidad, una lectura bibliomántica

Podría hablar de lo que creo que es la realidad, podría documentarme y llenar esto de citas de escritores, filósofos o poetas con autoridad que hablaron de ella antes que yo y lo hicieron mejor, podría también darle una entidad concienzuda a todo el texto, podría incluso intentar convenceros de algo, pero me apetece más recurrir a la bibliomancia para responder a la eterna pregunta de qué es la realidad.

Y para que quede claro, dinos RAE qué significa eso de “bibliomancia”.

bibliomancia o bibliomancía.  
(De biblio- y -mancia).  
1. f. Arte adivinatoria que consiste en abrir un libro por una página al azar e interpretar lo que allí se dice.

Pues bien, aclarado el concepto, voy a plantear el procedimiento que voy a seguir.  
Sobre la mesa coloco seis libros cerrados seguidos y voy a plantear tres ejercicios que los libros, por el puro azar de mi mano abriendo uno detrás de otro, responderán. Y al final os diré qué libros he utilizado.

1- ¿Qué es la realidad?  
Libro1) “No sabrías, quiero decir, fuera del balbuceo de cuatro subjetividades imprecisas y poco airosas, tan discutibles como indiscutibles: indiscutibles para ti (¿quién osaría contradecirte?), discutibles para los otros. — ‘Es verdad, no sabría’, pensé.”

Libro2) “Suele considerarse a la fotografía un instrumento para conocer las cosas. Cuando Thoreau dijo «No puedes decir más de lo que ves», daba por sentado que la vista ocupaba el primer puesto entre los sentidos.”

Libro3) “Aquél día, en su despacho, Howard parecía un hombre necesitado de una buena porción de reconfortante poesía. Claire siempre había satirizado sobre su escrupuloso intelectualismo, del mismo modo que él se mofaba de sus ideales artísticos.”

Libro4) “Ocurre a veces que nuestro corazón está como expulsado del cuerpo. Y el cuerpo está como muerto.  
No alcanzamos lo imposible: pero nos sirve como linterna. Evitaremos a la abeja y a la serpiente, desdeñaremos el veneno y la miel.”

Libro5) “Habían llegado al punto muerto de siempre. Ya no cabía más salida que la de ponerse a lanzar venablos contra los premios literarios concedidos de antemano, contra los críticos, contra los jurados y editores, contra la televisión, contra los autores consagrados y nuevos, casi nadie se libraba de la quema.”

Libro6) “Camina, llega a la frontera. Las rapaces revolotean en torno a un centro invisible, probablemente carroña. Los músculos de sus muslos responden con elasticidad a los desniveles del terreno. Una estepa amarillenta cubre las colinas; hacia el este, la vista se extiende hasta el infinito. No ha comido desde la víspera; ya no tiene miedo.”

2- Defina en una palabra qué es la realidad.

Libro1) “Negando.”  
Libro2) “Registrar.”  
Libro3) “Exhaustivo.”  
Libro4) “Familiarmente.”  
Libro5) “Objetos.”  
Libro6) “Pintura.”

3- En una frase proponga una manera de sobrevivir a la realidad.  
Libro1) “Lograba interesarme con cualquier cosa que eligiera, y aún peor, me divertía (peor porque tenía conciencia de que un día me tocaría apartarme).”  
Libro2) “La declaración de que la fotografía ha de ser realista no es incompatible con el ensanchamiento del abismo entre imagen y realidad, en el que ese conocimiento de origen misterioso (y la ampliación de la realidad) que nos proporcionan las fotografías supone una previa alienación o devaluación de la realidad.”  
Libro3) “Kiki guardó silencio, mientras trataba de imaginar a Jerome en ese momento: dónde se encontraba, el tamaño de la habitación, dónde estaba la ventana y qué se veía por ella.”  
Libro4) “Ese instante en que la belleza, después de haberse hecho esperar largo tiempo, surge de las cosas comunes, atraviesa nuestro campo radiante, liga todo lo que puede ser ligado, enciende todo cuanto debe ser encendido de nuestra gavilla de tinieblas.”  
Libro5) “Una vez habían estado hablando bastante a fondo de los argumentos banales y sacaron en consecuencia que la gente que los esgrime es la más desconsiderada y tirana a la hora de exigir atención.”  
Libro6) “Nunca, en ninguna época y en ninguna otra civilización, se ha pensado tanto y tan constantemente en la edad; la gente tiene en la cabeza una idea muy simple del futuro: llegará un momento en que la suma de los placeres físicos que uno puede esperar de la vida sea inferior a la suma de los dolores (uno siente, en el fondo de sí mismo, el giro del contador; y el contador siempre gira en el mismo sentido).”

4- Lista de libros.  
Libro1) Los enamoramientos, Javier Marías.  
Libro2) Sobre la fotografía, Susan Sontag.  
Libro3) Sobre la belleza, Zadie Smith.  
Libro4) Indagación de la base y de la cima, René Char.  
Libro5) Irse de casa, Carmen Martín Gaite.  
Libro6) Las partículas elementales, Michel Houellebecq.

Por Susana Portero.