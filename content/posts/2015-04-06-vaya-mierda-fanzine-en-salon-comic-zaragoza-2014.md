---
title: ¡Vaya Mierda! Fanzine en Salón cómic Zaragoza 2014
author: Neurona
type: post
date: 2015-04-06T11:18:52+00:00
url: /vaya-mierda-fanzine-en-salon-comic-zaragoza-2014/
categories:
  - Blog

---
“Allá vamos otra vez” dijo una vez el androide C3PO en la clásica saga de “La Guerra de las Galaxias”. Así nos pasó a nosotros. Sabíamos que nuestro nombre (pffffff) y reputación (JAJAJAJA) nos tenían asegurado un puesto en el Salón del Cómic de Zaragoza. Aunque hubiéramos tenido un parón de un año.

Con un equipo renovado que ha sabido estar muy por encima de las expectativas, el reencuentro con viejos compañeros del evento, las actividades, charlas, los otros expositores y la gente que han venido al Salón y pasado por nuestro stand han hecho un fin de semana muy agradable. Ya nos quedan pocas camisetas de edición limitada de la cucaracha y la ameba y como cada año, gracias a las aportaciones de la gente nos hemos sacado mínimo una página de contenido por el morro (¡¡yujuuuu!!). Desde aquellos que nos compraron un fanzine a lo “esto qué narices es” hasta aquellos que sin dudar, dijeron “dame uno de cada” , a todos&#8230; GRACIAS!! porque con vuestro apoyo sumado a nuestra ilusión tenemos ganas para rato de crear fanzines. Dad por seguro que salvo hecatombe, el año que viene repetimos.