---
title: Tarde social y bar Linares
author: Neurona
type: post
date: 2014-08-26T17:35:23+00:00
url: /tarde-social-y-bar-linares/
categories:
  - Blog
tags:
  - Tarde de vida social

---
[<img loading="lazy" class="alignleft size-full wp-image-761" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares1.jpg" alt="En Bar Linares Zaragoza" width="337" height="722" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares1.jpg 337w, https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares1-140x300.jpg 140w" sizes="(max-width: 337px) 100vw, 337px" />][1]Era una tarde veraniega de sábado en la cuál me apetecía vida social y visitar diversos parajes de la capital maña. Era consciente que al día siguiente lo pagaría, pero no puedo evitarlo. Soy así a causa de una extraña mutación en la orina.  
Comenzamos la sesión degustando varias cervezas en la feria de la cerveza artesana que todos los años se celebra en el centro de historias de Zaragoza. Fueron 4 modalidades bírricas las que pasaron por nuestro paladar, de graduaciones entre 4º y 10º acompañadas por un perrito caliente para amortiguar el efecto alcohólico en nuestro ser.  
Posteriormente, nuestro deber como zaragocistas, nos empujó a disfrutar del primer partido de liga en Taberna Urbana en el barrio de la Madalena. Esta vez la cerveza era industrial de la archiconocida marca en mañolandia «Ámbar».  
La ingesta del elemento líquido primario se dejaba ya palpar en mi sistema neuronal, por lo que al finalizar el encuentro, al acceder a cenar en el restaurante vegetariano del mismo barrio «Birosta», metí la manga de mi camisa en el gazpacho. Es algo inevitable en mí, cuando bebo bastante cerveza, meto las mangas en fluidos de colores llamativos.  
Dicho restaurante vegetariano destaca por poseer una zona de biblioteca donde los clientes pueden disfrutar de diversos títulos en papel. Este hecho me hizo reflexionar para concluir en el siguiente pensamiento, ¿en un futuro casi todos los libros serán digitales y solamente se utilizará el papel para limpiarse el culo? ¿O también inventarán papel higiénico no analógico? (aunque se utilice en una zona cuyo nombre incluye parte de la palabra analógico).  
La grata compañía con la que contaba junto con las ingestas varias producían en mí un estado de anormalidad superior a lo habitual. Sinceramente, siento algo de vergüenza en estos casos, aunque la gente que me sufre, realmente no le da importancia. Creo que influye en mi percepción la paranoia elevada de mi Yo interno.  
En la recta final de la tarde-noche hicimos una visita a un bar auténtico. El bar Linares. Pequeñito, acogedor y poseedor de una gramola donde todo el mundo se amontona ansioso para saberse protagonista de obligar al resto de clientes a escuchar su tema [<img loading="lazy" class="size-full wp-image-762 alignright" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares2.jpg" alt="Gramola Bar Linares Zaragoza" width="351" height="488" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares2.jpg 351w, https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares2-215x300.jpg 215w" sizes="(max-width: 351px) 100vw, 351px" />][2]favorito a cambio de un intercambio monetario.  
El local está regentado por dos personas con gran experiencia que brindan su amor y amabilidad a los clientes. Los precios me parecieron muy aceptables y la calidad de las espirituosidades excepcional.  
Tras la visita a este rinconcito zaragocil, aterrizamos en la conocida zona de «El casco». Me dejé caer por «La recogida» donde el rock hace acto de presencia en  abundante diversidad y por una chupitería llamada «Espit chupitos». Tras pedir para nuestro grupo de personas una coctelera cuyo contenido tenía un sabor afrutado poco espirituoso que daba para unos 12 chupitos, intenté catar leche de pantera, pero me fue imposible ya que los camareros estaban muy entretenidos en hacer malabares con fuego y bebidas varias. La verdad es que esta zona no me hace sentir mucho placer, soy más hombre de tabernas y antros perdidos en los rincones más inhóspitos de las urbes más antiguas.  
A las 2am me subí en un taxi y di por cerrada la jornada de investigación.

<p style="text-align: right;">
  <em>Neurona</em>
</p>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares1.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2014/08/linares2.jpg