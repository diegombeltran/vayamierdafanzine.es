---
title: Mucho ruido, pocas nueces
author: Neurona
type: post
date: 2015-04-15T18:32:42+00:00
url: /mucho-ruido-pocas-nueces/
categories:
  - Blog

---
Actualmente estamos en la era audiovisual. Hay estudios que dicen que se producen tres veces las películas y series que hace veinte años. Concretamente en lo que a series respecta vivimos su era más brillante con presupuestos que rivalizan grandes producciones de cine con actores de primera fila. Pero sin embargo al mismo tiempo vivimos una especie de bombardeo mediático tan brutal y salvaje que es como si nada de lo que vemos realmente nos calase hondo y nuestras sobre estresadas sinapsis neuronales tan sólo estuviesen esperando al siguiente chute de imágenes por segundo.

De igual manera hay estudios médicos que dicen que toda esta sobre estimulación es adictiva: cuando el cerebro se ajusta a un determinado nivel de imágenes o situaciones procesadas al día, posteriormente demanda continuar con ese nivel de excitación y precisa de un periodo de adaptación para bajar a un nivel inferior.

Podemos encontrar una analogía en el séptimo arte en las películas del productor y director Michael Bay. Para quien no le suene&#8230; La Roca, Armaggedon, Pearl Harbor, Dos policías rebeldes, las más recientes Transformers y la última adaptación de Las Tortugas Ninja. Director criado profesionalmente en publicidad y vídeos musicales con varios premios de la MTV en sus espaldas, ha trasladado esta forma de entender la comunicación audiovisual al cine. Grandes explosiones, planos cortos, escenas rápidas con cortes frenéticos&#8230; un estilo ¿ideal? Para películas de acción y aventuras. Probablemente el exponente más extremo sea la última película de Transformers: La Era de la Extinción. “El paraíso de un chatarrero” la llamó un amigo por la cantidad de piezas volando que se ocasionaban mutuamente autobots y decepticons repartiéndose tollinas. En suma, el triunfo de lo visual&#8230; ¿por encima del guión? En este caso desde luego. Pero&#8230; ¿llegará a tener más importancia la forma que el fondo en las futuras producciones?

Que este tipo de cine es rentable es indudable. A pesar de los tremendos costes en producción y publicidad, los beneficios son considerables. También que la profundidad argumental y los mensajes que se transmiten son bastante superficiales. El mismo director dice que él hace cine para adolescentes. Es entretenimiento puro y duro, dos horas de acción y aventuras en los que desconectar del mundo y la realidad. Y probablemente encaja perfectamente en esta sociedad en la que nos vemos inmersos donde todo es usar y tirar.

Mucha gente ve este tipo de cine como una especie de abominación. Yo particularmente no lo veo especialmente malo, he disfrutado con varias de estas películas y he pasado un buen rato. Es todo un espectáculo que en muchas ocasiones merece la pena ver en pantalla grande. Incluso afirmo que este director tiene un estilo, un lenguaje propio que se puede interpretar y analizar. Pero sí veo un peligro en lo que se podría convertir el cine como medio artístico si las producciones tendiesen mayoritariamente a este tipo de formas de expresión, ya que se vaciaría de significado una forma de arte. Este peligro no es baladí si tenemos en cuenta la degeneración progresiva de la educación en España: es verdad que siempre habrá gente con inquietudes para expresar cosas, pero si la masa poblacional se aborrega&#8230; no hay mejor cine para ellos ya que simple y llanamente, no hay que pensar.

Lo que sí me resulta tremendamente llamativo es la poca repercusión social a medio y largo plazo que deja este tipo de cine. La gente recuerda más Armaggedon y La Roca, dos cintas que tienen un guión y un argumento muchísimo más trabajado, además de un reparto de mayor calidad, que Transformers por ejemplo, siendo muchísimo más rentables estas últimas ya por su cuarta entrega. Y aún podemos decir que el poso social de todas estas no tiene nada que ver con lo que ha dejado en el imaginario colectivo producciones muy anteriores en el tiempo como Indiana Jones o La Guerra de las Galaxias. ¿Crisis de creatividad?&#8230;Bajo mi punto de vista, desde luego.

<p style="text-align: right;">
  Nolfy
</p>