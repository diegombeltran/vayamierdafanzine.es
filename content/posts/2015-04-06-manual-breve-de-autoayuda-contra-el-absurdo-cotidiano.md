---
title: Manual breve de autoayuda contra el absurdo cotidiano
author: Neurona
type: post
date: 2015-04-06T11:14:17+00:00
url: /manual-breve-de-autoayuda-contra-el-absurdo-cotidiano/
categories:
  - Blog

---
«Absurdity is what I like most in life, and there&#8217;s humor in struggling in ignorance.»  
-David Lynch  
Antes de comenzar este artículo, y traducir la cita inicial en algún momento (no diré cuándo: sigan leyendo por favor, déjense sorprender), me gustaría llamar a la puerta y saludarles afectuosamente. Damas y caballeros, aquí comienza mi estreno vayamierdero/vayamierdesco, o como prefieran, pues no se trata de elegir una respuesta correcta (en la vida no hay respuestas correctas, sólo elecciones, también pueden elegir no elegir, lo que ustedes quieran, posibilidades infinitas, ya saben, arriésguense). AVISO. Continúo enseguida, sólo quiero comentarles de antemano que tiendo a utilizar largos paréntesis, no desesperen, es que quiero explicarme bien cueste lo que cueste, y los paréntesis me caen bien, tienen un je ne sais quoi inconsciente, una cualidad subterránea, un matiz oculto, aportan un extra preciso y muy personal incluso, un bonus track patrocinado por el mundo interior de cada cual. Igual me he pasado. Bueno, ya.

En cualquier caso esto no es un concurso de la tele y no es la vida y no hay que elegir. Esto sí es un artículo o un intento de artículo, así que lean esto, sea lo que sea, porque claro, hay palabras escritas, muchas, 2600 al final, como máximo, si todo va bien, o tal vez menos, sí, por si les apetece contarlas, pues hay quien tiene hobbies extraños, insólitos, yo misma me cuento dentro de ese club selecto de personas que se dedican a las más raras actividades, como por ejemplo coleccionar los resguardos ajenos de la biblioteca; pero eso es otra historia que les contaré otro día, si me dejan.

Ya vale, que no “he venido a hablar de mi libro”, he venido a hablarles de LO ABSURDO, que es lo que más me gusta a mí de la vida junto con los canelones de mi madre, y a David Lynch pues parece que también, que es muy de gustarle eso (los canelones de mi madre no, el absurdo).  
Aquí es donde paramos un microsegundo para observar la definición de “absurdo” y dejarlo todo claro por si las moscas.

absurdo, da.  
(Del lat. absurdus).  
1. adj. Contrario y opuesto a la razón; que no tiene sentido. U. t. c. s.  
2. adj. Extravagante, irregular.  
3. adj. Chocante, contradictorio.  
4. m. Dicho o hecho irracional, arbitrario o disparatado.  
Real Academia Española © Todos los derechos reservados

“Sí, muy bien, nos queda perfectamente claro qué es el absurdo, de hecho ya lo sabíamos, mira que poner la definición de la RAE…”, pueden estar pensando algunos de ustedes. ¿Pero, a todo esto, quién es David Lynch?  
David Lynch, para los que no lo conozcan, es un director de Montana, Estados Unidos (según me dice Wikipedia, fuente de sapiencia infinita). Bien, pues este gran artista que cuenta en su haber con numerosas joyas cinematográficas como son “Eraserhead” o “Mulholland Drive”, por mencionar algunas, y que deben ver si no lo han hecho ya, este señor del Renacimiento que tan pronto te explica una receta de quinoa en un vídeo de YouTube, como saca al mercado su propia marca de café, o un disco, o dice que se va a poner a diseñar ropa, y que ahora, después de 25 años, va a continuar con el hito televisivo que supuso su serie “Twin Peaks”, este ser humano excepcional sin duda alguna, que además promueve la meditación trascendental allá por donde va, que parece estar más ocupado y tener más energía que Beyoncé, dice que el absurdo es lo que más le gusta de la vida y que encuentra humor cuando tiene problemas con la ignorancia (traducción, ahora sí, de la cita inicial).

Ahí es nada.  
Más allá de una cita, nos encontramos con toda una filosofía de vida, pues el humor y sólo el humor nos salvará ante el absurdo cotidiano aleatorio y permanente. Así que rían, no paren de reír, pues “un día sin reír es un día perdido” (Charles Chaplin), y “dientes, dientes, que eso es lo que les jode” (Isabel Pantoja).

Isabel Pantoja, en esa maravillosa y sabia frase para la posteridad, se refería a esos periodistas que la perseguían incansablemente, también llamados paparazzi (palabra en desuso, por desgracia, que me remite a años pasados) pero seguro que ustedes encuentran otros focos humanos sobre los que volcar una sonrisa profident a tiempo, sobre los que la risa dejará en un estado de shock, sin saber qué hacer, totalmente desvalidos.

Y es que el espectro de humanos a los que sonreír puede ir desde el conductor de autobús que decide darle a uno con la puerta en las narices (a quién no le ha pasado), a la señora que decide colarse descaradamente en la cola del supermercado (mítico), pasando por ese jefe (en el caso de ser autónomo el enemigo será usted mismo claro), ese cuñado, o suegro, o \___\___\___\____(rellene aquí con su caso personal) que a uno le perturba el ánimo durante las reuniones familiares.

Imagínese que usted lo ha intentado una y otra vez, ha probado con el humor en toda circunstancia molesta, perturbadora o difícil, ha probado a reír, a forzar una sonrisa sin conseguirlo, entonces, en ese instante, ¿qué le queda?

Tal vez decide, en ese segundo de desesperación, mirar al suelo. La situación es la siguiente. Está usted caminando por la ciudad, solo, con las manos en los bolsillos de su abrigo, ha llovido (la lluvia siempre aporta un plus de dramatismo), el suelo está mojado, el cielo gris, usted se siente así, como gris, y mira al suelo. Mira al suelo. Y ahí, en ese suelo húmedo, encuentra un papel que le ilumina la mente con una idea, con una dirección que seguir.  
Le muestra el camino.

Sí, se trata del “gran vidente médium competente soluciona todos los problemas en 72 horas”, también conocido como profesor Dielany (puede que también le suene el Maestro Barro, la oferta es amplia, el diseño de los flyers con los que se anuncian muy parecido).

Flyer encontrado en el puente de Santiago, en un día de lluvia.  
Aquí lo tiene, el todopoderoso Dielany, que se ofrece para ayudarle en cualquier situación que se precie, y además define su trabajo como serio, con seguridad, garantía y confianza. Para qué más.

Si finalmente el profesor no logra ayudarle (todo puede suceder) existen más opciones para salir del atolladero, del absurdo, sí, como por ejemplo buscarse una afición extraña, o comer chocolate, hacer punto de cruz, contar palabras o ver partidos de fútbol. Hay gente para todo.

Sea lo que sea que elija, elija la vida siempre, elija reírse y no enfadarse, para qué.

[<img loading="lazy" class="alignleft size-full wp-image-924" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/04/foto-artículo-susana.jpg" alt="foto-artículo-susana" width="480" height="350" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/04/foto-artículo-susana.jpg 480w, https://www.vayamierdafanzine.es/contenido/uploads/2015/04/foto-artículo-susana-300x219.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/04/foto-artículo-susana-411x300.jpg 411w" sizes="(max-width: 480px) 100vw, 480px" />][1]

Susana Portero

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/04/foto-artículo-susana.jpg