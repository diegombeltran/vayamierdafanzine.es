---
title: Ver, oír y callar.
author: Neurona
type: post
date: 2016-03-09T10:32:08+00:00
url: /ver-oir-y-callar/
categories:
  - Blog

---
Para sorpresa de todos vosotros, en este número no voy a torturaros con ningún laberinto mental cinematográfico. Vuestra cabeza ya ha sufrido bastante como para martirizarla más este mes.

Esto no significa que vaya a ser benévola esta vez y os haga un pequeño análisis de _Zoolander_ o _Atrapa la bandera_, no. En este caso, quiero atacar otra parte de vuestro ser que utilizáis aún menos que la cabeza. Con la película que os traigo, pretendo poner a prueba lo poco que os queda de corazón y, poniéndonos espirituales, incomodar el trocito de alma que aún no habéis vendido.

_El club_ del cineasta chileno Pablo Larraín nos introduce la compleja rutina de cuatro curas exiliados en una casa que denominan “de retiro espiritual” donde se ven obligados a vivir el resto de sus días para purgarse de los pecados cometidos durante su sacerdocio. Vigilados bajo la maternal mirada de la hermana Mónica, intentan sobrevivir en este aburrido y kantiano limbo al que han sido desterrados. La paz y tranquilidad que intentaban mantener en esas cuatro paredes se quiebran tras la entrada de un quinto cura, cuyo “desliz” pondrá en peligro la integridad de todos los penitentes.

Larraín canta la cruda historia sobre los abusos sexuales perpetuados por curas y religiosos, un estribillo que todos conocemos y que nos produce rechazo tararear. Aunque la pederastia es el conflicto principal de la película, el chileno profundiza de una forma plausible en esa necesidad de purgación que tiene el hombre, ya sea ateo o religioso, esa lista del karma que nos pesa y que, a no ser que se nos haya concedido el don de no tener conciencia, nos sentimos obligados a solucionar “por si acaso”.

A través de los protagonistas del film, el director se atreve a plantear polémicas cuestiones que todavía la sociedad se prohíbe comentar. Uno de los curas, el padre Vidal, construido e interpretado por Alfonso Castro de manera impecable, se autodenomina como “el rey de la represión”. Se atribuye a sí mismo ese “mérito” por saberse pedófilo y controlar sus impulsos para no dañar a los inconscientes monaguillos. Es incuestionable que la pederastia es un crimen y destroza para siempre las vidas de niños abocados a los antidepresivos y tranquilizantes hasta que mueran. Ahora, ¿qué es la pedofilia? ¿Es una condición sexual o una enfermedad? En el caso de que sea una enfermedad, un trastorno mental, ¿hay alguna solución médica que no sea el electroshock para curarles?

Os recomiendo infinitamente que veáis esta obra, pero cuidado, hiere la sensibilidad y os dará bastante que pensar. Lo siento, pero si no, no estaría aquí.

Por Patry Torres.