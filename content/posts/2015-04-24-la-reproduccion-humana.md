---
title: La reproducción humana
author: Neurona
type: post
date: 2015-04-24T18:31:29+00:00
url: /la-reproduccion-humana/
categories:
  - Blog

---
Es curioso como somos los seres humanos. Hasta tal punto que yo a veces me pregunto si somos comunalmente así de bobos o si realmente hay una voluntad oscura, ya sean demonios, extraterrestres o malvados de película de talla global empeñados en hacernos la puñeta como especie.

Lo digo porque habiendo alimentos de sobra en el mundo para la población mundial sigue habiendo países que sufren hambrunas mientras en otros lugares sobra comida que se tira a la basura. Mientras en un sitio hay medios ampliamente difundidos para el control de la natalidad, en otros lugares a pesar de existir igualmente, la presión social o religiosa de comunidades quizá con menos formación cultural y pensamiento crítico entorpece su uso. Al mismo tiempo mientras en el mundo desarrollado se hace (por lo general) una mayor valoración de los pros y los contras, de la situación económica y laboral de la pareja antes de tener un hijo (en muchos casos en países con una gran despoblación en zonas rurales) en el mundo subdesarrollado o en vías de desarrollo los hijos se ven como una necesidad, como una ayuda de mano de obra a partir de la adolescencia o de la niñez para las labores del hogar e incluso para buscar un complemento salarial. Como especie vivimos en el absurdo. Y no precisamente del que hace gracia.

¿Cuál es el verdadero valor de la vida humana? La respuesta a esta pregunta podría dar testimonio del verdadero valor que tenemos como especie. Tristemente hay que decir que depende. Tras una corriente de desarrollo de derechos sociales de protección de la Humanidad y de la infancia tras la Segunda Guerra Mundial (Declaración Universal de derechos humanos, 1948; Declaración de los derechos del niño, 1954) y el desarrollo del llamado Estado del Bienestar parecía que la Humanidad en su conjunto caminaba poco a poco hacia un cambio de paradigma, una conciencia en la que la importancia de la vida humana como especie, independientemente del color, raza o religión se iba abriendo camino a pesar de las diferencias entre países. Sin embargo, desde el inicio de la crisis económica de 2007 si bien estos valores a nivel general no se han perdido, sí se han visto en gran medida relativizados. Han perdido importancia frente a otros problemas inmediatos. En el año 2015 básicamente todos vivimos peor, aunque unos mucho peor que otros&#8230; obviamente las economías de las llamadas democracias avanzadas aunque han perdido derechos, no han perdido tanto como las economías de países subdesarrollados. En unas se sigue incentivando con considerables ayudas sociales la natalidad, mientras en otras&#8230; siguen igual o peor. Al final el valor de la vida humana tal y como tenemos montado el chiringuito&#8230; se mide con dinero. Triste pero cierto.

Nolfy