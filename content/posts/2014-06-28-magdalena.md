---
title: Magdalena
author: Neurona
type: post
date: 2014-06-28T07:49:38+00:00
url: /magdalena/
categories:
  - Blog
tags:
  - cómic absurdo

---
Os dejamos una adaptación recién sacada del horno.

[<img loading="lazy" class="alignleft size-full wp-image-667" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/magdalena.jpg" alt="magdalena" width="912" height="313" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/magdalena.jpg 912w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/magdalena-300x102.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/magdalena-500x171.jpg 500w" sizes="(max-width: 912px) 100vw, 912px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/magdalena.jpg