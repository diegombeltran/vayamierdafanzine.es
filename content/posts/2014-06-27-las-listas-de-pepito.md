---
title: Las listas de Pepito
author: Neurona
type: post
date: 2014-06-27T08:09:10+00:00
url: /las-listas-de-pepito/
categories:
  - Blog
tags:
  - parida

---
[<img loading="lazy" class="alignleft" src="http://www.hacercurriculum.net/wp-content/uploads/2011/10/Haz-una-lista-antes-de-buscar-empleo.jpg" alt="" width="265" height="300" />][1]{.broken_link}Pepito tenía un padre especial. No padecía de ningún grado de oligofrenia, pero disponía de una forma de ser que le hacía sentirse orgulloso del regalo espermático que brindó a su madre.

Un buen día su padre le dijo que no se olvidase de la lista de tareas vitales que le había preparado. Le haría muy feliz que su hijo fuese capaz de realizarlas.

Para haceros una idea del grado de ingenio de su padre, os voy a plasmar aquí dicha lista:

&#8211;          Comer todos los días

&#8211;          Orinar 4 veces al día

&#8211;          Expulsar heces una vez por día

&#8211;          Respirar continuamente

&#8211;          Comprar mazapán con forma de Spider Man

&#8211;          Evitar la diarrea

Aunque le costó hacer algunas más que otras, con un poco de práctica, buen hacer e insistencia, consiguió cumplirlas «a rajatabla».

Su padre le dijo que cuando consiguiera dominar esta primera lista, le haría entrega de otra completamente diferente, y así sucesivamente hasta encontrar la sabiduría.

Pepito lleva 15 años superando una por una todas las listas de su padre, y realmente se siente más sabio y una persona más completa.

<p style="text-align: right;">
  <em>Neurona</em>
</p>

 [1]: http://www.hacercurriculum.net/wp-content/uploads/2011/10/Haz-una-lista-antes-de-buscar-empleo.jpg