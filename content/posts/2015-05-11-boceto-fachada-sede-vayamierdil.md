---
title: Boceto fachada sede vayamierdil
author: Neurona
type: post
date: 2015-05-11T22:21:40+00:00
url: /boceto-fachada-sede-vayamierdil/
categories:
  - Blog

---
Nuestro intrépido, genial y underground diseñador contraportadista de ¡Vaya Mierda! Fanzine [Iñaki Goñi][1]{.broken_link}, será el encargado de realizar el pintado de fachada de nuestra sede.

Os presentamos dicho boceto. Va a quedar niqueladico.

[<img loading="lazy" class="alignleft size-full wp-image-1003" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/boceto-sede-local.jpg" alt="Boceto fachada sede ¡Vaya Mierda! Fanzine" width="1000" height="709" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/boceto-sede-local.jpg 1000w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/boceto-sede-local-300x213.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/boceto-sede-local-423x300.jpg 423w" sizes="(max-width: 1000px) 100vw, 1000px" />][2]

 [1]: https://www.facebook.com/santoespanto "Santo Espanto"
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2015/05/boceto-sede-local.jpg