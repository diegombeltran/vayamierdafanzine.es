---
title: El 90% de mis amigos son gilipollas (pero con amor, eh)
author: Neurona
type: post
date: 2015-04-06T11:23:22+00:00
url: /el-90-de-mis-amigos-son-gilipollas-pero-con-amor-eh/
categories:
  - Blog

---
Desconocidos.  
Tengo 160 amigos en Facebook. Hermanos, ex (no), amigos de la uni, bodegones, el erasmus, el colegio, los colegios, y alguno más. El grueso del Erasmus. Un ex árbitro internacional. Amigos de la época de BCN. Ex del trabajo y poco más.  
Un jesuita. Los del campo de trabajo, los del campamento, ex profesores, hijos de amigos. Amigos del hoy, del mañana, y también del ayer (y enriquecidos en Calcio). Ex jefes. Novias de amigos y algún que otro foráneo. Bebés de hijos no natos que en realidad son de Google Glass.  
También pertenezco a algún grupo friki de mujeres musculosas y fotos de vaqueros, pero esta vez no cuentan.  
Muchos de los amigos que tengo no sé si darían un duro por mí, el 99% no conozco cuándo cumplen años, ni si quiera tengo su nombre en una agenda de verdad, y casi el 100% nunca mantuvo correspondencia por carta conmigo (yo tampoco con ellos), aunque se salvan benditas rarezas.  
Sin embargo sigo viendo vuestras fotos en Tailandia, Vietnam, vuestros pies encima de maletas, dedos en forma de v, bebés recién nacidos no natos, y fotos del perro en el Pirineo.  
Y el cumpleaños de Isabel, y Hugo en el camping, y tú y ella, por fin juntos. Tardes de terraceo, y así da gusto despertarse.  
Casi el 100% de nuevo nunca me invitó a merendar y con seguridad del 100% nunca iremos juntos al altar.  
Molaría que Google y su Cultural Institute sacara un mercado bereber fomentando el intercambio colectivo de amigos de Facebook. Yo te paso este, tú aquel. Lo ten, me fal. Compartimos amigos. O cacería nocturna, asociación de amigos en Facebook nocturna, ¿no?  
Con sus tonos sepias, fotos de viajes a Tailandia, el combo. Packs de amigos felices (La Buena Vida, el Buen Amigo, lo anunciarán en el Corte Inglés).  
¿Por qué me seguís? ¿Por qué me enviáis vuestras fotos y solicitudes de buen rollo? Hijos de la soja, ¿qué queréis de mí? Soy un tipo normal, me gusta la pizza y dormir. Mirar entre las cortinas y odio los equipos de veteranos, con sus excesos en carnes, másteres e inversiones.  
Tampoco publico fotos, porque las tengo «capadas», sólo alguna frasecilla.  
Nada interesante ciertamente, al margen de hacerme propia gracia a mí mismo.  
Doy publicidad también a algún blog que tengo, pero tampoco creo que hayáis llegado hasta mi tablón por ellos.  
Tampoco doy nunca un like por amor al tuit, ni al post ajeno. ¿Qué queréis realmente de mí? No tengo sponsors, si quiera órdenes de alejamiento.  
Llámalo x. Llámalo pánico virtual.  
Sólo me quedan estas líneas para despedirme en mitad de la nada. Para describiros.  
Hijos del Candy Crash. Y dejaros a continuación este fugaz análisis que os he hecho, con el que pongo final a nuestra relación virtual:  
El 17,5% de los nombres los copan: Paula, María, Sara, Carlos, Cristina, Pablo, Ana y Alba. Nombres muy comunes.  
61,80% de los 160 son fiesteros. El 98,75% son famosos en su casa.  
Tan sólo el 1,2% son famosos: ex árbitro internacional, y R2D2.  
El 13, 1% de mis amigos en Facebook son gordos.  
Afirman tener doble Personalidad el 0,625%. Por otro lado, entre flipados-van de guays-exhibicionistas y divas del Motor tenemos un 43,75%. Un número muy elevado, curioso dato&#8230;  
Me han enviado alguna vez una postal&#8230; el 3,125%.  
Me iría de cañas con ellos, 65,6%.  
Eliminaría pero no lo haré: el 35%.  
Darían un duro por mí: el 19,3% (incluso quizá demasiado).  
Gente genuina, el 16,875%.  
Compartir una noche de Motel, 21,87%.  
Atentamente.  
Javier Burguinho.  
*Estadísticas reales sobre amigos de Facebook con los que en el tablón pasábamos las horas.