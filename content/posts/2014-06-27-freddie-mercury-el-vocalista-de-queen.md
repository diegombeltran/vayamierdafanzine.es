---
title: Freddie Mercury, el vocalista de Queen
author: Neurona
type: post
date: 2014-06-27T08:16:56+00:00
url: /freddie-mercury-el-vocalista-de-queen/
categories:
  - Blog

---
[<img loading="lazy" class="size-medium wp-image-649 alignleft" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/fm-203x300.jpg" alt="Freddy Mercury por Nabey" width="203" height="300" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/fm-203x300.jpg 203w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/fm.jpg 463w" sizes="(max-width: 203px) 100vw, 203px" />][1]¿Cuántas veces nos hemos preguntado quiénes somos en realidad? ¿Cuántas veces por culpa de convencionalismos sociales, familiares, religiosos, ético-morales… nos hemos sentido culpables? ¿Cuántas veces hemos reprimido nuestros deseos verdaderos por no estar bien considerados? ¿Cuántas veces nos hemos fallado a nosotros mismos por esto?

Últimamente he pensado mucho en todas aquellas cosas que dejamos atrás en el camino de la vida por no fallar al entorno, y no hablo de aquello actos que suponen el dolor o la muerte de otra persona (no estoy hablando de matar o agredir), sino de aquellos actos que, siendo en beneficio propio o por deseo, no llegamos a realizar. De aquellas vías del camino que no cogemos porque no es lo que se espera de uno.

Conozco muchos casos de gente que no ha estudiado lo que de verdad quería por no fallar a sus familiares. También conozco gente que cada día se esfuerza por aparentar ser lo que no es, gente que copia movimientos, vestuario y gestos de otras personas y que se acaban perdiendo en las propias arrugas de sus ropas y de sus sueños. Pero, sin lugar a dudas, una de las manifestaciones de anulación del yo, que mi ser altruista en estos momentos quiere resaltar, es la anulación de la sexualidad.

Seguro que todos, repasando mentalmente, conocemos algún caso de alguien que, aun sabiendo todo el mundo que es homosexual, tiene una relación heterosexual. ¿Es eso justo? No, no es justo que alguien deba reprimir su yo sexual, por miedo, porque no es otra cosa.

No hablo de una declaración pública de la homosexualidad. Yo no me presento con mi nombre, apellido y acuño «heterosexual, encantada». Hablo del guardar las apariencias, de la persecución que han sufrido los homosexuales y que, por desgracia, aún sufren. Las etiquetas («Sí, Fulanita, la amiga lesbiana de tal», «No, el que tú dices es Mengano, el gay»), la relación directa que se les atribuye a enfermedades de transmisión sexual (ETS), pues parece ser que hay ingenuos peligrosos todavía en este mundo que no saben que mediante una relación heterosexual sin protección o a través de la sangre, como ocurre en el caso de los heroinómanos, también se transmiten las ETS. Y digo ingenuos peligrosos porque la desinformación en este tema en concreto, es muy peligrosa. Pero es mucho más cómodo pensar que la culpa y la responsabilidad es de otros.

Tras 22 años después de su muerte, Freddie Mercury (Farrokh Bulsara, Zanzíbar 1946-Londres 1991) aún es recordado por muchos con etiquetas: el cantante homosexual, sí, ese, el que tenía SIDA. Freddie Mercury era una voz, pero era también un hijo, un amante. Recordémoslo entonces como el cantante que murió, como muchos otros, que vivió como pudo algunas veces, como quiso otras. Recordémoslo como la voz de Queen.

<p style="text-align: right;">
  <em>Nene</em>
</p>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/fm.jpg