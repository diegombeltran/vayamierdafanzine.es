---
title: Coherence
author: Neurona
type: post
date: 2015-04-15T18:25:10+00:00
url: /coherence/
categories:
  - Blog

---
A partir de este número, y hasta que me echen, tendré el privilegio de compartir con todos ustedes todo tipo de rarezas del séptimo arte. Hablando en plata, voy a romperos la cabeza con temas grotescos, personajes bizarros e historias que nunca debieron ver la luz.

Con motivo de mi desvirgamiento “vaya mierdil”, he escogido una película apta para todas las cabezas, aunque no por eso menos brillante. Se trata del film estadounidense _Coherence_, un largometraje que trastocó al público de Sitges y que tendremos la oportunidad de ver en cartelera a finales de este mes.

_ _James Ward Byrkit, progenitor de la película, se desmarca por primera vez del cine más comercial (Rango, Piratas del Caribe&#8230;) para jugársela en el ámbito puramente experimental. Y no le salió mal, pues con apenas 50.000$ de presupuesto (en la industria cinematográfica no te da ni para pan duro), su propia casa como único escenario, ocho actores hasta ahora desconocidos y un guion abierto a la improvisación, _Coherence_ es una master class sobre la idea como verdadera esencia de una película, con independencia de los recursos que poseamos.

Voy a dejarme ya de formalidades para hablaros de lo que realmente importa, ¿por qué esta película? Bien, la trama principal de _Coherence_ se construye en base a una paradoja que a todos nos suena de algo: El gato de Schrödinger. Supongamos que tenemos a un gato encerrado en una caja, en la cual hay además un dispositivo con veneno el cual tiene el 50% de posibilidades de ser activado. Según la física clásica, al abrir la caja el gato puede estar muerto o puede estar vivo. Sin embargo, para la mecánica cuántica, el gato está vivo y muerto a la vez.

En esta película no hay gatos, sino ocho amigos que se reúnen para cenar ajenos a las anomalías que sufrirán a lo largo de la noche. Estas aberraciones, físicamente inexplicables, son atribuidas a la acción de un extraño cometa llamado Miller. El meteorito es el responsable de lo que parece ser un desdoblamiento en el tiempo, es decir, el culpable de la creación de universos paralelos.

Sé que esto ha sonado a chino, así que a continuación lo explicaré sin formulismos ni Schrödingers. Desde que nos levantamos hasta que nos acostamos hacemos miles de elecciones, aunque algunas son más relevantes que otras, todas ellas definen quienes somos. ¿Cómo sería yo si hubiera hecho esto o aquello? Si pudiéramos ver quienes seríamos de haber tomado decisiones distintas, observaríamos a miles de versiones de nosotros mismos. Esta es la función del cometa Miller, desplegar todas esas realidades de nosotros; mostrándonos al gato vivo y al gato muerto.

Si por alguna razón, ni Palafox ni Aragonia deciden incluir este film entre sus películas mierderas, vedla en series.ly (a mi pesar), porque esta historia merece la pena.

<p style="text-align: right;">
  Patry Torres
</p>