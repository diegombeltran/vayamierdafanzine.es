---
title: Editorial fanzine ¡Vaya Mierda! Marzo 2016
author: Neurona
type: post
date: 2016-03-09T10:32:57+00:00
url: /editorial-fanzine-vaya-mierda-marzo-2016/
categories:
  - Blog

---
¿La mediocridad humana es la causante de que cuatro personas gobiernen el mundo?

¿El liberalismo económico suele descontrolarse para que cuatro empresas compren las demás y gobiernen el devenir político?

¿Existe una mediocridad programada para que el sistema siga adelante? En caso de una respuesta afirmativa, ¿se puede mejorar/cambiar dicho sistema?

¿El ser humano es principalmente esclavo de algo llamado «nómina»?

Éstas son algunas preguntas que me vienen a la mente y que tomo nota para plasmarlas en una editorial vayamierdil con el fin de compartirlas con los inteligentes lectores de este fanzine. Os animo a reflexionar sobre ellas, opinar y compartir vuestros pensares a través de nuestra web o página de Facebook. Cuantas más mentes útiles, mayor riqueza de contenidos.

También ha eyaculado mi circuito neuronal alguna que otra reflexión. Por ejemplo, el estar por encima de la media mediocre humana, no siempre conlleva a tener éxito en la vida o ganar mucho dinero. Se dan circunstancias en las que aún disponiendo de una configuración mental más rica, exista una inadaptación al sistema establecido, quedando el individuo en cuestión aislado y por consiguiente con serios problemas para adaptarse a lo que resume casi todo en el comportamiento humano, «la vida humana es vender para poder comprar». Si uno no vende, o venden por él, o muere.

Y para culminar este breve texto, creo que en un futuro, la enseñanza mínima obligatoria será muy superior. Es decir, lo que antes era el graduado escolar, se convertirá en un grado con 2 másteres.

Por Neurona.