---
title: Sesión dobla-grapil Número abril 2015
author: Neurona
type: post
date: 2015-05-06T12:21:01+00:00
url: /sesion-dobla-grapil-numero-abril-2015/
categories:
  - Blog

---
Os dejamos la foto oficial de la sesión dobla-grapil del Fanzine ¡Vaya Mierda! del número de abril 2015.

En breve se comenzará a repartir por varios parajes culturales de Zaragoza.

La tirada es de 350 ejemplares.

<div id="attachment_999" style="width: 910px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/dobla-grapa-abril-2015.jpg"><img aria-describedby="caption-attachment-999" loading="lazy" class="wp-image-999 size-full" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/dobla-grapa-abril-2015.jpg" alt="Sesión de doblado y grapado fanzine vaya mierda abril 2015" width="900" height="638" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/05/dobla-grapa-abril-2015.jpg 900w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/dobla-grapa-abril-2015-300x213.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/05/dobla-grapa-abril-2015-423x300.jpg 423w" sizes="(max-width: 900px) 100vw, 900px" /></a>
  
  <p id="caption-attachment-999" class="wp-caption-text">
    Parte del staff tras doblar y grapar 350 ejemplares de «La Joya».
  </p>
</div>

&nbsp;