---
title: La rotativa de ¡Vaya Mierda! Fanzine.
author: Neurona
type: post
date: 2014-07-16T11:49:36+00:00
url: /la-rotativa-de-vaya-mierda-fanzine/
categories:
  - Blog
tags:
  - rotativa

---
Os presentamos la rotativa del Fanzine. De su útero pare el bucocalcarismo más crónico.

[<img loading="lazy" class="alignleft size-full wp-image-746" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/07/rotativa-small.jpg" alt="Rotativa fanzine ¡Vaya Mierda!" width="500" height="713" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/07/rotativa-small.jpg 500w, https://www.vayamierdafanzine.es/contenido/uploads/2014/07/rotativa-small-210x300.jpg 210w" sizes="(max-width: 500px) 100vw, 500px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/07/rotativa-small.jpg