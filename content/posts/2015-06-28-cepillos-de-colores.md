---
title: Cepillos de colores
author: Neurona
type: post
date: 2015-06-28T20:10:50+00:00
url: /cepillos-de-colores/
categories:
  - Blog

---
De qué puedo hablar cuando hay tantas cosas de las que hacerlo. Cosas importantes, digo. Cosas que se merecen líneas, párrafos, páginas. Cosas de verdad, cosas con substancia. A eso me refiero. Cosas que importan a la gente de a pie.  
¿Dónde encuentro la substancia?, ¿dónde están esas cosas? ¿O se dice “sustancia”? Nunca lo he sabido bien. Menudas dudas de repente. Dudas tontas porque sí. O dudas de verdad, muy muy serias. Muy absurdas en apariencia pero muy serias en realidad.

No sé qué escribir, qué esperar, qué buscar ni dónde. Me limito a juntar palabras esperando que se produzca una suerte de epifanía o una suerte de profundo fracaso. Que me tiren tomates, que me corten la cabeza, que me digan: ¿se puede saber qué estás haciendo?, ¿a qué aspiras? (leer esto como si fuera un rap).

Todo eso, sí. Todo a la vez. De una vez.

Mientras tanto, mientras busco un tema del que hablar pienso que “tema” y “meta” contienen las mismas letras. Y me lavo los dientes. Pienso en las personas que se lavan los dientes mojando el cepillo antes de echar la pasta, y las que lo hacen después. También pienso en los que prefieren Nesquik y los que prefieren Colacao. En todo eso a la vez. Y se me mezcla todo, ¿lavarse los dientes con Nesquik o con Colacao?, ¿esto o lo otro?

Adónde vamos a ir a parar, y ahora qué. Un montón de lugares comunes, un montón de lugares, un montón de todo y yo no sé de qué hablar. O para qué. ¿Para quién?

Mañana va a llover, mañana es fin de semana y va a llover, pero no pasa nada. En el parte meteorológico de hoy sale un paraguas, un pequeño paraguas blanco sobre fondo azul, y muy lejos del día de mañana aparece un sol, que vendrá tal vez dentro de unos días. O no. Nunca se sabe.

Ahora estornudo, con tal mala suerte que lo hago frente a mi carné de identidad, que tengo entre las manos desde hace un segundo, por hacer algo, como mirarme al espejo de una forma no tan directa, o como mirarme a un espejo congelado en un momento pasado. ¿Por qué salgo tan mal?, ¿y este pelo? No me reconozco. En absoluto. Mocos en mi cara.

Por la ventana que da a la calle entra un bullicio enorme, gritos de niños se solapan con los motores de los coches y con las voces de señoras que venden bragas, algún pájaro se une. También oigo un canto de sirena, que parece líquido, alguien cantando debajo del agua, o alguien disfrazándose de pez, alguien que no sabe qué hacer.  
Por otra parte, huele bien, a flores de algún tipo, los árboles florecidos, porque es primavera.

[<img loading="lazy" class="alignleft size-full wp-image-1041" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/foto-susana.jpg" alt="foto-susana" width="728" height="540" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/foto-susana.jpg 728w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/foto-susana-300x223.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/foto-susana-404x300.jpg 404w" sizes="(max-width: 728px) 100vw, 728px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/06/foto-susana.jpg