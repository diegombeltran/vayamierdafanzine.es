---
title: 'Eyaculen sus interiores: ¿Qué es la existencia?'
author: ¡Vaya Mierda! Fanzine
type: post
date: 2009-04-02T18:55:04+00:00
url: /laexistencia/
robotsmeta:
  - index,follow
categories:
  - Blog

---
Algo se cuece en otra frecuencia que difiere del Carbono&#8230;

<span style="color: #ff00ff;"><span style="color: #000000;">Fuente inicial: <a href="http://es.wikipedia.org/wiki/Existencia" target="_blank">Wikipedia &#8211; Existencia</a></span><br /> </span>

> En el uso común, la existencia es «estar en el [mundo][1]«.
> 
> De eso somos conscientes al ver con nuestros sentidos otras cosas «existentes». De modo que existir viene a ser «estar en el mundo», tanto nosotros como las cosas que nos rodean.
> 
> Los [filósofos][2] investigan cuestiones como «¿Qué [es][3] lo que existe?», «¿Es lo mismo [ser][3] que existir?», «¿Cómo [conocemos][4][sabemos][5]?», «¿Hasta qué punto son los sentidos una guía fidedigna respecto al conocimiento de lo que existe?», «¿Cuál es el sentido, si existe, de las [afirmaciones][6] acerca de la existencia y de las [categorías][7], [ideas][8] y [abstracciones][9][mundo][1] y de las cosas que existen?».

 [1]: http://es.wikipedia.org/wiki/Mundo "Mundo"
 [2]: http://es.wikipedia.org/wiki/Fil%C3%B3sofos "Filósofos"
 [3]: http://es.wikipedia.org/wiki/Ser "Ser"
 [4]: http://es.wikipedia.org/wiki/Conocimiento "Conocimiento"
 [5]: http://es.wikipedia.org/wiki/Saber "Saber"
 [6]: http://es.wikipedia.org/wiki/Afirmaci%C3%B3n "Afirmación"
 [7]: http://es.wikipedia.org/wiki/Categor%C3%ADas "Categorías"
 [8]: http://es.wikipedia.org/wiki/Ideas "Ideas"
 [9]: http://es.wikipedia.org/wiki/Abstracci%C3%B3n_%28filosof%C3%ADa%29 "Abstracción (filosofía)"