---
title: Amebín’s Adventures (Prélugo)
author: Teletubi
type: post
date: 2011-06-16T08:48:50+00:00
url: /amebins-adventures-prelugo-por-teletubi-chiflado/
jabber_published:
  - 1308214130
robotsmeta:
  - index,follow
categories:
  - Blog

---
Lugar: Planeta Tierra, república de Espiña, capital Monegros.  
Fecha: 3.000.000 de años, 3 días y 1 hora después del Cataclismo.  
Precedente: Hace unos cuantos años el planeta Tierra sufrió un cataclismo que te cagas. Tal y como predijo el profeta RajoHoy, Espiña se hundió: Mucha agua y mucho mar arrasaron lo que había sido una próspera tierra de pubs nocturnos. Una vez que el cambio climático se aburrió de hacer el canso, la Península Ibérica resurgió de los mares en plan Expediente X (pausa breve para la sorpresa). Las cucarachas recolonizaron ese territorio y como ya no existían los Borbones se fundó una república. Vivían en paz y prosperidad, sin ninguna amenaza sobre sus antenas&#8230; o eso creían&#8230;  
Amebín era un blatodeo especial. Él se creía valiente, sus amigos le consideraban temerario, el resto simplemente le llamaba «tontolculo». Aún  
así, este pequeño y repugnante bicho va a gozar de una importancia especial en los sucesos venideros y me veo en la obligación de  
contárosla. A continuación comenzaré a detallaros su historia: una historia de amor, de aventuras y de hechos contrastados científicamente.  
Todo comenzó en Monegros, un vasto desierto que abarcaba desde Salamanca hasta Salou. Allí vivía Amebín rodeado de amigos, familiares y demás insectos que le repudiaban. Desde pequeño dedicó su vida a los estudios y a la investigación de su especial fascinación: los humanos, seres mitológicos que supuestamente vivieron hace tres millones de años y compartían piso con sus ancestros cucarachiles.  
Era aún de noche cuando Amebín regresaba a su batcueva tras competir en la maratón semanal de absenta. Sus diminutas alas se batían con  
la brusca caricia del cierzo mientras avanzaba tambaleándose entre vómito y vómito. Conforme iba acercándose a su casa, Amebín trataba de  
poner orden en su mente e intentar parecer un insecto sobrio y normal. No quería que sus progenitores le echaran en cara lo de siempre: «qué desfachatez ir a emborracharse con tus amigotes», «ya tienes un mes de edad y deberías buscarte un trabajo», «deberías tomar ejemplo de tus  
hermanos»&#8230;  
Mientras cavilaba en lo que parecía ser su futuro más inmediato, la joven cucaracha observó una figura extraña detrás de la tercera piedra tirando a mano izquierda. Envalentonado por el alcohol, intrigado por si era alguna joven de merecer y entusiasmado por retrasar la próxima pelea familiar, Amebín se acercó furtiva y sigilosamente (o al menos eso creía el) para averiguar de quién se trataba. No había alcanzado la primera roca cuando una melódica voz dijo:  
&#8211; No temas Amebín. Soy la Dama del Lago.  
Continuará&#8230;