---
title: Terapia Rebuznil
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-06-16T08:35:45+00:00
url: /terapia-rebuznil/
jabber_published:
  - 1308213346
categories:
  - Blog
tags:
  - terapia rebuzno

---
Un bar del área de Lepanto ofrece terapia rebuznil. Este tipo de práctica es pionera en la comarca. La técnica consistente en un amplio repertorio de mantras basados en rebuznos está causando furor en toda la zona.[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/burro.jpg?w=300" alt="" title="burro" width="300" height="225" class="alignnone size-medium wp-image-54" />][1]{.broken_link}

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/burro.jpg