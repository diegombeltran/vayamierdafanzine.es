---
title: La vida o sobre la mierda sublime
author: Neurona
type: post
date: 2016-03-09T10:30:42+00:00
url: /la-vida-o-sobre-la-mierda-sublime/
categories:
  - Blog

---
Todo comienza en una fiesta.  
Me encontraba en un cumpleaños cuando dos de las personas que también estaban allí, invitadas como yo, con ese mismo estatus, se reían entre dientes mientras miraban sus respectivos móviles y comentaban algo por lo bajini. Y claro, como la curiosidad es así, les pregunté que por qué se reían. Y hablaron de mandarse fotos de cacas. De las suyas. Como proezas orgánicas sin igual. De hecho tenían –tienen- un grupo de whatsapp dedicado al tema: mandarse fotografías en tiempo real de  aquellas cacas que por tamaño o forma resultan sublimes (no dijeron “sublimes” pero creo que al final se trata de eso, de contemplar la belleza en todas sus formas y contextos).

Aquello me pareció extraño, asqueroso e interesante a partes iguales, como la vida. Y quise saber más. Unas horas más tarde (momento _flashforward_), hablando con un amigo, le comenté el tema: hay gente que se manda fotos de sus excrementos, tío (yo no digo “tío” pero queda más registro coloquial, más realista, me gusta). Mi susodicho amigo en lugar de sorprenderse me dijo que ya lo sabía, que un amigo suyo lo hacía también, que le escribiera. La investigación empezaba a expandirse.Bien.

Así pues, quise indagar más en el tema, y hablé con el amigo de mi amigo por una red social, le pregunté sobre la cuestión, y me contó:  
“Le hago fotos a cacas que me parecen bonitas, graciosas o excesivamente desagradables.Y a cacas de perros también hago fotos. O si el hecho de cagar es una hazaña (en un sitio peligroso) también. Pero no las mando. Me las quedo yo. Y luego a según qué colegas se lo cuento y se las enseño porque creo que les hará gracia que sea tan tonto. Pero creo que debe haber un_shitagram_. Lo sé porque me hubiera gustado inventarlo y alguien me dijo que ya existía.”

Los matices se amplían, puesto que no hay dos personas iguales, como no hay dos cacas iguales. Esto es así. Se añaden nuevos datos, como el hecho de hacer fotos de cacas de perros, y el factor riesgo al realizar capturas de la hazaña que supone defecar en un sitio peligroso.

Siguiendo la nueva pesquisa que me lanza esta nueva información busco “shitagram” en el buscador  de cabecera y al no encontrarlo pongo el hashtag #shitagram en instagramy me encuentro con una gran variedad de fotos, aunque esperaba un mayor número: 583 publicaciones etiquetadas.

Entre estas 583 capturas vemos fotos de pizzas, de personas con una mascarilla facial con una textura sospechosa, postres con pinta poco apetecible, selfies, memes, tazas de váter, pantalones bajados, collages hechos con paint, paisajes bonitos, fotos desenfocadas de gintonics, azulejos, vacas, una botella de cerveza llena de cáscaras de pistacho (WTF). Dejo de rastrear cuando veo a una chica poniendo morritos.

Seguiremos informando.  
Por Susana Portero.