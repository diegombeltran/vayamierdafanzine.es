---
title: Bulos masturbatorios
author: Neurona
type: post
date: 2015-04-15T18:34:34+00:00
url: /bulos-masturbatorios/
categories:
  - Blog

---
El sexo vende. Guste o no guste, genera expectación y audiencia y si se hace convivir con un elemento que lo amplifique, se convierte en una bomba explosiva de gran poder.

Hubo un bulo hace años en Internet que unificaba dos aspectos, masturbación y monjas. Dicho bulo se propagó por email y varios blog y hablaba sobre la creación en 1840 en Málaga del «Cuerpo de Pajilleras del Hospicio de San Juan de Dios».

El Obispo de Andalucía, a través de una dispensa especial, autorizaba la creación de dicho cuerpo formado por mujeres de todo tipo de atractivo o carente de éste cuya misión era consolar a los soldados heridos en las diferentes guerras carlistas.

Sor Ethel Sifuentes fue la religiosa que hacía labores de enfermera que ideó dicha creación. El ambiente en el hospital estaba repleto de ansiedad, no era nada bueno y había que encontrar una solución al respecto.

A base de una pajilla diaria, el talante de los heridos mejoró notablemente. Incluso pacientes de distintos bandos se relacionaban entre sí amigablemente.

A este cuerpo se unieron integrantes seculares con un uniforme diseñado para tal efecto: una holgada hopalanda que ocultaba las formas femeniles y un velo de lino que embozaba el rostro.

El proyecto pajeril iba a más y se crearon distintos cuerpos: el Cuerpo de Pajilleras de La Reina, Las Pajilleras del Socorro de Huelva, Las Esclavas de la Pajilla del Corazón de María y ya entrado el siglo XX, las Pajilleras de la Pasionaria que tanto auxilio habrían de brindarle a las tropas de la República.

Incluso en Latinoamérica nacieron filiales del frote penil: mami-chingonas o las ordeñamecos.

El origen de esta invención generadora de las fantasías más variopintas nace en una página humorística mexicana.

Siguiendo por un camino común, hemos leído que en China, hay bancos de esperma donde preciosas enfermeras facilitan el traslado del esperma desde los testículos a un tarro. Este tema habrá que investigarlo en profundidad para no caer en la tentación de hacer correr otro bulo.

¿Y la masturbación femenina? ¿No hay bulos masturbatorios enfocados al sexo femenino?

Seguro que sí, aunque no hemos encontrado uno de la índole del cuerpo monja-pajil. No obstante sí que recibimos en nuestra redacción una noticia curiosa por la que Apple prohibía «HappyPlayTime» en su AppStore, un juego que enseña técnicas de masturbación a las mujeres.

Podéis encontrar más información sobre la aplicación en: http://happyplaytime.com/

¡Qué pudorosos que son los de la manzanita!

<p style="text-align: right;">
  Neurona
</p>