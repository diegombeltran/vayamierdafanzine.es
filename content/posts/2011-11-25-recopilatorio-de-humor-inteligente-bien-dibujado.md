---
title: Recopilatorio de humor inteligente bien dibujado
author: Neurona
type: post
date: 2011-11-25T12:52:50+00:00
url: /recopilatorio-de-humor-inteligente-bien-dibujado/
categories:
  - Blog
  - Chistes Adaptados

---
[<img loading="lazy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/memory_0001.jpg" alt="" title="memory_0001" width="900" height="636" class="alignnone size-full wp-image-546" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/memory_0001.jpg 900w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/memory_0001-300x212.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/memory_0001-424x300.jpg 424w" sizes="(max-width: 900px) 100vw, 900px" />][1][<img loading="lazy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0004-723x1024.jpg" alt="" title="comic1_0004" width="584" height="827" class="alignnone size-large wp-image-545" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0004-723x1024.jpg 723w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0004-212x300.jpg 212w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0004.jpg 1654w" sizes="(max-width: 584px) 100vw, 584px" />][2][<img loading="lazy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0002-723x1024.jpg" alt="" title="comic1_0002" width="584" height="827" class="alignnone size-large wp-image-544" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0002-723x1024.jpg 723w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0002-212x300.jpg 212w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0002.jpg 1654w" sizes="(max-width: 584px) 100vw, 584px" />][3][<img loading="lazy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0001-723x1024.jpg" alt="" title="comic1_0001" width="584" height="827" class="alignnone size-large wp-image-543" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0001-723x1024.jpg 723w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0001-212x300.jpg 212w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0001.jpg 1654w" sizes="(max-width: 584px) 100vw, 584px" />][4][<img loading="lazy" src="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0003-723x1024.jpg" alt="" title="comic1_0003" width="584" height="827" class="alignnone size-large wp-image-547" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0003-723x1024.jpg 723w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0003-212x300.jpg 212w, https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0003.jpg 1654w" sizes="(max-width: 584px) 100vw, 584px" />][5]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2011/11/memory_0001.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0004.jpg
 [3]: https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0002.jpg
 [4]: https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0001.jpg
 [5]: https://www.vayamierdafanzine.es/contenido/uploads/2011/11/comic1_0003.jpg