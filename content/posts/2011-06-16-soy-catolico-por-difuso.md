---
title: Soy Católico, por Difuso
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-06-16T07:52:04+00:00
url: /soy-catolico-por-difuso/
jabber_published:
  - 1308214325
categories:
  - Blog

---
Ya está. Ya lo he dicho.  
Me gusta Raboncín (lo escribo mal a propósito porque no sé si se necesita algún tipo de licencia para poder mentarle) y  
soy Católico.  
No me importa lo que puedan pensar de mí estas juventudes ultra-progresistas que tan de moda se han puesto  
últimamente, y también me da igual lo que digan las mentes más brillantes del planeta. ¿Acaso soy el único que piensa que  
Stephen Hawking ya chochea? Seguro que Freud tendría algo que decir acerca de esa extraña obsesión suya por los agujeros  
negros&#8230; pero bueno, ese es otro tema.  
Lo repetiré una vez más: soy Católico y tengo mis motivos.  
En primer lugar soy friki. De hecho soy muy friki. Y, sinceramente, estoy seguro de que creer en Dios es lo más friki que  
pueda hacerse hoy en día. ¡Es el tío con más superpoderes que nadie ha inventado jamás! Joder, si hasta el Dr. Manhattan se  
mearía en los pantalones si tuviera pantalones que pelear contra él&#8230;  
“Ya, pero&#8230;” dirán muchos, “puedes creer en Dios y ser de otra religión, como pastafari o musulmán”. Convertirme al  
pastafarismo casi me lo podría pensar (aunque la historia cristiana de su hijo heavy muriendo por él en la cruz mola que te cagas),  
pero&#8230; ¡¿Musulmán?! ¡¿Es que os habéis vuelto todos locos?! Sí, sí&#8230; ya sé que suena al último superhéroe de Marvel, pero  
pensémoslo fríamente: ¿qué me ofrece Alá si soy un buen musulmán? ¿200 vírgenes? Si ya me cuesta tirarme a una guarra,  
¿qué coño hago yo con 200 indecisas?  
Puedes pasarte meses o incluso años de sequía, para que el día en el que la has conseguido emborrachar lo suficiente o  
que su médico la ha recordado que le quedan escasas horas de vida, por fin mojes y descubras que no tiene ni puta idea de  
satisfacer a un hombre.  
Y así 200 veces&#8230; ¿qué pasa, que Alá ya sabe que las otras 199 anteriores te la van a pegar en cuanto las hayas  
enseñado un par de truquitos básicos? ¿Y si te quedas sin las 200, te dedicas el resto de la eternidad a fortalecer el antebrazo?  
Lo siento, pero NO, GRACIAS.  
Yo me quedo con el Dios bondadoso (aunque no obstante vengativo) de la religión Católica. Él, sencillamente, te promete  
que serás feliz. Y punto pelota.  
Y eso sólo puede significar una cosa: montones de buffets libres, videojuegos, figuritas de Warhammer, precuelas  
películas de Star Wars, vivienda digna y, por supuesto, chacha.  
Vale que la Iglesia haya cometido algunos “errorcillos” a lo largo de la historia, o que haya sido una de las principales  
causas de mortalidad en los dos últimos milenios, pero&#8230; ¡joder, errar es humano!, y hay que saber perdonar a los que nos  
ofenden. Además, luego nos quejaremos de la superpoblación&#8230; ¡Basta ya de hipocresía y dobles morales! ¡Otra Guerra Santa  
es lo que necesita este mundo ahora mismo!  
Y eso no es todo. La cosa es que las ideas de la Iglesia también me atraen bastante. Y no me refiero únicamente a que,  
como los Sumos Pontífices, yo también sea fan de Mr. T. (aunque no lo parezca, pero es que soy de clase trabajadora y humilde y  
no me puedo permitir llevar tanto oro encima&#8230;). Por cierto, aprovecho esto para mandar un mensaje a todos los niños que me  
estén leyendo: no os droguéis.  
Pero volviendo al tema, pongamos de ejemplo el aborto. Creo que todo aquél que tiene un mínimo de aprecio por la vida  
debe saber que impedir que un embrión llegue a nacer es una atrocidad, un atentado contra la humanidad.  
De hecho, yo me atrevo a ir mucho más lejos aún. Un óvulo no fecundado es una vida muerta, una criatura que caerá en el  
olvido, que jamás tendrá la oportunidad de haber nacido. Por eso, desde aquí, quiero animar aconsejar obligar amenazar con el  
infierno, a toda aquella mujer que ni siquiera trate de obrar el milagro de la vida cada 28 días, como una buena hija de Dios. Da  
igual con quién lo intentéis, pues todos somos hermanos, y como decía Jesucristo: debemos amarnos los unos a los otros.  
O, por ejemplo, el uso de preservativos. Y esto da igual que sea Europa, África o Marte. Todo el mundo está de acuerdo en  
que a pelo mola más. Entonces, ¿por qué esa estúpida manía de llevarle la contraria a la Iglesia? Unos te dicen que si el SIDA,  
otros que si no se qué&#8230; Pues que lo sepáis todos: el SIDA es al follar sin condón lo que el cáncer a fumar: son cosas que pasan  
por hacer lo que nos gusta. Y como diría un fumador: de algo hay que morir, ¿no?  
Perogrullo puro y duro.  
Pues a pesar de la obviedad de esto, estoy seguro de que muchos de los que se ponen la gomita de marras, cuando  
terminan se echan el cigarrito de después. Otro claro ejemplo de falsa moral de los que critican a la Iglesia Católica.  
El único problema que yo le veo a ser católico es el tener que madrugar los domingos para ir a misa o pagarle un bulo a la  
Iglesia si quieres comer carne en Cuaresma (o para mantener relaciones sexuales, pero yo con eso no tendría ningún  
problema&#8230;), pero para lo primero ya se inventó el término “católico no practicante”, y lo segundo desde cierto punto de vista  
puede ser incluso bueno: ¿a quién no le gustaría poder comprar el perdón de su dios? Puedes hacer lo que quieras, que con que  
te confieses y enciendas cuatro velitas con lo que le ibas a dar al mendigo de la puerta (que siempre hay alguno), ya vas al  
paraíso. Un chollazo, vamos.  
En definitiva: soy Católico, tengo motivos de sobra y estoy orgulloso de serlo, le pese a quién le pese.  
Y para los que no lo seáis: arded en el infierno, hijos de puta.  
[<img loading="lazy" src="http://fanzinevayamierda.files.wordpress.com/2011/06/soy_catolico_y_que.jpg?w=300" alt="" title="soy_catolico_y_que" width="300" height="300" class="alignnone size-medium wp-image-61" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2011/06/soy_catolico_y_que.jpg 400w, https://www.vayamierdafanzine.es/contenido/uploads/2011/06/soy_catolico_y_que-150x150.jpg 150w, https://www.vayamierdafanzine.es/contenido/uploads/2011/06/soy_catolico_y_que-300x300.jpg 300w" sizes="(max-width: 300px) 100vw, 300px" />][1]{.broken_link}

 [1]: http://fanzinevayamierda.files.wordpress.com/2011/06/soy_catolico_y_que.jpg