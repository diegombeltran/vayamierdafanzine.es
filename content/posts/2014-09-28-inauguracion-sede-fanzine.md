---
title: Inauguración sede Fanzine
author: Neurona
type: post
date: 2014-09-28T07:24:33+00:00
url: /inauguracion-sede-fanzine/
categories:
  - Blog

---
Un día de un pasado muy cercano respecto al presente que escribo, varios seres pensantes se reunieron en la sede de un reducto marginal de índole bucocalcárica para divagar, comer, beber y disfrutar de la ambigua existencia.

Aquí os dejamos un resumen de esta convivencia humana.

[<img loading="lazy" class="alignleft size-large wp-image-857" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046-1024x764.jpg" alt="Cúpula directiva de ¡Vaya Mierda! Fanzine" width="584" height="435" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046-1024x764.jpg 1024w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046-300x224.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046-401x300.jpg 401w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046.jpg 1280w" sizes="(max-width: 584px) 100vw, 584px" />][1]Cúpula directiva de ¡Vaya Mierda! Fanzine disfrutándola.

[<img loading="lazy" class="alignleft size-large wp-image-858" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007-1024x576.jpg" alt="Magnífico equipo humano" width="584" height="328" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007-1024x576.jpg 1024w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007-300x168.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007-500x281.jpg 500w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007.jpg 1280w" sizes="(max-width: 584px) 100vw, 584px" />][2]El magnífico equipo humano vayamierdil en la primera foto de grupo de la etapa madura III.

[<img loading="lazy" class="alignleft size-large wp-image-859" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0019-764x1024.jpg" alt="IMG-20140927-WA0019" width="584" height="782" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0019-764x1024.jpg 764w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0019-224x300.jpg 224w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0019.jpg 956w" sizes="(max-width: 584px) 100vw, 584px" />][3]Muestra de pepino envuelto en plástico. (Ver mano derecho).



Staff vayamierdil cantando la mítica canción de misa durante 4 segundos.

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0046.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0007.jpg
 [3]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/IMG-20140927-WA0019.jpg