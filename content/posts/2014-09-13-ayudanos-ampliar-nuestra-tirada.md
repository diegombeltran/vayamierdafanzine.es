---
title: Ayúdanos a ampliar nuestra tirada
author: Neurona
type: post
date: 2014-09-13T09:20:01+00:00
url: /ayudanos-ampliar-nuestra-tirada/
categories:
  - Blog
  - Sin categoría

---
**[<img loading="lazy" class="alignleft size-full wp-image-843" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/carita-de-pena-vaya-mierda-fanzine.jpg" alt="carita-de-pena-vaya-mierda-fanzine" width="250" height="334" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/carita-de-pena-vaya-mierda-fanzine.jpg 250w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/carita-de-pena-vaya-mierda-fanzine-224x300.jpg 224w" sizes="(max-width: 250px) 100vw, 250px" />][1]Realmente, somos indigentes camuflados. Nada es lo que parece.**

Siempre me ha gustado crear, imaginar, divagar y plasmar de alguna manera los extraños productos que mi mente genera.

Nunca he pensado en hacer negocio de mis pajas mentales. Por ello decidí hace muchos años ir colocando la producción mental de un nutrido equipo de humanos aficionados al hobby de divagar en un fanzine.

Mucha gente me pregunta ¿pero ganáis dinero con esto?

Siempre respondo lo mismo, hay gente que le gusta esquiar y a otros, nos gusta escribir y dibujar en un fanzine. De todas las personas que conozco que esquían, nadie gana dinero con ello. Su balance económico es negativo, pero su placer crece.

No queremos publicar solamente en formato digital (lo más económico). Somos conscientes del atractivo que dispone que las personas puedan palpar en sus manos un ejemplar de ¡Vaya Mierda! Fanzine. Por ello damos mucha importancia a la distribución en papel. Distribuimos los fanzines de forma gratuita, imprimir cuesta dinero y éste tiene que salir de algún sitio. Entre los integrantes del fanzine vamos poniendo bote, pero si además nos dan un empujoncito, podremos ampliar la tirada de este rincón del pensamiento humano.

Aunque nuestro balance económico es negativo, si te gusta ¡Vaya Mierda! Fanzine y quieres ayudarnos a que sea lo menos negativo posible y a aumentar la tirada en papel de nuestra humilde publicación, puedes realizar la donación que creas conveniente en el siguiente enlace:

<p style="text-align: left;">
  <a title="Ayúdanos a ampliar nuestra tirada de fanzines" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=4JVA4NF94UP6U" target="_blank"><img loading="lazy" class="size-full wp-image-842 aligncenter" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/donativo-vaya-mierda-fanzine.jpg" alt="donativo vaya mierda fanzine" width="173" height="66" /></a>Todas las personas o empresas que colaboren con un donativo, y que así lo deseen, aparecerán en un listado para tal efecto en esta misma página web.
</p>

Para cualquier duda o comentario, no dudes en contactar con nosotros <a title="Contacta con ¡Vaya Mierda! Fanzine" href="https://www.vayamierdafanzine.es/contactar/" target="_blank">[Aquí]</a>

<p style="text-align: center;">
  Un gran abrazo,
</p>

<p style="text-align: center;">
  <em>Neurona</em>
</p>

<p style="text-align: right;">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/mi-foto-pollo.jpg"><img loading="lazy" class="size-full wp-image-845 aligncenter" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/mi-foto-pollo.jpg" alt="mi foto pollo" width="200" height="465" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/09/mi-foto-pollo.jpg 200w, https://www.vayamierdafanzine.es/contenido/uploads/2014/09/mi-foto-pollo-129x300.jpg 129w" sizes="(max-width: 200px) 100vw, 200px" /></a>
</p>

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/09/carita-de-pena-vaya-mierda-fanzine.jpg