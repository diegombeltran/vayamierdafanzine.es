---
title: ¿De dónde vienen los niños? Una guía para explicar la reproducción humana a extraterrestres
author: Neurona
type: post
date: 2015-04-24T18:39:36+00:00
url: /de-donde-vienen-los-ninos-una-guia-para-explicar-la-reproduccion-humana-a-extraterrestres/
categories:
  - Blog

---
<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Antes de comenzar, tengamos un momento de sinceridad: ¿Quién no ha hablado alguna vez sobre la reproducción de nuestra especie? Pues bien, de ahí tener que impartir un curso introductorio a extraterrestres recién llegados hay un pequeño paso. ¿Nervios?, ¿presión? No hay problema, este artículo te dará los puntos de enfoque necesarios para salir del atolladero.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>Esporas:</b> Los seres humanos no nos reproducimos directamente mediante esporas, aunque hay tipos de estas partículas que pueden intervenir favorablemente para alcanzar dicho objetivo. Sí, me estoy refiriendo a la caspa cerebral, requisito fundamental para participar en programas de Telecinco. Una vez alcanzada la fama, y con la ayuda de otro tipo de espora denominado cocaína, se entra en frenesí sexual que puede llegar a originar réplicas de los especímenes drogados.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>[Mito] Generación espontánea:</b> Tampoco nos originamos así por las buenas. Existen falsas leyendas artúricas según las cuales los niños que vienen de París o son traídos por cigüeñas. No voy a entrar a rebatir estas falsedades porque es muy complicado, pero no son verdad y punto.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>Aburrimiento:</b> Ahí le hemos dado. El aburrimiento es un importante catalizador para motivar la práctica sexual. Se dice que nueve meses tras el apagón de Nueva York hubo un montón de nacimientos masivos. No había televisión y no había luz, lo que permitió que gente tan fea como un ornitorrinco estreñido tuviese la gran oportunidad de su vida.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>Profesión:</b> Hay unos pocos casos en los que la vida te da todo sin tener que hacer nada a cambio. Recibes dinero por no hacer nada y en un increíble gesto de solidaridad decides reproducirte sin parar para que tu progenie (la legítima) pueda gozar de tu misma fortuna (que es la de otros). En España se conoce a este suceso como «el síndrome del Borbón».</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>[¿Mito?] Clonación:</b> Existen dobles de personas, e incluso parecidos razonables con el sacerdote del pueblo, pero hay que tener en cuenta que la meiosis celular (responsable de la variabilidad genética) puede averiarse en un momento dado. La clonación es otra cosa diferente que sirve para hacer películas de ciencia ficción, crear ovejas y consumir ternasco.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;"><b>Tener un «mini yo»:</b> A veces no tienes caspa cerebral, no te aburres y además tienes que trabajar para ganarte la vida. Aún así también puedes reproducirte con el objetivo de tener criaturas que compartan el 50% de tu carga genética. El otro 50% lo pone la otra parte participante, porque hay que tener en cuenta que la reproducción sexual se trata de una actividad de equipo: de dos o más.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  Teletubi
</p>