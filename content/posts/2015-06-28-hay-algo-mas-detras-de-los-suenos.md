---
title: ¿Hay algo más detrás de los sueños?
author: Neurona
type: post
date: 2015-06-28T20:24:55+00:00
url: /hay-algo-mas-detras-de-los-suenos/
categories:
  - Blog

---
<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Los sueños han sido objeto de análisis e interpretación desde que el ser humano alcanzó la consciencia. Y no es de extrañar: La capacidad de crear y sentir un universo interpretativo diferente al que conocemos es algo impresionante y digno de estudio.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Las antiguas civilizaciones, conforme configuraban varios elementos religiosos para explicar de forma sencilla aquello que no comprendían, incorporaron a los sueños la función de herramienta comunicativa: Un vínculo entre los espíritus (más adelante dioses y posteriormente Dios) y el ser humano. Los sueños podían ser agradables o convertirse en pesadillas, en cuyo caso podía variar tanto el emisor como el carácter de dicho mensaje: demandas, advertencias, amenazas o incluso revelaciones.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Las interpretaciones centradas únicamente en el propio soñador surgieron más adelante. Aristóteles, por ejemplo, atribuía al soñar con enfermedades un autodiagnóstico físico. Freud sostuvo que los sueños eran representaciones de deseos reprimidos, producto de que el subconsciente tuviera una mayor influencia en dichas etapas. Jung compartió también estas ideas, pero variando en el propósito de los sueños al verlos como un sistema de auto-compensación.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Dentro de la fase REM (del inglés Rapid Eyes Movement) es donde las ensoñaciones son más frecuentes e intensas y la actividad cerebral es tan elevada como en plena vigilia. La teoría de consolidación de memoria atribuye dicha tarea a esta fase y a los sueños que se conforman en la misma. En este caso, tanto el universo creado como las anomalías que presenta podrían ser producto de la reestructuración interna de las conexiones neuronales.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Pese a las numerosas teorías y los avances que hay en las mismas, aún nos encontramos lejos de identificar y comprender de forma clara y definitiva lo que realmente sucede y lo que no.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">Mientras, como especie consciente nos sigue aterrorizando la posibilidad de no controlar tanto una parte de nuestro ciclo vital como nuestra mente. Esta incógnita, así como el propio miedo, han sido llevados a la literatura y al cine en múltiples ocasiones, sopesando todos y cada uno de los posibles supuestos y explicaciones, centrándose en el soñador o incluyendo injerencias de agentes externos. Y es que el desconocimiento produce intranquilidad, y por ello la necesidad de saber es acuciante.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: Times New Roman,serif;"><span style="font-size: medium;">El universo que creamos mientras soñamos y al que «transferimos» nuestra consciencia es totalmente irreal, presentando anomalías evidentes. Sin embargo, raras veces somos conscientes de estos defectos, ignorándolos o justificándolos conforme van surgiendo. Es una vez que despertamos cuando sí nos damos cuenta de estas irregularidades, legitimando con ello que hemos pasado a la vigilia. Pero también existen sueños en los que soñamos, encadenando uno tras otro. La anomalía conocida como «Déjà vu» ocurre cuando estamos despiertos&#8230; Entonces, ¿qué seguridad tenemos?, ¿cómo podemos identificar y justificar lo que es vigilia y lo que es sueño?</span></span>
</p>