---
title: 'Eyaculen su opinión: La alcaldesa de la Muela'
author: ¡Vaya Mierda! Fanzine
type: post
date: 2009-04-02T19:46:38+00:00
url: /laalcaldesadelamuela/
robotsmeta:
  - index,follow
categories:
  - Blog

---
<div class="textoGrisVerdanaContenidos">
  <p>
    <span style="color: #000000;">Pero&#8230; ¿Era o no era inteligente la alcaldesa de La Muela?</span>
  </p>
  
  <p>
    Fuente: <a href="http://heraldo.es" target="_blank">Heraldo de Aragón</a>
  </p>
  
  <blockquote>
    <p>
      La alcaldesa en funciones del Ayuntamiento de La Muela, Miriam Fajardo, dijo que el grupo municipal del Partido Aragonés (PAR) en este Consistorio «nunca» instará a la alcaldesa María Victoria Pinilla o al concejal Juan Carlos Rodrigo Vela a que renuncien a sus cargos porque «es una decisión personal de ellos». Asimismo, pidió a los concejales «seguir adelante» con la gestión municipal trabajando «con responsabilidad, rigor y firmeza».
    </p>
    
    <p>
      Pinilla y Rodrigo Vela se encuentran en prisión por su imputación en una presunta trama de corrupción urbanística, investigada bajo la denominación de Operación Molinos y de la que es responsable el titular del Juzgado de Instrucción número 1 de La Almunia de Doña Godina, Alfredo Lajusticia. María Victoria Pinilla y su esposo Juan Antonio Embarba han sido expulsados del PAR.
    </p>
    
    <p>
      Miriam Fajardo se expresó así momentos antes de iniciarse la primera sesión plenaria del Ayuntamiento desde que se produjera la detención de la alcaldesa en el marco de esta operación policial, el pasado día 18 de marzo.
    </p>
    
    <p>
      Fajardo, tras pedir a las numerosas cámaras de televisión y fotógrafos que asistieron a la sesión que salieran de la sala, expuso a sus vecinos la situación generada por la operación policial. Así, explicó que como primera teniente de alcalde ha asumido las responsabilidades propias de la alcaldía en ausencia de Pinilla, para «hacer posible el mantenimiento de los servicios municipales, la atención al vecindario y el equilibrio de la propia administración local».
    </p>
    
    <p>
      Sin embargo, consideró que esta situación «no debe prolongarse», en primer lugar por su avanzado estado de gestación que «me obligará a causar baja por maternidad» y a relevarla en el puesto su compañera Ana Cristina Mateo, segunda teniente de alcalde.
    </p>
    
    <p>
      Además, «si se llegara a producir cualquier determinación judicial o renuncia por parte de Pinilla o Rodrigo Vela, podría cambiar la composición de la corporación y la fórmula de la gestión municipal», aclaró.
    </p>
    
    <p>
      En este punto, afirmó también que «nunca me he planteado renunciar o dimitir de mi actual cargo» y, en referencia a una información en la que se afirmaba que había pedido hablar con Pinilla para afrontar la gestión municipal, aseguró que «es falso». «No he pedido hablar con ella», dijo, y agregó que los concejales del PAR «defenderemos la presunción de inocencia» de ambos.
    </p>
    
    <p>
      Por tanto, «la determinación de nuestro grupo es seguir adelante en el gobierno municipal con responsabilidad, seriedad y rigor, y trabajando con firmeza», aunque la situación «es muy difícil», porque hay asuntos municipales «que forman parte del sumario abierto por el Juzgado de Instrucción y no debemos intervenir en los mismos».
    </p>
    
    <p>
      Sin embargo, el Ayuntamiento de La Muela se ha visto afectado por la crisis y «tenemos un importante endeudamiento que nos obliga a reducir el presupuesto, contener el gasto. Reducir los servicios municipales, potenciar la gestión tributaria, o incrementar los ingresos, sacando a la venta terrenos para la construcción de viviendas protegidas». Todo ello con el fin de «recuperar la situación de estabilidad presupuestaria».
    </p>
    
    <p>
      Debido a esta misma situación, «tendremos que aparcar también de manera temporal algunos de los proyectos emprendidos, así como buscar soluciones y ayudas para hacer frente a una situación que es especialmente compleja».
    </p>
    
    <p>
      De esta forma, se impulsarán otros proyectos ya iniciados, como la obra de ampliación de la residencia «que la terminaremos con nuestro propio personal» y el Consistorio pedirá «un crédito a largo plazo para afrontar inversiones tan importantes como la mejora del abastecimiento de agua».
    </p>
    
    <p>
      Para afrontar esta situación, continuó, «confío y deseo encontrar el respaldo de todos los grupos municipales, para volver al nivel de servicios que veníamos disfrutando».
    </p>
    
    <p>
      Por otra parte, Fajardo dio cuenta de algunos cambios en el Ayuntamiento, como la llegada de un nuevo secretario, plaza que será cubierta por un procedimiento selectivo de interinidad, con la ayuda de la Diputación Provincial de Zaragoza, ya que el secretario de este municipio, Luis Ruiz Martínez, también está implicado en la presunta trama de corrupción. Otra incorporación es la de una nueva interventora.
    </p>
    
    <p>
      En uno y otro caso, la alcaldesa accidental agradeció la ayuda de Jesús Colás y Vicenta Colás, de la institución provincial, y de la directora general de Administración Local del Gobierno de Aragón, Lourdes Rubio, a la hora de reemplazar ambos puestos de trabajo y ayudar al consistorio.
    </p>
    
    <p>
      Siempre a su disposición
    </p>
    
    <p>
      Fajardo continuó su discurso asegurando que «desde siempre he estado a disposición» de los grupos del PP y PSOE, en la oposición, «cuando lo han necesitado, así como «desde el primer momento me he mostrado decidida a trabajar muy duro para afrontar la difícil gestión en que se encuentra en estos momentos el Ayuntamiento»,
    </p>
    
    <p>
      Tras agradecer el apoyo personal «que he recibido de los vecinos, de mis compañeros y de los trabajadores del Ayuntamiento», instó a «todos» a defender «el buen nombre de La Muela» tras el «fatídico día 18 de marzo», porque antes de esa fecha «éramos protagonistas del progreso y de la calidad de vida que se respira en La Muela». Por último, valoró que «no son momentos para la discusión política sino de trabajar todos juntos, con fuerza y ánimo por nuestro pueblo: La Muela».
    </p>
    
    <p>
      Un centenar de vecinos escucharon las palabras de la alcaldesa en funciones Miriam Fajardo, quien, a continuación, dio lectura al orden del día, en el que se aprobó pedir un préstamo por valor de cinco millones de euros para inversiones, así como una reducción presupuestaria de las cuentas para 2009, que finalmente alcanzaran los 27 millones de euros.
    </p>
  </blockquote>
</div>