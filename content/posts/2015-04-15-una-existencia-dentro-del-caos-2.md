---
title: Una existencia dentro del caos
author: Neurona
type: post
date: 2015-04-15T18:39:25+00:00
url: /una-existencia-dentro-del-caos-2/
categories:
  - Blog

---
Dicen que en nuestra infancia es cuando descubrimos las dimensiones y aprendemos la empatía, entre otras cosas. Al principio todo es un «yo», puesto que son nuestras necesidades infantiles las que nos urgen. Es más adelante cuando adquirimos la «conciencia de la conciencia del resto de personas». Es cuando nos percatamos de que nuestras inquietudes pueden ser compartidas y que hay anhelos diferentes dependiendo de cada persona.

El funcionamiento de las redes neuronales en la informática trata de imitar el funcionamiento de nuestro cerebro. Una neurona recibe varios impulsos (o la ausencia de los mismos) de otras neuronas y, a partir de un proceso de cálculo, «decide» si emitir o no un impulso y con qué intensidad, el cual será referente y decisivo para otras neuronas. La red puede complicarse hasta el infinito, ya que se crean y eliminan conexiones con el tiempo. El cerebro, en resumen, no deja de ser complejo conjunto de pequeños puntos de decisión que permiten crear una visión compuesta de un concepto más amplio.

Las dimensiones también las descubrimos en una muy temprana infancia. Primero las dos más necesarias y poco después la tercera. Tardamos más en encontrar la cuarta dimensión: el tiempo; y de percatarnos de su especial funcionamiento respecto a las tres anteriores.

Y luego todo se complica más. Por observación se ha comprobado la validez del «big bang» y que toda la materia proviene de un mismo punto. Y, sin embargo, eso es contrario a la ley de la gravedad. Un punto que concentrase toda la materia sería en sí mismo una singularidad que no permitiría que nada escapase. Es más, pese a que cabe pensar en un universo cerrado donde al final toda la materia volverá a reunirse, actualmente el universo se expande cada vez más rápido. En este caso, un universo abierto (el más probable), el fin de la existencia será por muerte térmica: las estrellas apagadas y un universo oscuro y frío.

A tamaño microscópico tampoco funciona la gravedad, donde impera la mecánica cuántica. Ahí solamente existen probabilidades que se transforman en certeza una vez son observadas y no antes. A partir de ahí surge la teoría bastante aceptada de los universos múltiples de Hugh Everett. Hay muchos universos paralelos (y con ello necesitamos añadir al menos una dimensión más) y entre ellos se reparten y materializan todas las probabilidades del suceso cuántico.

Este concepto, aplicándolo a la conciencia humana como si se tratase de algo cuántico, puede ser fascinante y, a la vez, sobrecogedor. Podríamos imaginarnos un «plan completo» donde todas las decisiones son tomadas. Entonces, ¿cómo quedaría el famoso libre albedrío? Podemos tomar una decisión, sí, pero realmente hemos tomado todas las posibles. ¿Es trampa?. Aun así, cada decisión sigue teniendo valor en nuestra propia existencia, en nuestro universo.

<p style="text-align: right;">
  Teletubi
</p>