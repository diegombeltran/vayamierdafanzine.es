---
title: Nos vemos en el Salón del Cómic de Zaragoza 2014
author: Neurona
type: post
date: 2014-11-30T10:53:33+00:00
url: /nos-vemos-en-el-salon-del-comic-de-zaragoza-2014/
categories:
  - Blog

---
<div style="width: 890px" class="wp-caption aligncenter">
  <img loading="lazy" class="" src="http://www.saloncomiczaragoza.com/img/image/historia%281%29.jpg" alt="Salón cómic Zaragoza" width="880" height="244" />
  
  <p class="wp-caption-text">
    Salón cómic Zaragoza
  </p>
</div>

<span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">Ya tenemos todas impresiones que compondrán el repertorio vayamierdil para el Próximo Salón del Cómic de Zaragoza.<br /> Solamente queda que el magno repertorio de seres humanos pensiles que forman parte del equipo de VM Fanzine se den cita en la sede oficial para realizar un dobla-grapa.</p> 

<p>
  La lista de precios de nuestra extensa oferta será:
</p>

<p>
  &#8211; 1 Fanzine: 0,50 €<br /> &#8211; 3 Fanzines: 1,20 €<br /> &#8211; Camiseta con la famosa frase: «Las cucarachas heredarán de la ameba» (Talla camisón) 4 €.
</p>

<p>
  Os esperamos en nuestro stand los días 12, 13 y 14 de Diciembre 2014.
</p>

<p>
  ¡Vaya Mierda! El auténtico Fanzine en blanco y negro con una grapa en medio.</span>
</p>

<p>
  + Info:<a title="¡Vaya Mierda! Fanzine en Salón del cómic Zaragoza 2014" href="http://www.saloncomiczaragoza.com/informacion/" target="_blank"> [Salón cómic Zaragoza 2014]</a>
</p>