---
title: Editorial ¡Vaya Mierda! Fanzine – Enero 2015
author: Neurona
type: post
date: 2015-04-05T20:47:08+00:00
url: /editorial-vaya-mierda-fanzine-enero-2015/
categories:
  - Blog

---
Soy muy sintético, me lo dijo un ser humano que piensa y se está quedando calvo.  
Por ello, debido a dicha aptitud y al reducido espacio existente para eyacular la editorial, procedo a exponer tres pensamientos de los muchos que hacen acto de presencia en mi mente.  
**Pensamiento 1:** Dos momentos altamente desagradables y angustiosos me visitan cada día del 1 de enero al 31 de diciembre. El primero se da cita al despertarme y el segundo ocurre a primera hora de la tarde al hacer lo mismo tras la siesta. Estar durmiendo supone para mí un estado altamente placentero, lejano a la complejidad diaria. Supongo que cuando estaba en el útero de mi madre sentía algo parecido. Pero luego llegó el juego de la vida.  
**Pensamiento 2:** Un día imaginé que estaba tan cansado de pensar continuamente en como existir que entré en coma. Permanecí seis meses en ese estado, y al despertar, estaba repleto de energía.  
Me sentía tan fuerte que me comí el mundo.  
**Pensamiento 3:** Tras hablar con amigos, me di cuenta de que la humanidad, a largo plazo, ha mejorado.  
Con sus momentos de vacas gordas y sus etapas de bajón, como la actual. Pero si tomamos nuestro origen cavernario y lo comparamos con el presente, creo que la mayoría de las personas pensarán que la situación ha ido a mejor. Conocí a un superhéroe que decía ser enviado por Dios para exterminar a la especie humana, ya que esta es horrible y contiene en sus genes la corrupción y el mal. Le expliqué lo expuesto en el anterior párrafo. Este lo meditó y convenció a Dios para que nos dejase seguir habitando la Tierra. Ya veremos en qué acaba.  
P.D.: Enviaré esta editorial a mi próximo psiquiatra para que se entretenga.

Por Neurona