---
title: Contraportada Marzo 2016
author: Neurona
type: post
date: 2016-03-09T10:37:49+00:00
url: /contraportada-marzo-2016/
categories:
  - Blog

---
<div id="attachment_1159" style="width: 810px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/contraportada-web.jpg" rel="attachment wp-att-1159"><img aria-describedby="caption-attachment-1159" loading="lazy" class="size-full wp-image-1159" src="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/contraportada-web.jpg" alt="Por Iñaki Goñi" width="800" height="834" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/contraportada-web.jpg 800w, https://www.vayamierdafanzine.es/contenido/uploads/2016/03/contraportada-web-288x300.jpg 288w, https://www.vayamierdafanzine.es/contenido/uploads/2016/03/contraportada-web-768x801.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" /></a>
  
  <p id="caption-attachment-1159" class="wp-caption-text">
    Por Iñaki Goñi
  </p>
</div>