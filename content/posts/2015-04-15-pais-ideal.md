---
title: País ideal
author: Neurona
type: post
date: 2015-04-15T18:30:12+00:00
url: /pais-ideal/
categories:
  - Blog

---
El año pasado fui de vacaciones a un país muy extraño. Me sorprendió enormemente su fobia a que las mujeres se depilasen el vello púbico.

Su apariencia era sofisticada, moderna en cuanto a arquitectura se refiere. La calidad de vida era francamente excelente y el respeto por el prójimo ejemplar.

Su elevado nivel cultural, hacía que prácticamente toda la población dispusiese de una opinión propia y eran personas abiertas a aprender día a día algo nuevo.

Paseando por uno de sus barrios me topé con un extraño bar. Su nombre era algo similar a «Dramaturgo estreñido».

Siempre disfruto con las cosas raras que no parecen peligrosas. Decidí entrar a husmear.

Su apariencia, similar a una suciedad bien ordenada con buen gusto me dejó sorprendido. Cosa bastante extraña en mí.

Me senté en la barra y pedí una jarra fría de cerveza, pero no existía la cerveza en este país. La bebida alcohólica más consumida era un fermento extraído de los testículos de un ave endémica de la zona sur. Así que no lo dudé, me gusta probar casi todo. Tenía un sabor parecido a la cerveza pero mejorado. La jarra, también estaba recién sacada del congelador.

Un hombre mal afeitado comenzó a darme conversación. Creo que pertenecía al escaso porcentaje poblacional cuyo nivel cultural rozaba el sustrato donde pacen las vacas.

  * ¿Moco va todo Gervasio? (en este país era normal llamar a los desconocidos con este nombre).

Permanecí durante 1,3 segundos algo perplejo, pero aun así respondí:

  * Bien, en la supervivencia placentera del día a día.

El ser mostró una expresión facial neutra, como de bobo. Creo que no fue una respuesta adecuada por mi parte al no adaptarse a la índole humana que tenía en frente. De hecho, no siguió hablándome. No obstante, le pregunté sin pudor debido al efecto que la espirituosa bebida ingerida hacía sobre mi cerebro:

  * ¿Aquí no se depilan las partes íntimas las mujeres?

La persona se fue a fumar un cigarro a la puerta. Me hizo gracia la falta de adaptación que mi lenguaje puede ocasionar cuando me relaciono con el 90% de la especie humana.

<p style="text-align: right;">
  <em>Neurona</em>
</p>