---
title: Comprensión de la muerte
author: Neurona
type: post
date: 2014-06-27T08:04:06+00:00
url: /comprension-de-la-muerte/
categories:
  - Blog
tags:
  - muerte

---
[<img loading="lazy" class="alignleft" src="http://hablemosdemisterio.com/wp-content/uploads/2011/02/MUERTE.jpg" alt="" width="268" height="203" />][1]{.broken_link}El concepto de muerte nos resulta instintivamente estremecedor y desagradable. Y es que la muerte entraña una pérdida, como tantas otras, pero su carácter definitivo e irresoluble la dota de mayor profundidad y fuerza. La pérdida de un ser cercano entraña dolor y pena, lo mismo que la propia pérdida de la vida.

El modelo Kübler-Ross no es el único, pero sí el más conocido sobre nuestro comportamiento y reacciones frente a este «problema». Según su planteamiento, se atraviesan varias etapas para asimilar completamente la pérdida producida por cualquier elemento que consideramos importante en nuestra vida: negación, ira, negociación, depresión y aceptación. No se tienen por qué atravesar todas las fases ni ser esto una verdad hegemónica en todas las culturas, pero puede servir como punto de partida para analizar y comprendernos dentro de nuestro ámbito cultural.

Y es que el pesar por la muerte de alguien cercano es perfectamente entendible puesto que sigues vivo como testigo de esa ausencia. Puedes extrañar o necesitar estar con esa persona perdida, y solamente te queda su recuerdo como bálsamo para aliviar el dolor. No obstante, al miedo por la muerte propia sí cabe una réplica inicial: ¿Para qué preocuparse?

Los no religiosos deberían tener asumida la no existencia, mientras que los que sí son religiosos tienen fe en la reencarnación o el «más allá». Entonces, ¿por qué nos causa tanto desazón nuestra futura muerte?, ¿por el conociento de la misma?

El hecho de sentir la muerte como algo que nos rodea nos ayuda a percatarnos de su gravedad pero, por lo dicho anteriormente, el tener un «plan B» (entendimiento de la _muerte_ como definitiva o la creencia en una continuación de la vida) debería mitigar esa sensación. Desde mi punto de vista, la muerte en sí no es lo que nos preocupa, si no su propia definición de &#8216;no vida&#8217;. Una vez muerto no puedes seguir experimentando la vida que conoces y también sabes que, por reciprocidad, tus seres cercanos te extrañarán como tú harías con ellos en su lugar.

Por eso es tan importante vivir, percatarse de lo bueno que esto conlleva, disfrutar en todo momento de todo aquello que te rodea y, lo más importante, tener una meta para tu existencia. No solamente se trata de deleitarse de nuestra existencia y el conocimiento de la misma, si no que esta debe tener unos objetivos definidos: saber qué hacer con tu vida y qué aportar con ello a la humanidad en general, y a amigos y familiares en particular.

Exprimir las horas de cada día para darles un sentido es lo más útil para afrontar el fin con el menor miedo posible. Es por ello por lo que, aprovechando la excusa del año nuevo, podemos crear e intentar nuevos propósitos. Hacerlos efectivos ya dependerá de otras cosas.

<p style="text-align: right;">
  <em>Por Teletubi a secas</em>
</p>

 [1]: http://hablemosdemisterio.com/wp-content/uploads/2011/02/MUERTE.jpg