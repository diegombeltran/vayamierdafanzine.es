---
title: Abril 2011
author: ¡Vaya Mierda! Fanzine
type: post
date: 2011-04-05T11:15:45+00:00
url: /abril11/
robotsmeta:
  - index,follow
categories:
  - Fanzines

---
Seguimos con el nuevo y esplendoroso diseño de manos de nuestra maquetadora profesional. En este número se dan cita buenas dosis de _Ajo&#8217;s Revolt_, _Recetas Guarras_, pensamientos de todo tipo, y el siguiente capítulo de _Amebín&#8217;s Adventures._

A vuestra disposición queda. ¡Recordad disfrutarlo!

[gview file=»http://dl.dropbox.com/u/20576313/Abril_2011.pdf»]

&nbsp;