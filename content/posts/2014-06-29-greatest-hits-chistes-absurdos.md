---
title: Greatest Hits – Chistes absurdos
author: Neurona
type: post
date: 2014-06-29T20:11:59+00:00
url: /greatest-hits-chistes-absurdos/
categories:
  - Blog

---
[<img loading="lazy" class="alignleft wp-image-679" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012-743x1024.jpg" alt="Chistes absurdos" width="1400" height="1927" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012-743x1024.jpg 743w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012-217x300.jpg 217w" sizes="(max-width: 1400px) 100vw, 1400px" />][1]

&nbsp;

[<img loading="lazy" class="alignleft size-large wp-image-680" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012b-743x1024.jpg" alt="Chistes absurdos" width="584" height="804" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012b-743x1024.jpg 743w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012b-217x300.jpg 217w" sizes="(max-width: 584px) 100vw, 584px" />][2]

&nbsp;

[<img loading="lazy" class="alignleft size-large wp-image-681" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/contraportada-recopilatorio-2012-743x1024.jpg" alt="chiste absurdo" width="584" height="804" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/contraportada-recopilatorio-2012-743x1024.jpg 743w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/contraportada-recopilatorio-2012-217x300.jpg 217w" sizes="(max-width: 584px) 100vw, 584px" />][3]

&nbsp;

[<img loading="lazy" class="alignleft size-large wp-image-682" src="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/portada-recopilatorio-2012-743x1024.jpg" alt="chiste absurdo" width="584" height="804" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2014/06/portada-recopilatorio-2012-743x1024.jpg 743w, https://www.vayamierdafanzine.es/contenido/uploads/2014/06/portada-recopilatorio-2012-217x300.jpg 217w" sizes="(max-width: 584px) 100vw, 584px" />][4]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012.jpg
 [2]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/arte-con-mente-recopilatorio-2012b.jpg
 [3]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/contraportada-recopilatorio-2012.jpg
 [4]: https://www.vayamierdafanzine.es/contenido/uploads/2014/06/portada-recopilatorio-2012.jpg