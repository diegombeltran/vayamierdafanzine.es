---
title: El escritor famoso
author: Neurona
type: post
date: 2015-09-09T11:28:17+00:00
url: /el-escritor-famoso/
categories:
  - Blog

---
El escritor famoso  
Entrevista realizada por Lucy Streetworker

A ver… Probando, probando… Bien, la grabadora funciona… Vamos allá.  
&#8211; Buenos días, Don… En primer lugar, felicitarle por el éxito de ventas de su última novela. Parece que los lectores la están disfrutando mucho…  
&#8211; Natural, querida muchacha: La he escrito yo. Ellos me adoran.  
&#8211; ¿Cómo se definiría en pocas palabras? Tanto en lo personal como en lo literario, quiero decir.  
&#8211; Hmm. No me gusta hablar de mí, pero ya que me lo preguntas, diría que soy un hombre sencillo, de trato agradable, generoso, solidario, educado, inteligente, valeroso, amable y atento con las mujeres y (esto ponlo en negrita si puede ser) bastante rico. En mi faceta literaria, me definiría como un escritor brillante, pero escasamente valorado por la crítica (quizá, precisamente, a causa de mi enorme talento).  
&#8211; Hablando de su talento… Se dice que usted inventó un nuevo género literario: La sandez. ¿Qué podría contarme al respecto?  
&#8211; No, no, no, querida… El mérito no es mío. El género ya estaba inventado desde mucho antes. Es más: a decir verdad, la mayor parte de mis contemporáneos forman parte de esa corriente literaria.  
&#8211; Hay quienes -seguramente críticos malintencionados-, para definir su estilo, han usado la palabra insustancialidad. ¿Es usted un autor insustancial?  
&#8211; Bueno, no sé. Es posible… Pero si a mis lectores les gusta, ¿qué puedo hacer yo? Es el estilo que me caracteriza, mi seña de identidad, por así decir… Y las ventas, ¿qué me dices de las ventas? El público es sabio, querida mía. Ah, mi amado público…  
&#8211; Respecto a las acusaciones de plagio… Numerosas, por cierto…  
&#8211; Ah, mi niña. (Te queda muy bien esa minifalda, por cierto). Nunca faltan buitres que quieran aprovecharse del éxito ajeno. ¿Qué significan cien o doscientas coincidencias en una novela? Si la vida en sí es pura coincidencia.  
&#8211; Pero debe admitir que es muy raro que incluso los nombres de los protagonistas sean idénticos a los de las novelas de quienes le acusan…  
&#8211; Ay, niña. ¿Qué es un nombre? Si lo que cuenta es la esencia, la trama.  
&#8211; También por ese lado parece haber asombrosas casualidades…  
&#8211; Mira… Te contaré un secreto… Pero, acércate más… Así, bien juntitos… Y apaga esa grabadora. Verás: Toda la culpa es de un tipo que tengo subcontratado. Yo tengo una vida social demasiado activa como para poder dedicar tiempo a escribir. Así que él me escribe las novelas. Luego le doy un porcentaje y aquí paz y después gloria. Pero el tipo es un vago y, en lugar de escribir, se dedica a copiar historias que ve por ahí y así le queda tiempo para bajarse porno. En el fondo, no es mala cosa, ya que esas historias son bastante mejores que las que él o yo podríamos idear. Según me contó cuando nos demandaron, lo difícil es adaptar esas historias a mi estilo, pero eso lo hace bastante bien. Por eso no le despido. Bueno, y porque si se va de la lengua, estoy jodido.  
&#8211; Entonces, ¡Es cierto!  
&#8211; Shhh… Esto no puedes contarlo. Es una confidencia de cama, por así decir…  
&#8211; ¡Pero no estamos en una cama!  
&#8211; No te impacientes, monina. En seguida lo estaremos.  
&#8211; ¿Qué puede contarme de su próximo trabajo?  
&#8211; Poca cosa, bonita, poca cosa… Estoy con el borrador, ya sabes. (Guiño cómplice).  
&#8211; Pero me podrá adelantar algo del argumento…  
&#8211; Bueno. Es la historia de dos hombres que suben una escalera porque el ascensor se ha averiado. Después bajan por la misma escalera.  
&#8211; Sí. Concuerda con su estilo. Bien. Creo que con esto tendremos suficiente.  
&#8211; Ah. Excelente, querida. Así podemos pasar a lo importante… ¿Quieres que te desnude yo? Dicen que lo hago muy bien.  
&#8211; De eso nada. La entrevista ha terminado. Yo me piro.  
&#8211; Pero… Me habías prometido…  
&#8211; Sí, claro, pero es que es el primer trabajo que hago para el VayaMierda! Y si no vuelvo con la entrevista me despiden, y una ya está mayor para pasarse la noche en una esquina… es cosa de supervivencia… Seguro que usted me entiende. Hasta otra.  
&#8211; Espera, espera… Te pagaré.  
&#8211; Hmmm… No sé si la revista lo vería con buenos ojos, pero… ¿Cuánto?  
&#8211; Cien. ¿Te parece bien?  
&#8211; Mejor doscientos y no se lo cuento a tu mujer. (Qué chollo. Esto lo grabo y ya tengo para otro artículo. Lo mismo me hacen redactora jefa).  
Las dos sombras se pierden por la puerta del fondo. Círculo negro cerrándose. Fin.

Del libro inédito: Entrevistando monstruos,  
de Lucy Streetworker