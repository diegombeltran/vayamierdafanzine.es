---
title: Nueva sala de videofórum Vintage
author: Neurona
type: post
date: 2015-06-26T13:38:41+00:00
url: /nueva-sala-de-videoforum-vintage/
categories:
  - Blog

---
Ya tenemos montada la sala vintage para las sesiones de videofórum en nuestra sede. Tele con culo, VHS y disco duro multimedia. ¡Grandes éxitos desconocidos pasarán por nuestros ojos!

[<img loading="lazy" class="alignleft size-full wp-image-1021" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/sala-videofórum.jpg" alt="sala-videofórum" width="1000" height="750" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/06/sala-videofórum.jpg 1000w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/sala-videofórum-300x225.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/06/sala-videofórum-400x300.jpg 400w" sizes="(max-width: 1000px) 100vw, 1000px" />][1]

 [1]: https://www.vayamierdafanzine.es/contenido/uploads/2015/06/sala-videofórum.jpg