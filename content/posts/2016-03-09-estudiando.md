---
title: Estudiando
author: Neurona
type: post
date: 2016-03-09T10:39:49+00:00
url: /estudiando/
categories:
  - Blog

---
<div id="attachment_1168" style="width: 594px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/estudiando.jpg" rel="attachment wp-att-1168"><img aria-describedby="caption-attachment-1168" loading="lazy" class="size-large wp-image-1168" src="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/estudiando-732x1024.jpg" alt="Por Neurona" width="584" height="817" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2016/03/estudiando-732x1024.jpg 732w, https://www.vayamierdafanzine.es/contenido/uploads/2016/03/estudiando-214x300.jpg 214w, https://www.vayamierdafanzine.es/contenido/uploads/2016/03/estudiando-768x1074.jpg 768w" sizes="(max-width: 584px) 100vw, 584px" /></a>
  
  <p id="caption-attachment-1168" class="wp-caption-text">
    Por Neurona
  </p>
</div>
