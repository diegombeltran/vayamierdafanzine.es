---
title: 'Eyaculen sus comentarios: Un billón de dólares para reflotar la economía mundial'
author: ¡Vaya Mierda! Fanzine
type: post
date: 2009-04-02T18:43:01+00:00
url: /economiamundial/
robotsmeta:
  - index,follow
categories:
  - Blog

---
<span style="color: #000000;">Comenzando una de las actividades que realizaremos en el blog del Fanzine ¡Vaya Mierda!, exponemos esta primera noticia con el fin de recibir sus ideas, devenires mentales y comentarios, desde el punto de vista de sus cráneos:</span>

<span style="color: #000000;">Fuente: <a href="http://www.heraldo.es" target="_blank"> Heraldo de Aragón</a><br /> </span>

> Los líderes del G-20 han llegado a un acuerdo para intentar superar la crisis económica, que incluye una reforma del sistema financiero y un fondo de 1 billón de dólares (743.000 millones de euros) para los organismos multilaterales, según anunció el primer ministro británico, Gordon Brown.
> 
> «Este es el día en que el mundo se unió para luchar conjuntamente contra la recesión global», dijo Brown en una intervención ante la prensa tras finalizar la cumbre del G-20 en Londres, que no fija nuevos estímulos para impulsar la economía y sólo se refiere a ellos de forma genérica.
> 
> Brown, afirmó, no obstante, que «el esfuerzo fiscal sin precedentes» que están llevando a cabo los países del G-20 aportará a la economía mundial 5 billones de dólares adicionales hasta finales de 2010.
> 
> En el comunicado final de la cumbre se señala que este esfuerzo fiscal ayudará a salvar o crear millones de puestos de trabajo en todo el mundo y a aumentar la producción mundial un 4%.
> 
> El acuerdo de Londres se logró tras «duras» negociaciones -como subrayó la canciller alemana, Angela Merkel- entre los países que, como Alemania y Francia, daban prioridad a la regulación del sistema financiero internacional y los que, como EEUU, abogaban por estímulos fiscales para impulsar la economía.
> 
> El primer ministro británico aseguró que el G-20 lanza un mensaje claro de que «en esta era global nuestra prosperidad es indivisible» y de que «son necesarias soluciones globales a los problemas globales».
> 
> Brown dijo que «el consenso de Washington está superado» y que ha llegado «un nuevo consenso», en el que el comercio mundial debe convertirse «en un motor del crecimiento».
> 
> Los jefes de Estado y de Gobierno acordaron una reforma del sistema financiero global, incluidos los hedge funds (fondos de inversión libre), el control de las agencias de calificación de riesgos y el establecimiento de un sistema internacional contable más claro.
> 
> «El secreto bancario es una cosa del pasado», subrayó Brown, quien añadió que es necesario «limpiar los bancos» para restablecer las líneas crediticias a empresas y ciudadanos, y que también habrá nuevas reglas sobre los bonos de los directivos bancarios.
> 
> Habrá asimismo un enfoque común para hacer frente a los «activos tóxicos» en manos de las entidades financieras y un paquete de ayuda de 50.000 millones de dólares destinados a los países pobres.
> 
> Entre las medidas concretas, el G-20 comprometió 1 billón de dólares (743.000 millones de euros al cambio actual) para los organismos financieros multilaterales con el objetivo de ayudar a los países con problemas.
> 
> El Fondo Monetario Internacional (FMI) triplicará sus recursos y recibirá 500.000 millones de dólares adicionales a los 250.000 millones ya comprometidos para este organismo, y habrá otros fondos para el Banco Mundial (BM)y el Foro de Estabilidad Financiera.
> 
> El Foro se rebautiza como Financial Stability Board (Consejo de Estabilidad Financiera) y colaborará con el Fondo para garantizar la cooperación transfronteriza y establecer un mecanismo de alerta temprana de eventuales episodios de inestabilidad financiera.
> 
> El G-20 acordó destinar 250.000 millones de dólares para tratar de relanzar el comercio mundial y las exportaciones, en lugar de los 100.000 millones que se habían planteado en un principio.
> 
> Esta multimillonaria partida será financiada, explicó el primer ministro británico, por la Unión Europea (UE), con 100.000 millones de dólares, Japón, con otros 100.000 millones, y China, con 40.000 millones, además de otros países que no nombró.
> 
> Los líderes políticos del G-20, grupo que representa al 85% de la economía y a dos tercios de la población mundial, acordaron además medidas concretas contra el proteccionismo y decidieron establecer sanciones contra los paraísos fiscales.
> 
> No hubo acuerdo sobre nuevos estímulos para impulsar la economía, tras las distintas posiciones expresadas por EE. UU. y los países de la Europa continental, especialmente Francia y Alemania.
> 
> Este apartado se quedó en una referencia a «hacer todo lo que sea necesario» para recuperar las economías del planeta.
> 
> El presidente francés, Nicolas Sarkozy, anunció que habrá una nueva cumbre del G-20 en septiembre en Nueva York, coincidiendo con la Asamblea General de la ONU.