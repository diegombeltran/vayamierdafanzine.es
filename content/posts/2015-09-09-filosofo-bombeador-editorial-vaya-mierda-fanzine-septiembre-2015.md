---
title: Filósofo bombeador – Editorial ¡Vaya Mierda! Fanzine Septiembre 2015.
author: Neurona
type: post
date: 2015-09-09T11:35:13+00:00
url: /filosofo-bombeador-editorial-vaya-mierda-fanzine-septiembre-2015/
categories:
  - Blog

---
Hoy tenemos en ¡Vaya Mierda! Fanzine a Remilgio López de Lanuza, filósofo autodidacta, freelance del pensamiento y pionero en el arte del Yoga absurdo.  
&#8211; Hola Remilgio, ¿Qué tal se encuentra?  
&#8211; Muy bien, y sin GPS.  
&#8211; ¿Qué es para usted el sexo?  
&#8211; Un acto fisiológico, al igual que comemos, bebemos, cagamos o sudamos, necesitamos orgasmos. En mi caso es de imperiosa necesidad eyacular cada día o a lo sumo cada dos.  
&#8211; ¿Tiene relación sexo y amor?  
&#8211; No mucha. El sexo masculino por ejemplo es frotar la varita orgánica hasta eyacular. La mujer necesita también correrse. Imagínese una mujer o un hombre que le dan duro y nunca alcanzan el orgasmo. Una persona puede follar con otra que ama o no. Son aspectos diferentes. Uno puede comer con el amor de su vida o con su suegra, compañero de trabajo, vecino, cliente, etc.  
&#8211; ¿Son muy diferentes los orgasmos masculinos y femeninos?  
&#8211; Por la impresión que tengo, si. Yo tengo uno y luego me desinflo y hasta dentro de un rato no puedo seguir. Otra cosa es lo que haga durar el frote hasta conseguir mi orgasmo. La mujer es otra historia, por ejemplo, follando de la misma manera he conseguido en una mujer uno o ningún orgasmo y en otra catorce. Creo que hay mujeres que necesitan mucha estimulación clitoriana y otras prácticamente ninguna. O quizá tengan muy conectado el clítoris a la vagina y solamente con la penetración consiguen orgasmos múltiples. No obstante, no soy sexólogo ni vaginólogo. Solamente le hablo de lo vivido.  
&#8211; ¿Y los juegos previos?  
&#8211; Los juegos previos son, como su nombre indica, preliminares, pero lo que al final importa a la mayoría de las personas es alcanzar el orgasmo. Hay gente que les da mucha importancia y otros que les gusta más lo burdo y genital. El ser humano es muy variado y hay que respetar sin escandalizarse sin que se supere la línea de lo inadmisible.  
&#8211; Muchísimas gracias Remilgio, ha sido un placer tenerle aquí.  
&#8211; Igualmente, quizá otro día me tenga en otro lugar más cálido y húmedo.  
&#8211; ¿República Dominicana?  
&#8211; Efectivamente, veo que conoce mi agenda de ponencias.

Neurona