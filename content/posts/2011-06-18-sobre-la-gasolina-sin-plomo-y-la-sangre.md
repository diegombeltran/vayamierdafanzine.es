---
title: Sobre la gasolina sin plomo y la sangre
author: Sandman
type: post
date: 2011-06-18T09:40:23+00:00
url: /sobre-la-gasolina-sin-plomo-y-la-sangre/
jabber_published:
  - 1308390024
robotsmeta:
  - index,follow
categories:
  - Blog

---
Hace unos años, los gobiernos occidentales tomaron la decisión de sustituir la antigua gasolina por una versión “light”, menos eficiente pero menos contaminante. Lo que hicieron fue, en el proceso de depuración, eliminarle el plomo, que era uno de los componentes más contaminantes. Pronto surgió el problema de qué hacer con el plomo eliminado. Con la solidaridad que nos caracteriza a los occidentales, decidimos devolverlo a los legítimos dueños del petróleo. Eso sí, no sin antes haberlo transformado convenientemente en minas antipersona y armas de largo calibre.

Estamos en un momento en que es difícil mantener un equilibrio entre los países productores y consumidores de petroleo, y es por ello que nuestros gobiernos tienen que tomar decisiones. Con equilibrio me refiero, obviamente, a permitir que en esos países puedan comer casi todos los días, aunque sin excesos, pero a nosotros que no nos limiten la velocidad a 110 km/h. ¡faltaría más!

Todas estas revoluciones que se están montando, sin duda, son un grave problema para nosotros, y nuestros “amados” líderes se han puesto rápidamente manos a la obra. La oferta final será bastante clara: Sangre por petroleo. Ellos ponen la sangre y nosotros nos llevamos el petroleo.

¿Digo con esto que la OTAN va a intervenir en Libia?. Por supuesto que no. Siempre y cuando Gadafi, o cualquier otro gobernante que se ponga en su lugar, haga negocios. Eso sí, sería bastante molesto no invadir Libia ahora, después de todo el dinero invertido en mover a nuestros barquitos.

El único problema al respecto es que ahora se lleva mucho eso de decir que se respetan los derechos humanos. Aparentando respetar los derechos humanos no se puede invadir un país para esclavizar a sus habitantes y robar sus bienes. Para evitarnos tomar la decisión de realizar una invasión que chocaría con nuestro policorrectismo, como siempre, he dado con la solución. Es más sencilla de lo que parece. Todo es cuestión de hacer un pequeño cambio en un documento.

Donde dice

`Todos los seres humanos nacen libres e iguales en dignidad y derechos y, dotados como están de razón y conciencia, deben comportarse fraternalmente los unos con los otros`

Se debería cambiar por

`Todos los seres humanos nacen libres e iguales en dignidad y derechos y, dotados como están de razón y conciencia, deben comportarse fraternalmente los unos con los otros. Se excluyen del punto anterior aquellos pertenecientes a países no miembros de la OTAN, salvo que puedan pagarse un buen abogado. Asimismo, las personas sin hogar y todos aquellos cuyos ingresos estén por debajo del primer cuartil en una gráfica normal, se consideran no merecedores de ser protegidos por los derechos humanos. Éstos últimos podrán ganarse la condición de humanos trabajando en el ejército y dando la vida por sus patrias.`

Así matamos dos pájaros de un tiro. Podemos invadirlos y nos garantizamos gente para hacerlo.

¿Ficción? No, es lo que estamos haciendo, solo que nunca escucharemos a nadie decirlo en voz alta.