---
title: Humor inteligente
author: Neurona
type: post
date: 2016-03-09T10:33:56+00:00
url: /humor-inteligente/
categories:
  - Blog

---
<p lang="zxx" align="CENTER">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;"><i>por Teletubi</i></span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Vaya forma pedante de definir un tipo de humor como «humor inteligente». Si no te gusta o no lo entiendes, ¿qué característica mental prefieres aplicarte?: ¿el de la «tontunez» suprema o el de la esencia de la «sosez»?</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Obviando la soberbia y falta de tacto por el empleo ese nombre, creo que se podrían englobar dentro de este tipo de humor aquellas expresiones, situaciones y conversaciones que tengan, como mínimo, una doble o triple lectura. En ese caso tendrían cabida la ironía, la mordacidad y la dialéctica absurda.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Entonces, por un lado estaría el humor que hace gracia de un modo instantáneo y por otro lado el que requiere una interpretación posterior, la cual generalmente es inmediata, pero no instantánea.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Al igual que con el humor instantáneo puede no llegar a hacer gracia, al no ajustarse a los gustos del sujeto receptor de la risa; puede ocurrir lo mismo con el humor de interpretación si el resultado de la interpretación no parece coherente o sencillamente no interesa.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">En el cine me gustan las películas que se auto-refuerzan en los diálogos, generalmente mediante conversaciones saturadas de mordacidad o conceptos absurdos. Es por ello que me encantan las obras de Quentin Tarantino, Woody Allen, o Monty Python pese a que sus temáticas generales sean muy diferentes.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">Vuelvo de las ramas. ¿Es más satisfactorio reírse por un doble sentido o un sarcasmo que, por ejemplo, ver una caída tonta? Solamente si los primeros encajan mejor con tu personalidad y gustos. En mi caso sí, pero una de las bellezas de la especie humana es su diversidad, por lo que no puede esgrimirse por ello mayor o menor inteligencia. Salvo en el caso de que se emplee a Belén Esteban como baremo de medida, pero eso es jugar con ventaja.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">¿Es más «inteligente» un chiste que necesita ser traducido o explicado sobre uno que no? Probablemente no, ya que la «inteligencia» residiría más en simplificar la complejidad de un doble sentido que en regodearse de la misma. Que no sea simple pero sí accesible, ya que cuanto más tiempo se requiera para interpretar más riesgo hay de que el cerebro pierda el hilo.</span></span>
</p>

<p lang="zxx" align="JUSTIFY">
  <span style="font-family: 'Times New Roman', serif;"><span style="font-size: medium;">En resumen, reíros como y de lo que os dé la [censurado] gana.</span></span>
</p>