---
title: Baños del mundo
author: Neurona
type: post
date: 2015-03-23T11:13:45+00:00
url: /banos-del-mundo/
categories:
  - Blog

---
Estimados humanos de alta alcurnia mental.  
Os presentamos el rincón virtual de baños del mundo, patrocinado por ¡Vaya Mierda! Fanzine.  
Espero lo disfruten.  
Si tienen baños de algún paraje que os guste, no duden en enviarnos vuestras fotografías.

<div id="attachment_908" style="width: 594px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-nuevo-sidecar-casco-zaragoza.jpg"><img aria-describedby="caption-attachment-908" loading="lazy" class="size-large wp-image-908" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-nuevo-sidecar-casco-zaragoza-638x1024.jpg" alt="Baños del nuevo Pub El Sidecar en Zaragoza." width="584" height="937" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-nuevo-sidecar-casco-zaragoza-638x1024.jpg 638w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-nuevo-sidecar-casco-zaragoza-187x300.jpg 187w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-nuevo-sidecar-casco-zaragoza.jpg 1728w" sizes="(max-width: 584px) 100vw, 584px" /></a>
  
  <p id="caption-attachment-908" class="wp-caption-text">
    Baños del nuevo Pub El Sidecar en Zaragoza.
  </p>
</div>

<div id="attachment_909" style="width: 594px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-pub-el-zorro-zaragoza.jpg"><img aria-describedby="caption-attachment-909" loading="lazy" class="size-large wp-image-909" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-pub-el-zorro-zaragoza-1024x702.jpg" alt="Baños Pub El Zorro en Zaragoza." width="584" height="400" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-pub-el-zorro-zaragoza-1024x702.jpg 1024w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-pub-el-zorro-zaragoza-300x206.jpg 300w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baño-pub-el-zorro-zaragoza-438x300.jpg 438w" sizes="(max-width: 584px) 100vw, 584px" /></a>
  
  <p id="caption-attachment-909" class="wp-caption-text">
    Baños Pub El Zorro en Zaragoza.
  </p>
</div>

<div id="attachment_910" style="width: 594px" class="wp-caption alignleft">
  <a href="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baños-bar-lagasca-zaragoza.jpg"><img aria-describedby="caption-attachment-910" loading="lazy" class="size-large wp-image-910" src="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baños-bar-lagasca-zaragoza-910x1024.jpg" alt="Baños bar Lagasca. Zaragoza." width="584" height="657" srcset="https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baños-bar-lagasca-zaragoza-910x1024.jpg 910w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baños-bar-lagasca-zaragoza-266x300.jpg 266w, https://www.vayamierdafanzine.es/contenido/uploads/2015/03/baños-bar-lagasca-zaragoza.jpg 1932w" sizes="(max-width: 584px) 100vw, 584px" /></a>
  
  <p id="caption-attachment-910" class="wp-caption-text">
    Baños bar Lagasca. Zaragoza.
  </p>
</div>